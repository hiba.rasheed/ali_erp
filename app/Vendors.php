<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendors extends Model
{
    protected $table = 'vendors';
    protected $primaryKey = 'id';
    protected $fillable = [
        'c_group',
        'p_group',
        'v_type',
        'company',
        'name',
        'address',
        'c_no',
        'country',
        'VAT',
        'GST',
        'state',
        'email',
        'postalCode',
        'c_id',
        'created_by',
        'updated_by',
        'u_id',
        'status',
        'name2',
        'c_no2',
        'balance'
    ];
    public function city()
    {
        return $this->hasOne('App\City','id','c_id');
    }
    public function user()
    {
        return $this->hasOne('App\User','id','u_id');
    }
    public function cgroup()
    {
        return $this->hasOne('App\Groups','id','c_group');
    }

    public function pgroup()
    {
        return $this->hasOne('App\Groups','id','p_group');
    }

    public function purchase()
    {
        return $this->belongsTo('App\PurchaseOrder');
    }

    public function sale()
    {
        return $this->belongsTo('App\Sales');
    }

    public function stocks()
    {
        return $this->belongsTo('App\Stocks');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }

    public function quotation()
    {
        return $this->belongsTo('App\Quotation');
    }

}
