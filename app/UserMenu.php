<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMenu extends Model
{
    protected $table = 'user_menu';
    protected $primaryKey = 'id';
    protected $fillable = [
        'p_id',
        'name',
        'icon',
        'route',
        'sort',
        'created_by',
    ];

    public function children()
    {
        return $this->hasMany('App\UserMenu','p_id','id');
    }
    public function parent()
    {
        return $this->hasOne('App\UserMenu','id','p_id');
    }

    public function permission()
    {
        return $this->hasMany('App\Permissions','m_id','id');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function roleM()
    {
        return $this->hasMany('App\RoleMenu','m_id','id');
    }
}
