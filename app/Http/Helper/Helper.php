<?php
use App\RoleMenu;
use App\RolePermission;
use App\Permissions;
use App\UserMenu;
use Automattic\WooCommerce\Client;

function getMenuId($request){
    return UserMenu::where('route',$request->route()->getName())->first()->id;
}
function getRolePermission($menu_id){
    $permission_id      =   Permissions::where('m_id',$menu_id)->get()->pluck('id')->toArray();
    $role_permissions   =   RolePermission::with('permission')->where('r_id',Auth::user()->r_id)->whereIn('p_id',$permission_id)->get()->pluck('p_id')->toArray();
    return Permissions::whereIn('id',$role_permissions)->get()->pluck('name')->toArray();
}

function wooCommerce()
{

    $woocommerce = new Client(
        'https://www.staging19.montivo.pk/', // Your store URL
        'ck_fa7c16380215fb37d13b3c9a0f5e4e7f17882bb4', // Your consumer key
        'cs_03de97c80e54a5a4e5decf215065cb01bbc604c6', // Your consumer secret
        [
            'wp_api' => true, // Enable the WP REST API integration
            'version' => 'wc/v3' // WooCommerce WP REST API version
        ]
    );
    return $woocommerce;
}


?>
