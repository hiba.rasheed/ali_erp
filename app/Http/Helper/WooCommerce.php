<?php

use App\Products;
use App\Variants;

function storeProduct(array $request)
{
    // dd($variants);
    $woocommerce        =   wooCommerce();
    if(isset($request['image']))
    {
        $wp_Data=([
            'name' => $request['pro_name'],
            'sku' => $request['pro_code'],
            'regular_price' => $request['price'],
            'description' => $request['description'],
            'short_description' => null,
            'manage_stock' => true,
            'categories' => [
                ['id' => $request['cat_id']]
            ],
            'images' => [
                'src' => env('AppUrl').$request['image']
            ]
        ]);
    }
    else {
        $wp_Data=([
            'name' => $request['pro_name'],
            'sku' => $request['pro_code'],
            'regular_price' => $request['price'],
            'description' => $request['description'],
            'short_description' => null,
            'manage_stock' => true,
            'categories' => [
                ['id' => $request['cat_id']]
            ],
            'images' => [

            ]
        ]);
    }
    $woo = $woocommerce->post('products', $wp_Data);
    // if(count($variants['variants']) > 0 )
    // {
    //     for ($i=1; $i <= count($variants['variants']) ; $i++)
    //     {
    //         for ($j=0; $j < count($variants['variants'][$i]) ; $j++) {
    //             $pv = Variants::where('name',$variants['variants'][$i][$j])->first();
    //             $data = [
    //                 'regular_price' => $variants['pricev'][$i] == null ? $request['price'] :  $variants['pricev'][$i],
    //                 'image' => [],
    //                 'attributes' => [
    //                     [
    //                         'id' => 5,
    //                         'variation' => true,
    //                         'visible' => true,
    //                         'option' => $variants['variants'][$i][$j]
    //                     ]
    //                 ]
    //             ];
    //             // dd($data);
    //             $woocommerce->post('products/'.$woo->id.'/variations', $data);
    //         }
    //     }
    // }


    return $woo;
}

function listProduct()
{
    $woocommerce        =   wooCommerce();
    return json_decode(json_encode($woocommerce->get('products')),true);
}

function addStock(array $request,$id)
{
    $woocommerce        =   wooCommerce();
    $product = Products::where('id',$id)
    ->first();
    $products_w = json_decode(json_encode($woocommerce->get('products/'.$product->wooId)),true);
    if($products_w['stock_quantity'] == null)
    {
        $stock_quantity = [
            'stock_quantity' => $request['quantity']
        ];
    }
    else {
        $stock_quantity = [
            'stock_quantity' => $request['quantity'] + $products_w['stock_quantity']
        ];
    }
    $woocommerce->put('products/'.$product->wooId, $stock_quantity);


}

function listOrders()
{
    $woocommerce        =   wooCommerce();
    return json_decode(json_encode($woocommerce->get('orders')),true);
}


function addPurchaseQty(array $request,$id)
{
    $woocommerce        =   wooCommerce();
    $product = Products::where('id',$id)
    ->first();
    $products_w = json_decode(json_encode($woocommerce->get('products/'.$product->wooId)),true);
    for ($i=0; $i <count($request['p_id']) ; $i++)
    {
        if($products_w['stock_quantity'] == null)
        {
            $stock_quantity = [
                'manage_stock' => true,
                'stock_quantity' => $request['received_quantity'][$i]
            ];
        }
        else {
            $stock_quantity = [
                'manage_stock' => true,
                'stock_quantity' => $request['received_quantity'][$i] + $products_w['stock_quantity']
            ];
        }
        $woocommerce->put('products/'.$product->wooId, $stock_quantity);

    }
}

function subSaleQty(array $request,$id,$status,$sid)
{
    $woocommerce        =   wooCommerce();
    $product = Products::with('unit')
    ->where('id',$id)
    ->first();
    $products_w = json_decode(json_encode($woocommerce->get('products/'.$product->wooId)),true);
    for ($i=0; $i < count($request['p_id']) ; $i++) {
        $stock_quantity = [
            'stock_quantity' => $wooproduct['stock_quantity'] - $request['delivered_quantity'][$i]
        ];
        $woocommerce->put('products/'.$product->wooId, $stock_quantity);
    }
    $sale = Sales::find($sid);
    $woocommerce->put('orders/'.$sale->order_no_w, $status);
}

function findOrder($id)
{
    $woocommerce        =   wooCommerce();
    return json_decode(json_encode($woocommerce->get('orders/'.$id)),true);
}

function findProduct($id)
{
    $woocommerce = wooCommerce();
    return json_decode( json_encode( $woocommerce->get('products/'.$id) ), true );
}

function createOrder(Request $request)
{

    $data = [
        'payment_method' => $request['pay_type'],
        'payment_method_title' =>  $request['pay_type'],
        'set_paid' => true,
        'billing' => [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'address_1' => '969 Market',
            'address_2' => '',
            'city' => 'San Francisco',
            'state' => 'CA',
            'postcode' => '94103',
            'country' => 'US',
            'email' => 'john.doe@example.com',
            'phone' => '(555) 555-5555'
        ],
        'shipping' => [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'address_1' => '969 Market',
            'address_2' => '',
            'city' => 'San Francisco',
            'state' => 'CA',
            'postcode' => '94103',
            'country' => 'US'
        ],
        'line_items' => [
            [
                'product_id' => 93,
                'quantity' => 2
            ],
            [
                'product_id' => 22,
                'variation_id' => 23,
                'quantity' => 1
            ]
        ],
        'shipping_lines' => [
            [
                'method_id' => 'flat_rate',
                'method_title' => 'Flat Rate',
                'total' => '10.00'
            ]
        ]
    ];
    $woo = $woocommerce->post('orders', $data);
    return $woo;
}



?>
