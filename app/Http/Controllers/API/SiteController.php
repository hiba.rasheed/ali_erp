<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products;
use App\Category;
use App\Subcategory;
use App\Brands;
use App\Vendors;
use App\Unit;
use App\Sales;
use App\SaleReturn;
use App\SaleCounter;
use App\Counter;
use App\SaleDetails;
use App\GeneralLedger;
use App\Bank;
use App\CurrentStock;
use App\AccountDetails;
use App\TransactionHistory;
use App\HeadCategory;
use App\Stocks;
use DB;
use Auth;
use Session;
use Carbon\Carbon;
use App\StockOut;

class SiteController extends Controller
{
    public function getProduct(Request $request, $brand = 0, $category = 0)
    {
        if($category && $brand){

            $products = Products::where([['cat_id',$category],['brand_id',$brand],['visibility',1],['status',1]])->get();
        }
        elseif ($category) {

            $products = Products::where([['cat_id',$category],['visibility',1],['status',1]])->get();
        }
        elseif ($brand) {

            $products = Products::where([['brand_id',$brand],['visibility',1],['status',1]])->get();
        }else{

            $products = Products::where([['visibility',1],['status',1]])->get();
        }
        return response()->json([
            'product'   =>  $products
        ],200);
    }

    public function getCategory()
    {
        $category = Category::where([['status',1]])->get();
        return response()->json([
            'category'   =>  $category
        ],200);
    }

    public function getSubCategory(Request $request, $category)
    {
        $category = Subcategory::where([['cat_id',$category],['status',1]])->get();
        return response()->json([
            'category'   =>  $category
        ], 200);
    }

    public function getBrand()
    {
        $brand = Brands::where([['status',1]])->get();
        return response()->json([
            'brand'   =>  $brand
        ], 200);
    }


    public function saleOrder(Request $request)
    {
        $orders = findOrder($request->order);
        $biller = Vendors::where('v_type','Biller')->where('name','montivo')->first();
        $customer = Vendors::where('v_type','Customer')->where('name','Customer')->first();
        $w_id = 1;

        if(count($orders['line_items']) > 0)
        {
            if($orders['prices_include_tax'] == false)
            {
                $tax_status = 'No';
                $tax = null;
            }
            else {
                $tax_status = 'Yes';
                $tax = $orders['total_tax'];
            }
            if($orders['status'] == 'processing')
            {
                $s_status = 'Approved';
                $p_status = 'Pending';
            }
            $data=([
                'sale_date' => Carbon::parse($orders['date_created'])->format('Y-m-d'),
                'b_id' => $biller->id,
                'c_id' => $customer->id,
                'w_id' => $w_id,
                's_address' => $orders['billing']['address_1'],
                's_status' => $s_status,
                'p_status' => $p_status,
                'tax' => $tax,
                'total' => $orders['total'],
                'advance' => 'No',
                'tax_status' => $tax_status,
                'pay_type' => 'Cash on Delivery',
                'return_status' => 'No Return',
                's_type' => 'Woo Commerce',
                'order_no_w' => $orders['id']
            ]);
            $p=Sales::create($data);
            for ($j=0; $j <  count($orders['line_items'])  ; $j++)
            {
                // $product = Products::find($request->line_items[$i]['product_id']);
                $pid = Products::where('wooId', $orders['line_items'][$j]['product_id'])->first();
                SaleDetails::create([
                    's_id' => $p->id,
                    'p_id' => $pid->id,
                    'wooId' => $orders['line_items'][$j]['product_id'],
                    'quantity' => $orders['line_items'][$j]['quantity'],
                    'sub_total' => $orders['line_items'][$j]['subtotal'],
                    'price' => $orders['line_items'][$j]['subtotal'] /  $orders['line_items'][$j]['quantity'] ,
                    'type' => 0,
                    'delivered_quantity' => null
                ]);
            }
        }
        return response()->json('sale added', 200);
    }


    public function createProduct(Request $request)
    {
        $productID = $request->p_id;
        $product = findProduct($productID);
        if($product['name'] != 'AUTO-DRAFT')
        {
            if(count($product['categories']) > 0)
            {
                $cat = Category::where('cat_name',$product['categories'][0]['name'])->first();
                if($cat == null)
                {
                    $cat = Category::create([
                        'cat_name' => $product['categories'][0]['name']
                    ]);
                }
            }

            else
            {
                $cat = Category::where('cat_name' , 'No Category')->first();
            }
            $brand = Brands::where('b_name','No Brand')->first();
            $unit = Unit::where('u_name','No Unit')->first();
            $scat = Subcategory::where('s_cat_name','No Subcategory')->first();

            $manage_stock = [
                'manage_stock' => true
            ];
            $woocommerce        =   wooCommerce();
            // return $product;
            $woocommerce->put('products/'.$productID, $manage_stock);
            $data=([
                'pro_name' => $product['name'],
                'unit_id' => $unit->id,
                'brand_id' => $brand->id,
                'cat_id' => $cat->id,
                's_cat_id' => $scat->id,
                'image' => count($product['images']) > 0 ? $product['images'][0]['src'] : null,
                'vstatus' => 0,
                'wooId' => $product['id'],
                'price' => $product['price'] == "" ? 0 : $product['price'],
            ]);
            $p = Products::create($data);
            $product = findProduct($productID);
            $p->update([
                'pro_code' => $product['sku']
            ]);
            $hcat = HeadCategory::where('name','Inventory')->first();

            $account = AccountDetails::where('c_id',$hcat->id)
            ->latest('created_at')->orderBy('id','desc')->first();

            if($account == null)
            {
                $id = 001;
            }
            else
            {
                $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
            }

            $str_length = strlen((string)$id)+2;
            $id = substr("0000{$id}", -$str_length);
            $code = $hcat->code.'-'.$id;
            $data1 = [
                'Code' => $code,
                'name_of_account' => $p->pro_code.' - '.$p->pro_name,
                'c_id' => $hcat->id,
                'type' => 0
            ];
            AccountDetails::create($data1);
            return response()->json('product added', 200);
        }
    }

    public function saleOrderStatus(Request $request)
    {
        $orders = findOrder($request->order);
        $sales = Sales::where('order_no_w',$orders['id'])->first();
        $sales->update([
            's_status' => $request->status
        ]);
        $quan = 0;
        if($request->status == 'completed')
        {
            TransactionHistory::create([
                'p_s_id' => $sales->id,
                'p_type' => 'Sales',
                't_type' => 'Received',
                'paid_by' => 'Cash',
                'total' => $sales->total
            ]);
            $saleDetails = SaleDetails::where('s_id',$sales->id)->get();

            foreach ($saleDetails as $key => $s) {
                $s->update([
                    'delivered_quantity' => $s->quantity
                ]);
                $prod=CurrentStock::where('p_id',$s->p_id)
                ->where('w_id',$sales->w_id)
                ->where('type',0)
                ->first();
                $quan=$prod->quantity -  $s->quantity;
                CurrentStock::where('p_id',$s->p_id)
                ->where('w_id',$sales->w_id)
                ->where('type',0)->update(['quantity'=>$quan]);
                // $prod->quantity = $quan;
                // $prod->save();
            }
        }
        return response()->json('status updated', 200);
    }

    public function addStock(Request $request)
    {
        $productID = $request->p_id;
        $product = Products::with('unit')
        ->where('wooId',$productID)
        ->first();
        if($product != null)
        {
            $account = AccountDetails::where('name_of_account',$product->pro_code.' - '.$product->pro_name)
            ->first();
            $posted_date = Carbon::now()->format('Y-m-d');
            $period = Carbon::now()->format('M-y');
            Stocks::create([
                'p_id' => $product->id,
                'w_id' => 1,
                'quantity' => $request->quantity,
                'stock_date' => $posted_date,
                's_type' => 'Manual',
                'cost' => $product->cost,
                'type' => 0
            ]);
            $cs=CurrentStock::where('p_id',$product->id)
            ->where('w_id',1)
            ->where('type',0)
            ->first();
            $unit_quantity = null;
            if($cs == null)
            {
                CurrentStock::create([
                    'p_id' => $product->id,
                    'w_id' => 1,
                    'quantity' => $request->quantity,
                    'unit_quantity' => $unit_quantity,
                    'type' => 0
                ]);
            }

            else
            {
                $quan = $cs->quantity + $request->quantity;
                $u_quan=null;
                CurrentStock::where('p_id',$product->id)
                ->where('w_id',1)
                ->where('type',0)
                ->update([
                    'quantity' => $quan,
                    'unit_quantity' => $u_quan
                ]);
            }
            $gl = GeneralLedger::max('id');
            if($gl == null)
            {
                $link_id = 1;
            }
            else
            {
                $ledger1 = GeneralLedger::where('id',$gl)->first();
                $link_id = $ledger1->link_id + 1;
            }
            $balance = 0;
            $net_d_p = 0;
            $debit_p = GeneralLedger::where('account_code',$account->Code)
            ->where('w_id',1)
            ->get();
            if($debit_p->isEmpty())
            {
                GeneralLedger::create([
                    'source' => 'Woo commerce',
                    'description' => 'Added product by Woo commerce',
                    'account_name' => $account->name_of_account,
                    'link_id' => $link_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account->Code,
                    'currency_code' => 'PKR',
                    'stock_in' => $request->quantity,
                    'stock_out' => '0',
                    'net_value' => $request->quantity,
                    'balance' => $request->quantity,
                    'debit' => ($product->cost * $request->quantity),
                    'credit' => 0,
                    'amount' => ($product->cost),
                    'type' => 0,
                    'w_id' => 1
                ]);
            }
            else
            {
                foreach ($debit_p as $key => $c) {
                    $balance+=$c->net_value;
                }
                $net_d_p = $balance + ( $request->quantity - 0);
                GeneralLedger::create([
                    'source' => 'Woo commerce',
                    'description' => 'Added product by Woo commerce',
                    'account_name' => $account->name_of_account,
                    'link_id' => $link_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account->Code,
                    'currency_code' => 'PKR',
                    'stock_in' => $request->quantity,
                    'stock_out' => '0',
                    'net_value' => $request->quantity,
                    'amount' => ($product->cost),
                    'balance' => $net_d_p,
                    'debit' => ($product->cost * $request->quantity),
                    'credit' => 0,
                    'type' => 0,
                    'w_id' => 1
                ]);
            }


            return response()->json('stock added');
        }
    }

}
