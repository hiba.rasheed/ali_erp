<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unit;
use DataTables;
use Auth;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('unit.index',compact('permissions'));
    }

    public function unitDatatable()
    {
        $unit=Unit::all();
        return DataTables::of($unit)->make();
    }

    public function status(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';
        $u_id = Auth::user()->id;
        $id     = $request->input('id');
        $status = $request->input('status');

        $item = Unit::find($id);
        if ($item->update(['status' => $status])) {
            Unit::where('id',$id)
            ->update([
                'status' => $status,
                'updated_by' => $u_id
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' =>false
        ];
        return view('unit.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'u_name'      =>  'required|string|max:255|unique:units'
        ]);
        $u_id = Auth::user()->id;
        Unit::create([
            'u_name' => $request->u_name,
            'created_by' => $u_id
        ]);
        toastr()->success('Unit added successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // dd($id)
        if(request()->ajax())
        {
            $unit=Unit::find($id);
            return $unit;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unit=Unit::find($id);
        $data=[
            'isEdit' => true,
            'unit' =>$unit
        ];
        return view('unit.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'u_name'      =>  'required|string|max:255|unique:units,u_name,'.$id
        ]);
        $u_id = Auth::user()->id;
        $unit=Unit::where('id',$id)
        ->update([
            'u_name' =>$request->u_name,
            'updated_by' => $u_id
        ]);
        toastr()->success('Unit updated successfully!');
        return redirect(url('').'/unit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
