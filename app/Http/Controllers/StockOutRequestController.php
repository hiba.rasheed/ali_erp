<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StockOutRequest;
use App\StockOutRequestDetails;
use App\Warehouse;
use App\Products;
use App\Unit;
use App\Brands;
use App\Category;
use App\Subcategory;
use App\Variants;
use App\ProductVariants;
use App\AccountDetails;
use App\GeneralLedger;
use App\StockOut;
use App\CurrentStock;
use Auth;
use DB;
use App\User;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;
use Carbon\Carbon;

class StockOutRequestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ware=Warehouse::all();
        $prequest=StockOutRequest::with(['warehouse','reqdetails'])
        ->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        // dd($prequest);
        return view('stockoutrequest.index',compact('prequest','ware','permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $menu_id =   getMenuId($request);
        $order=StockOutRequest::max('id');
        if($order == null)
        {
            $id=1;
        }
        else
        {
            $id=$order+1;
        }
        $ware=Warehouse::all();
        $date=Carbon::now()->format('Y-m-d');
        $product=Products::with(['brands','unit','category','subcategory'])
        ->doesntHave('finish.rawproducts')
        ->where('status',1)
        ->get();
        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $data=[
            'isEdit' => false,
            'ware' => $ware,
            'product' => $product,
            'date' => $date,
            'id' => $id,
            'unit' => $unit,
            'cat' => $cat,
            'sub' => $sub,
            'brands' => $brands,
            'permissions' => getRolePermission($menu_id)
        ];
        return view('stockoutrequest.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $role_id = Auth::user()->r_id;
        $env_a_id = env('ADMIN_ID');
        $env_m_id = env('MANAGER_ID');
        $u_id = Auth::user()->id;
        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $status = 'Approved';
        }
        else
        {
            $status = 'Pending';
        }
        $data=([
            'req_date' => $request->req_date,
            'w_id' => $request->w_id,
            'created_by' => $u_id,
            'status' => $status,
            'reason' => $request->reason,
        ]);


        $p=StockOutRequest::create($data);
        for ($i=1; $i<=count($request->p_id) ; $i++) {
            StockOutRequestDetails::create([
                'r_id' => $p->id,
                'p_id' => $request->p_id[$i],
                'quantity' => $request->quantity[$i],
                'type' => $request->type[$i]
            ]);
        }



        // if($status == 'Approved')
        // {
        //     $gl = GeneralLedger::max('id');
        //     if($gl == null)
        //     {
        //         $link_id = 1;
        //     }
        //     else
        //     {
        //         $ledger = GeneralLedger::where('id',$gl)->first();
        //         $link_id = $ledger->link_id + 1;
        //     }
        //     $posted_date = Carbon::now()->format('Y-m-d');
        //     $period = Carbon::now()->format('M-y');
        //     $product = [];
        //     $account_i = [];
        //     for ($j=1; $j <= count($request->p_id) ; $j++) {
        //         if($request->type[$j] == 0)
        //         {
        //             $product[$j] = Products::with('unit')->where('id',$request->p_id[$j])->first();
        //             $prod=CurrentStock::where('p_id',$request->p_id[$j])
        //             ->where('w_id',$request->w_id)
        //             ->where('type',$request->type[$j])
        //             ->first();

        //             if($product->unit->u_name == 'Liter')
        //             {
        //                 $unit_quantity = ( 1000) * $request->quantity[$j];
        //             }
        //             else if($product->unit->u_name == 'Mililiter')
        //             {
        //                 $unit_quantity =  $request->quantity[$j];
        //             }
        //             else
        //             {
        //                 $unit_quantity = null;
        //             }

        //             if($prod->unit_quantity == null)//changes
        //             {
        //                 $u_quan=null;
        //             }
        //             else
        //             {
        //                 $u_quan = $prod->unit_quantity - $unit_quantity;
        //             }
        //             $quan=$prod->quantity - $request->quantity[$j];
        //             CurrentStock::where('p_id',$request->p_id[$j])
        //             ->where('w_id',$request->w_id)
        //             ->where('type',$request->type[$j])
        //             ->update([
        //                 'quantity' => $quan,
        //                 'unit_quantity' => $u_quan
        //             ]);
        //         }
        //         else
        //         {
        //             $product[$j] = ProductVariants::with('product.unit')->where('p_id',$request->p_id[$j])->first();
        //             $prod=CurrentStock::where('p_id',$request->p_id[$j])
        //             ->where('w_id',$request->w_id)
        //             ->where('type',$request->type[$j])
        //             ->first();
        //             // dd($product);

        //             if($product[$j]->product->unit->u_name == 'Liter')
        //             {
        //                 $unit_quantity = ( 1000) * $request->quantity[$j];
        //             }
        //             else if($product[$j]->product->unit->u_name == 'Mililiter')
        //             {
        //                 $unit_quantity =  $request->quantity[$j];
        //             }
        //             else
        //             {
        //                 $unit_quantity = null;
        //             }

        //             if($prod->unit_quantity == null)//changes
        //             {
        //                 $u_quan=null;
        //             }
        //             else
        //             {
        //                 $u_quan = $prod->unit_quantity - $unit_quantity;
        //             }
        //             $quan=$prod->quantity - $request->quantity[$j];
        //             CurrentStock::where('p_id',$request->p_id[$j])
        //             ->where('w_id',$request->w_id)
        //             ->where('type',$request->type[$j])
        //             ->update([
        //                 'quantity' => $quan,
        //                 'unit_quantity' => $u_quan
        //             ]);
        //         }
        //     }
        //     for ($i=1; $i <= count($request->p_id) ; $i++){
        //         if($request->type[$i] == 0)
        //         {
        //             $account_i[$i] = AccountDetails::where('name_of_account',$product[$i]->pro_code.' - '.$product[$i]->pro_name)
        //             ->first();
        //         }
        //         else
        //         {

        //             $account_i[$i] = AccountDetails::where('name_of_account',$product[$i]->name)
        //             ->first();
        //         }
        //     }
        //     for ($k=1; $k <= count($account_i) ; $k++) {
        //         $balance = 0;
        //         $net_d_p = 0;
        //         $debit_p = GeneralLedger::where('account_code',$account_i[$k]->Code)
        //         ->where('w_id',$request->w_id)
        //         ->get();
        //         if($debit_p->isEmpty())
        //         {
        //             GeneralLedger::create([
        //                 'source' => 'Automated',
        //                 'description' => 'Stock out product for stock out request number: '.$p->id,
        //                 'account_name' => $account_i[$k]->name_of_account,
        //                 'link_id' => $link_id,
        //                 'created_by' => $u_id,
        //                 'accounting_date' => $posted_date,
        //                 'posted_date' => $posted_date,
        //                 'period' => $period,
        //                 'account_code' => $account_i[$k]->Code,
        //                 'transaction_no' => $p->id,
        //                 'currency_code' => 'PKR',
        //                 'stock_out' => $request->quantity[$k],
        //                 'stock_in' => '0',
        //                 'net_value' => 0 - $request->quantity[$k],
        //                 'balance' => 0 - $request->quantity[$k],
        //                 'credit' => $product[$k]->cost * $request->quantity[$k],
        //                 'debit' => 0,
        //                 'amount' => 0 - $product[$k]->cost,
        //                 'type' => $request->type[$k],
        //                 'w_id' => $request->w_id
        //             ]);
        //         }
        //         else
        //         {
        //             foreach ($debit_p as $key => $c) {
        //                 $balance+=$c->net_value;
        //             }
        //             $net_d_p = $balance + ( 0 - $request->quantity[$k] );
        //             GeneralLedger::create([
        //                 'source' => 'Automated',
        //                 'description' => 'Stock out product for stock out request number: '.$p->id,
        //                 'account_name' => $account_i[$k]->name_of_account,
        //                 'link_id' => $link_id,
        //                 'created_by' => $u_id,
        //                 'accounting_date' => $posted_date,
        //                 'posted_date' => $posted_date,
        //                 'period' => $period,
        //                 'account_code' => $account_i[$k]->Code,
        //                 'transaction_no' => $p->id,
        //                 'currency_code' => 'PKR',
        //                 'stock_out' => $request->quantity[$k],
        //                 'stock_in' => '0',
        //                 'net_value' => 0 - $request->quantity[$k],
        //                 'balance' => $net_d_p,
        //                 'credit' => $product[$k]->cost  * $request->quantity[$k],
        //                 'debit' => 0,
        //                 'amount' => 0 - $product[$k]->cost,
        //                 'type' => $request->type[$k],
        //                 'w_id' => $request->w_id
        //             ]);
        //         }

        //         StockOut::create([
        //             'p_id' => $request->p_id[$k],
        //             'w_id' => $request->w_id,
        //             'quantity' => $request->quantity[$k],
        //             'stock_date' => $request->req_date,
        //             's_type' => 'Request',
        //             'price' => $product[$k]->price,
        //             'created_by' => $u_id,
        //             'type' => $request->type[$k]
        //         ]);
        //     }
        // }

        $u_name = Auth::user()->name;
        $user = User::where('r_id',env('ADMIN_ID'))->get();
        $data1 = [
            'notification' => 'New Stockout request has been added by '.$u_name,
            'link' => url('').'/stockoutRequest',
            'name' => 'View Stockout Requests',
        ];
        Notification::send($user, new AddNotification($data1));
        toastr()->success('Stockout Request created successfully!');
        return redirect(url('').'/stockoutRequest');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $p=StockOutRequest::with(['warehouse'])
        ->where('id',$id)
        ->first();
        $d=StockOutRequestDetails::with(['variant.product','variant.currentstocks','products.currentstocks','request'])
        ->where('r_id',$id)
        ->get();
        return [$p,$d];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $req=StockOutRequest::with(['warehouse'])
        ->where('id',$id)
        ->first();
        $rdetail=StockOutRequestDetails::with(['variant.currentstocks','products.currentstocks','products','products.brands','products.category','products.unit','request'])
        ->where('r_id',$id)
        ->get();
        // dd($rdetail);
        $ware=Warehouse::all();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $unit=Unit::where('status',1)->get();
        $product=Products::with(['brands','unit','category','subcategory'])
        ->doesntHave('finish.rawproducts')
        ->where('status',1)
        ->get();
        $data=[
            'isEdit' => true,
            'ware' => $ware,
            'product' => $product,
            'id' => $id,
            'req' => $req,
            'unit' => $unit,
            'brands' => $brands,
            'cat' => $cat,
            'sub' => $sub,
            'rdetail' => $rdetail
        ];
        return view('stockoutrequest.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $u_id = Auth::user()->id;
        StockOutRequest::where('id',$id)
        ->update([
            'w_id' => $request->w_id,
            'updated_by' => $u_id
        ]);
        DB::table('purchase_request_details')->where('o_id', $id)->delete();
        for ($i=1; $i<=count($request->p_id) ; $i++) {
            StockOutRequestDetails::create([
                'o_id' => $id,
                'p_id' => $request->p_id[$i],
                'quantity' => $request->quantity[$i],
                'type' => $request->type[$i]
            ]);
        }

        toastr()->success('Stockout Request updated successfully!');
        return redirect(url('').'/stockoutRequest');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delivery(Request $request)
    {
        // dd($request->all());
        $id = $request->o_no1;
        $u_id = Auth::user()->id;
        $item = StockOutRequest::find($id);
        StockOutRequest::where('id',$id)
        ->update([
            'status' => 'Delivered',
            'updated_by' => $u_id
        ]);
        $detail = StockOutRequestDetails::where('r_id',$id)->get();
        $gl = GeneralLedger::max('id');
        // dd($detail);
        if($gl == null)
        {
            $link_id = 1;
        }
        else
        {
            $ledger = GeneralLedger::where('id',$gl)->first();
            $link_id = $ledger->link_id + 1;
        }
        $posted_date = Carbon::now()->format('Y-m-d');
        $period = Carbon::now()->format('M-y');
        $product = [];
        $account_i = [];
        for ($j=0; $j < count($detail) ; $j++) {
            if($detail[$j]->type == 0)
            {
                $product[$j] = Products::with('unit')->where('id',$detail[$j]->p_id)->first();
                $prod=CurrentStock::where('p_id',$detail[$j]->p_id)
                ->where('w_id',$item->w_id)
                ->where('type',$detail[$j]->type)
                ->first();

                if($product[$j]->unit->u_name == 'Liter')
                {
                    $unit_quantity = ( 1000) * $detail[$j]->quantity;
                }
                else if($product[$j]->unit->u_name == 'Mililiter')
                {
                    $unit_quantity =  $detail[$j]->quantity;
                }
                else
                {
                    $unit_quantity = null;
                }

                if($prod->unit_quantity == null)//changes
                {
                    $u_quan=null;
                }
                else
                {
                    $u_quan = $prod->unit_quantity - $unit_quantity;
                }
                $quan=$prod->quantity - $detail[$j]->quantity;
                // dd($quan,$prod);
                CurrentStock::where('p_id',$detail[$j]->p_id)
                ->where('w_id',$item->w_id)
                ->where('type',$detail[$j]->type)
                ->update([
                    'quantity' => $quan,
                    'unit_quantity' => $u_quan
                ]);
            }
            else
            {
                $product[$j] = ProductVariants::with('product.unit')->where('p_id',$detail[$j]->p_id)->first();
                $prod=CurrentStock::where('p_id',$detail[$j]->p_id)
                ->where('w_id',$item->w_id)
                ->where('type',$detail[$j]->type)
                ->first();


                if($product[$j]->product->unit->u_name == 'Liter')
                {
                    $unit_quantity = ( 1000) * $detail[$j]->quantity;
                }
                else if($product[$j]->product->unit->u_name == 'Mililiter')
                {
                    $unit_quantity =  $detail[$j]->quantity;
                }
                else
                {
                    $unit_quantity = null;
                }

                if($prod->unit_quantity == null)//changes
                {
                    $u_quan=null;
                }
                else
                {
                    $u_quan = $prod->unit_quantity - $unit_quantity;
                }
                $quan=$prod->quantity - $detail[$j]->quantity;
                $q = CurrentStock::where('p_id',$detail[$j]->p_id)
                ->where('w_id',$item->w_id)
                ->where('type',$detail[$j]->type)
                ->update([
                    'quantity' => $quan,
                    'unit_quantity' => $u_quan
                ]);
                $prod=CurrentStock::where('p_id',$detail[$j]->p_id)
                ->where('w_id',$item->w_id)
                ->where('type',$detail[$j]->type)
                ->first();
            }
        }
        for ($i=0; $i < count($detail) ; $i++){
            if($detail[$i]->type == 0)
            {
                $account_i[$i] = AccountDetails::where('name_of_account',$product[$i]->pro_code.' - '.$product[$i]->pro_name)
                ->first();
            }
            else
            {

                $account_i[$i] = AccountDetails::where('name_of_account',$product[$i]->name)
                ->first();
            }
        }
        for ($k=0; $k < count($account_i) ; $k++) {
            $balance = 0;
            $net_d_p = 0;
            $debit_p = GeneralLedger::where('account_code',$account_i[$k]->Code)
            ->where('w_id',$item->w_id)
            ->get();
            if($debit_p->isEmpty())
            {
                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Stock out product for stock out request number: '.$id,
                    'account_name' => $account_i[$k]->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_i[$k]->Code,
                    'transaction_no' => $id,
                    'currency_code' => 'PKR',
                    'stock_out' => $detail[$k]->quantity,
                    'stock_in' => '0',
                    'net_value' => 0 - $detail[$k]->quantity,
                    'balance' => 0 - $detail[$k]->quantity,
                    'credit' => $product[$k]->cost * $detail[$k]->quantity,
                    'debit' => 0,
                    'amount' => 0 - $product[$k]->cost,
                    'type' => $detail[$k]->type,
                    'w_id' => $item->w_id
                ]);
            }
            else
            {
                foreach ($debit_p as $key => $c) {
                    $balance+=$c->net_value;
                }
                $net_d_p = $balance + ( 0 - $detail[$k]->quantity );
                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Stock out product for stock out request number: '.$id,
                    'account_name' => $account_i[$k]->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_i[$k]->Code,
                    'transaction_no' => $id,
                    'currency_code' => 'PKR',
                    'stock_out' => $detail[$k]->quantity,
                    'stock_in' => '0',
                    'net_value' => 0 - $detail[$k]->quantity,
                    'balance' => $net_d_p,
                    'credit' => $product[$k]->cost  * $detail[$k]->quantity,
                    'debit' => 0,
                    'amount' => 0 - $product[$k]->cost,
                    'type' => $detail[$k]->type,
                    'w_id' => $item->w_id
                ]);
            }

            StockOut::create([
                'p_id' => $detail[$k]->p_id,
                'w_id' => $item->w_id,
                'quantity' => $detail[$k]->quantity,
                'stock_date' => $posted_date,
                's_type' => 'Request',
                'price' => $product[$k]->price,
                'created_by' => $u_id,
                'type' => $detail[$k]->type
            ]);
        }
        return redirect()->back();
    }


    public function status(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
        $u_id = Auth::user()->id;

        if($status == 'Approved')
        {
            $item = StockOutRequest::find($id);
            if ($item->update(['status' => $status])) {
                StockOutRequest::where('id',$id)
                ->update([
                    'status' => $status,
                    'updated_by' => $u_id
                ]);

            }




            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);

        }
        else
        {
            return response()->json($response, 409);
        }
    }
}
