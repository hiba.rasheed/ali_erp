<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stocks;
use App\StockReceiving;
use App\StockOut;
use App\CurrentStock;
use App\Products;
use App\ProductVariants;
use App\Category;
use App\Brands;
use App\AccountDetails;
use App\GeneralLedger;
use App\Vendors;
use App\Warehouse;
use App\FinishProducts;
use DataTables;
use Carbon\Carbon;
use DB;
use Auth;
use App\Exports\CurrentStockExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        $product=Products::with(['brands','category','variants'])
        ->where('status',1)
        ->get();

        $supplier=Vendors::where('v_type','Supplier')->where('status',1)->get();
        $stock=Stocks::with(['products.brands','warehouse','pdetails','products.category','products.unit','products','supplier','variant.product.category','variant.product.unit','variant.product.brands'])
        ->get();
        $index = 0;
        // return listProduct();
        return view('stocks.index',compact('index','product','supplier','permissions','stock','menu_id'));
    }

    public function current(Request $request)
    {
        $warehouse=Warehouse::all();
        $product=Products::with(['brands','category','variants'])
        ->where('status',1)
        ->get();

        $brand=Brands::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $stock=CurrentStock::with(['warehouse','stock','sales','products.brands','products.category','products.unit','products','variant.product.category','variant.product.unit','variant.product.brands'])
        ->get();
        // return $stock;

        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        // dd($stock);
        return view('stocks.current',compact('warehouse','product','brand','cat','stock','permissions'));
    }

    public function OC_inventory(Request $request)
    {
        $warehouse          =   Warehouse::all();
        $product            =   Products::with(['brands','category','variants'])->where('status',1)->get();
        $brand              =   Brands::where('status',1)->get();
        $cat                =   Category::where('status',1)->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);

        if(isset($request->optradio))
        {
            // dd($request->all());
            $year = $request->year;
            $lastYear = date('Y',strtotime($year.' -1 year'));
            $index = 1;
            //last year closing = todays opening

            $stockin = DB::Select("select w.w_name, w.w_type, p.name as name,ps.pro_name as pro_name,ps.pro_code as code, ps.p_type, s.p_id, s.type, s.w_id, sum(s.quantity) as quantity
            from stocks as s
            Inner join product_variants as p on p.id = s.p_id
            inner join warehouse as w on s.w_id = w.id
            inner join products as ps on s.p_id = ps.id
            where date_format(s.stock_date,'%Y') <= '".$lastYear."'
            group by s.p_id,s.w_id,p.name,s.type, w.w_name, w.w_type,ps.p_type, s.type,ps.pro_name,ps.pro_code");


            $stockout = DB::Select("select w.w_name, w.w_type, p.name as name,ps.pro_name as pro_name,ps.pro_code as code, ps.p_type, s.p_id, s.type, s.w_id, sum(s.quantity) as quantity
            from stock_out as s
            Inner join product_variants as p on p.id = s.p_id
            inner join warehouse as w on s.w_id = w.id
            inner join products as ps on s.p_id = ps.id
            where date_format(s.stock_date,'%Y') <= '".$lastYear."'
            group by s.p_id,s.w_id,p.name,s.type, w.w_name, w.w_type,ps.p_type, s.type,ps.pro_name,ps.pro_code");

            //year stock in
            $stock_in     = DB::Select("select w.w_name, w.w_type,p.name as name,ps.pro_name as pro_name,ps.pro_code as code, ps.p_type, s.p_id, s.type, s.w_id, (sum(s.quantity)) as quantity
            from stocks as s
            Inner join product_variants as p on p.id = s.p_id
            inner join warehouse as w on s.w_id = w.id
            inner join products as ps on s.p_id = ps.id
            where date_format(s.stock_date,'%Y') <= '".$year."'
            group by s.p_id,s.w_id,p.name,s.type, w.w_name, w.w_type,ps.p_type, s.type,ps.pro_name,ps.pro_code");

            //year stockout

            $stock_out     = DB::Select("select w.w_name, w.w_type,p.name as name,ps.pro_name as pro_name,ps.pro_code as code, ps.p_type, s.p_id, s.type, s.w_id, (sum(s.quantity)) as quantity
            from stock_out as s
            Inner join product_variants as p on p.id = s.p_id
            inner join warehouse as w on s.w_id = w.id
            inner join products as ps on s.p_id = ps.id
            where date_format(s.stock_date,'%Y') <= '".$year."'
            group by s.p_id,s.w_id,p.name,s.type, w.w_name, w.w_type,ps.p_type, s.type,ps.pro_name,ps.pro_code");

            return view('stocks.OC_inventory',compact('index','warehouse','product','brand','cat', 'menu_id','permissions','stock_out','stockin','stockout','stock_in'));
        }
        else
        {
            $yesterdays = Carbon::yesterday()->format('Y-m-d');
            $todays = Carbon::now()->format('Y-m-d');
            // dd($yesterdays);

            // stock in
            $stock_in     = DB::Select("select w.w_name,s.stock_date, w.w_type, s.p_id, s.type, s.w_id, (sum(s.quantity)) as quantity
            from stocks as s
            inner join warehouse as w on s.w_id = w.id
            where s.stock_date <= '".$todays."'
            group by s.p_id,s.w_id,s.type, w.w_name, w.w_type,s.stock_date");

            //stockout

            $stock_out     = DB::Select("select w.w_name,s.stock_date, w.w_type,s.p_id, s.type, s.w_id, (sum(s.quantity)) as quantity
            from stock_out as s
            inner join warehouse as w on s.w_id = w.id
            where s.stock_date <= '".$todays."'
            group by s.p_id,s.w_id,s.type, w.w_name, w.w_type,s.stock_date");
            // return count($open_stock_in);
            // return [count($stock_in),count($open_stock_in)];
            $index = 0;

            return view('stocks.OC_inventory',compact('index','warehouse','product','brand','cat', 'menu_id','permissions','stock_out','stock_in'));
        }


    }
    public function pdf()
    {
        $stock=CurrentStock::with(['warehouse','stock','sales','products.brands','products.category','products.unit','products','variant.product.category','variant.product.unit','variant.product.brands'])
        ->get();
        $year = Carbon::now()->format('Y');
        $pdf = PDF::loadView('stocks.pdf', compact('stock','year'));

        return $pdf->download('Currentstocksreport.pdf');
    }

    public function excel()
    {
        return Excel::download(new CurrentStockExport, 'CurrentStockReport.xlsx');
    }

    public function stockInSearch(Request $request)
    {
        $menu_id            =   $request->menuid;
        $permissions        =   getRolePermission($menu_id);
        $product=Products::with(['brands','category','variants'])
        ->where('status',1)
        ->get();
        $supplier=Vendors::where('v_type','Supplier')->where('status',1)->get();
        if($request->optradio == 'Year')
        {
            if($request->p_id == null && $request->s_id == null)
            {
                $stock=Stocks::with(['variant.product.brands','products.brands','warehouse','pdetails','products.category','variant.product.category','products.unit','variant.product.unit','products','supplier'])
                ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y'))"),$request->year)
                ->orderBy('stock_date','desc')
                ->get();
            }
            if($request->p_id != null && $request->s_id == null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $stock=Stocks::with(['variant.product.brands','products.brands','warehouse','pdetails','products.category','variant.product.category','products.unit','variant.product.unit','products','supplier'])
                ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y'))"),$request->year)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->orderBy('stock_date','desc')
                ->get();
            }
            if($request->p_id == null && $request->s_id != null)
            {
                $stock=Stocks::with(['variant.product.brands','products.brands','warehouse','pdetails','products.category','variant.product.category','products.unit','variant.product.unit','products','supplier'])
                ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y'))"),$request->year)
                ->where('s_id',$request->s_id)
                ->orderBy('stock_date','desc')
                ->get();
            }
            if($request->p_id != null && $request->s_id != null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $stock=Stocks::with(['variant.product.brands','products.brands','warehouse','pdetails','products.category','variant.product.category','products.unit','variant.product.unit','products','supplier'])
                ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y'))"),$request->year)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->where('s_id',$request->s_id)
                ->orderBy('stock_date','desc')
                ->get();
            }
        }
        else if($request->optradio == 'Month')
        {
            if($request->p_id == null && $request->s_id == null)
            {
                $stock=Stocks::with(['variant.product.brands','products.brands','warehouse','pdetails','products.category','variant.product.category','products.unit','variant.product.unit','products','supplier'])
                ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m'))"),$request->month)
                ->orderBy('stock_date','desc')
                ->get();
            }
            if($request->p_id != null && $request->s_id == null)
            {

                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $stock=Stocks::with(['variant.product.brands','products.brands','warehouse','pdetails','products.category','variant.product.category','products.unit','variant.product.unit','products','supplier'])
                ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m'))"),$request->month)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->orderBy('stock_date','desc')
                ->get();
            }
            if($request->p_id == null && $request->s_id != null)
            {
                $stock=Stocks::with(['variant.product.brands','products.brands','warehouse','pdetails','products.category','variant.product.category','products.unit','variant.product.unit','products','supplier'])
                ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m'))"),$request->month)
                ->where('s_id',$request->s_id)
                ->orderBy('stock_date','desc')
                ->get();
            }
            if($request->p_id != null && $request->s_id != null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $stock=Stocks::with(['variant.product.brands','products.brands','warehouse','pdetails','products.category','variant.product.category','products.unit','variant.product.unit','products','supplier'])
                ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m'))"),$request->month)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->where('s_id',$request->s_id)
                ->orderBy('stock_date','desc')
                ->get();
            }
        }
        else if($request->optradio == 'Date')
        {
            if($request->from != null && $request->to != null)
            {
                $from = $request->from;
                $to = $request->to;
                if($request->p_id == null && $request->s_id == null)
                {
                    $stock=Stocks::with(['variant.product.brands','products.brands','warehouse','pdetails','products.category','variant.product.category','products.unit','variant.product.unit','products','supplier'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->orderBy('stock_date','desc')
                    ->get();
                }
                if($request->p_id != null && $request->s_id == null)
                {
                    $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                    $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                    $stock=Stocks::with(['variant.product.brands','products.brands','warehouse','pdetails','products.category','variant.product.category','products.unit','variant.product.unit','products','supplier'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_id',$p_id)
                    ->where('type',$type)
                    ->orderBy('stock_date','desc')
                    ->get();
                }
                if($request->p_id == null && $request->s_id != null)
                {
                    $stock=Stocks::with(['variant.product.brands','products.brands','warehouse','pdetails','products.category','variant.product.category','products.unit','variant.product.unit','products','supplier'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('s_id',$request->s_id)
                    ->orderBy('stock_date','desc')
                    ->get();
                }
                if($request->p_id != null && $request->s_id != null)
                {
                    $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                    $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                    $stock=Stocks::with(['variant.product.brands','products.brands','warehouse','pdetails','products.category','variant.product.category','products.unit','variant.product.unit','products','supplier'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_id',$p_id)
                    ->where('type',$type)
                    ->where('s_id',$request->s_id)
                    ->orderBy('stock_date','desc')
                    ->get();
                }
            }
            else
            {
                if($request->to == null)
                {
                    $date = $request->from;
                }
                if($request->from == null)
                {
                    $date = $request->to;
                }

                if($request->p_id == null && $request->s_id == null)
                {
                    $stock=Stocks::with(['variant.product.brands','products.brands','warehouse','pdetails','products.category','variant.product.category','products.unit','variant.product.unit','products','supplier'])
                    ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m-%d'))"),$date)
                    ->orderBy('stock_date','desc')
                    ->get();
                }
                if($request->p_id != null && $request->s_id == null)
                {
                    $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                    $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                    $stock=Stocks::with(['variant.product.brands','products.brands','warehouse','pdetails','products.category','variant.product.category','products.unit','variant.product.unit','products','supplier'])
                    ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m-%d'))"),$date)
                    ->where('p_id',$p_id)
                    ->where('type',$type)
                    ->orderBy('stock_date','desc')
                    ->get();
                }
                if($request->p_id == null && $request->s_id != null)
                {
                    $stock=Stocks::with(['variant.product.brands','products.brands','warehouse','pdetails','products.category','variant.product.category','products.unit','variant.product.unit','products','supplier'])
                    ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m-%d'))"),$date)
                    ->where('s_id',$request->s_id)
                    ->orderBy('stock_date','desc')
                    ->get();
                }
                if($request->p_id != null && $request->s_id != null)
                {
                    $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                    $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                    $stock=Stocks::with(['variant.product.brands','products.brands','warehouse','pdetails','products.category','variant.product.category','products.unit','variant.product.unit','products','supplier'])
                    ->where(DB::raw("(DATE_FORMAT(stock_date,'%Y-%m-%d'))"),$date)
                    ->where('p_id',$p_id)
                    ->where('type',$type)
                    ->where('s_id',$request->s_id)
                    ->orderBy('stock_date','desc')
                    ->get();
                }
            }

        }

        return view('stocks.index',compact('product','supplier','permissions','stock','menu_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $products=Products::with(['brands','category','variants'])
        ->where('status',1)
        ->get();

        // dd($products);
        $warehouse=Warehouse::all();
        $date=Carbon::now()->format('Y-m-d');
        $data=[
            'isEdit' => false,
            'products' => $products,
            'warehouse' => $warehouse,
            'date' => $date
        ];
        return view('stocks.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $type =  substr($request->p_id,  strpos($request->p_id, '-')+1);
        $id =  substr($request->p_id, 0, strpos($request->p_id, '-'));
        $u_id = Auth::user()->id;
        if($type==1)
        {
            $product = ProductVariants::with(['product.unit'])->where('id',$id)->first();
            $ptype= $product->product->p_type;
            $unit = $product->product->unit->u_name;
            // $weight = $product->product->weight;
            $account = AccountDetails::where('name_of_account',$product->name)
            ->first();
        }
        else {
            $product = Products::with('unit')
            ->where('id',$id)
            ->first();
            $ptype=$product->p_type;
            $unit = $product->unit->u_name;
            // $weight = $product->weight;
            $account = AccountDetails::where('name_of_account',$product->pro_code.' - '.$product->pro_name)
            ->first();




        }

        $posted_date = Carbon::now()->format('Y-m-d');
        $period = Carbon::now()->format('M-y');

        addStock($request->all(),$id);
        if($ptype != 'Finished' )
        {
            // dd('2332');
            Stocks::create([
                'p_id' => $id,
                'w_id' => $request->w_id,
                'quantity' => $request->quantity,
                'stock_date' => $request->stock_date,
                's_type' => 'Manual',
                'cost' => $request->cost,
                'created_by' => $u_id,
                'type' => $type
            ]);

            if($type == 0)
            {
                Products::where('id',$id)
                ->update([
                    'cost' => $request->cost,
                    'updated_by' => $u_id
                ]);
            }
            else {
                ProductVariants::where('id',$id)
                ->update([
                    'cost' => $request->cost,
                ]);
            }
            $cs=CurrentStock::where('p_id',$id)
            ->where('w_id',$request->w_id)
            ->where('type',$type)
            ->first();
            if($unit == 'Liter')
            {
                $unit_quantity = (1000) * $request->quantity;
            }
            else if($unit == 'Mililiter')
            {
                $unit_quantity =  $request->quantity;
            }
            else
            {
                $unit_quantity = null;
            }
            if($cs == null)
            {
                CurrentStock::create([
                    'p_id' => $id,
                    'w_id' => $request->w_id,
                    'quantity' => $request->quantity,
                    'unit_quantity' => $unit_quantity,
                    'type' => $type
                ]);
            }

            else
            {
                $quan = $cs->quantity + $request->quantity;
                if($unit_quantity == null)
                {
                    $u_quan=null;
                }
                else
                {
                    $u_quan = $cs->unit_quantity + $unit_quantity;
                }
                CurrentStock::where('p_id',$id)
                ->where('w_id',$request->w_id)
                ->where('type',$type)
                ->update([
                    'quantity' => $quan,
                    'unit_quantity' => $u_quan
                ]);
            }

            $gl = GeneralLedger::max('id');
            if($gl == null)
            {
                $link_id = 1;
            }
            else
            {
                $ledger1 = GeneralLedger::where('id',$gl)->first();
                $link_id = $ledger1->link_id + 1;
            }
            $balance = 0;
            $net_d_p = 0;
            $debit_p = GeneralLedger::where('account_code',$account->Code)
            ->where('w_id',$request->w_id)
            ->get();
            if($debit_p->isEmpty())
            {
                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Added product manually',
                    'account_name' => $account->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account->Code,
                    'currency_code' => 'PKR',
                    'stock_in' => $request->quantity,
                    'stock_out' => '0',
                    'net_value' => $request->quantity,
                    'balance' => $request->quantity,
                    'debit' => ($request->cost * $request->quantity),
                    'credit' => 0,
                    'amount' => ($request->cost),
                    'type' => $type,
                    'w_id' => $request->w_id
                ]);
            }
            else
            {
                foreach ($debit_p as $key => $c) {
                    $balance+=$c->net_value;
                }
                $net_d_p = $balance + ( $request->quantity - 0);
                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Added product manually',
                    'account_name' => $account->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account->Code,
                    'currency_code' => 'PKR',
                    'stock_in' => $request->quantity,
                    'stock_out' => '0',
                    'net_value' => $request->quantity,
                    'amount' => ($request->cost),
                    'balance' => $net_d_p,
                    'debit' => ($request->cost * $request->quantity),
                    'credit' => 0,
                    'type' => $type,
                    'w_id' => $request->w_id
                ]);
            }




            toastr()->success('Stock added successfully!');
            return redirect()->back();
        }
        else
        {
            // dd('fdfd');
            $status  = false;
            if($type == 0)
            {
                // dd('dffd');
                $fp = FinishProducts::with('rawproducts.unit','rawproducts2.product.unit')
                ->where('id',$id)
                ->get();
                // dd($fp);

                if($fp->isEmpty())
                {
                    // dd('dada');
                    Stocks::create([
                        'p_id' => $id,
                        'w_id' => $request->w_id,
                        'quantity' => $request->quantity,
                        'stock_date' => $request->stock_date,
                        's_type' => 'Manual',
                        'cost' => $request->cost,
                        'created_by' => $u_id,
                        'type' => $type
                    ]);

                    Products::where('id',$id)
                    ->update([
                        'cost' => $request->cost,
                        'updated_by' => $u_id
                    ]);

                    $cs=CurrentStock::where('p_id',$id)
                    ->where('w_id',$request->w_id)
                    ->where('type',$type)
                    ->first();
                    if($product->unit->u_name == 'Liter')
                    {
                        $unit_quantity = (1000) * $request->quantity;
                    }
                    else if($product->unit->u_name == 'Mililiter')
                    {
                        $unit_quantity = $request->quantity;
                    }
                    else
                    {
                        $unit_quantity = null;
                    }
                    if($cs == null)
                    {
                        CurrentStock::create([
                            'p_id' => $id,
                            'w_id' => $request->w_id,
                            'quantity' => $request->quantity,
                            'unit_quantity' => $unit_quantity,
                            'type' => $type
                        ]);
                    }

                    else
                    {
                        $quan = $cs->quantity + $request->quantity;
                        if($unit_quantity == null)
                        {
                            $u_quan=null;
                        }
                        else
                        {
                            $u_quan = $cs->unit_quantity + $unit_quantity;
                        }
                        CurrentStock::where('p_id',$id)
                        ->where('w_id',$request->w_id)
                        ->where('type',$type)
                        ->update([
                            'quantity' => $quan,
                            'unit_quantity' => $u_quan
                        ]);
                    }

                    $gl = GeneralLedger::max('id');
                    if($gl == null)
                    {
                        $link_id = 1;
                    }
                    else
                    {
                        $ledger1 = GeneralLedger::where('id',$gl)->first();
                        $link_id = $ledger1->link_id + 1;
                    }
                    $balance = 0;
                    $net_d_p = 0;
                    $debit_p = GeneralLedger::where('account_code',$account->Code)
                    ->where('w_id',$request->w_id)
                    ->get();
                    if($debit_p->isEmpty())
                    {
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Added product manually',
                            'account_name' => $account->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account->Code,
                            'currency_code' => 'PKR',
                            'stock_in' => $request->quantity,
                            'stock_out' => '0',
                            'net_value' => $request->quantity,
                            'balance' => $request->quantity,
                            'debit' => ($request->cost * $request->quantity),
                            'credit' => 0,
                            'amount' => ($request->cost),
                            'type' => $type,
                            'w_id' => $request->w_id
                        ]);
                    }
                    else
                    {
                        foreach ($debit_p as $key => $c) {
                            $balance+=$c->net_value;
                        }
                        $net_d_p = $balance + ( $request->quantity - 0);
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Added product manually',
                            'account_name' => $account->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account->Code,
                            'currency_code' => 'PKR',
                            'stock_in' => $request->quantity,
                            'stock_out' => '0',
                            'net_value' => $request->quantity,
                            'amount' => ($request->cost),
                            'balance' => $net_d_p,
                            'debit' => ($request->cost * $request->quantity),
                            'credit' => 0,
                            'type' => $type,
                            'w_id' => $request->w_id
                        ]);
                    }

                    toastr()->success('Stock added successfully!');
                    return redirect()->back();
                }
                else
                {
                    // dd('ffffff');
                    for ($i=0; $i <count($fp) ; $i++) {
                        $cs = CurrentStock::with('products','products.unit','variant.product')
                        ->where('p_id',$fp[$i]->p_id)
                        ->where('w_id',$request->w_id)
                        ->where('type',$fp[$i]->type)
                        ->first();
                        // dd($cs);
                        if($fp[$i]->type == 1 ? $fp[$i]->rawproducts2->product->unit->u_name == 'Mililiter' : $fp[$i]->rawproducts->unit->u_name == 'Mililiter')
                        {
                            $final = $request->quantity * $fp[$i]->quantity;
                            // dd($final.'3');
                            if($cs->unit_quantity < $final)
                            {
                                $status = true;
                                break;
                            }
                            else
                            {
                                $status = false;
                            }
                        }

                        else if($fp[$i]->type == 1 ? $fp[$i]->rawproducts2->product->unit->u_name == 'Liter' : $fp[$i]->rawproducts->unit->u_name == 'Liter')
                        {
                            $final = $request->quantity *  $fp[$i]->quantity ;
                            // dd($final.'1', $cs->unit_quantity);
                            if($cs->unit_quantity < $final)
                            {
                                $status = true;
                                break;
                            }
                            else
                            {
                                $status = false;
                            }

                        }
                        else
                        {
                            $final = $fp[$i]->quantity * $request->quantity;
                            // dd($final);
                            if($cs->quantity < $final)
                            {
                                $status = true;
                                break;
                            }
                            else
                            {
                                $status = false;
                            }
                        }
                    }
                    // dd($final);
                    if($status == true)
                    {
                        toastr()->warning('There is not enough stock!');
                        return redirect()->back();
                    }
                    else
                    {
                        // dd($final);
                        Stocks::create([
                            'p_id' => $id,
                            'w_id' => $request->w_id,
                            'quantity' => $request->quantity,
                            'stock_date' => $request->stock_date,
                            's_type' => 'Manual',
                            'cost' => $request->cost,
                            'created_by' => $u_id,
                            'type' => $type
                        ]);
                        //for woocommerce
                        // addStock($request->all(),$id);

                            // dd($account);
                        $gl = GeneralLedger::max('id');
                        if($gl == null)
                        {
                            $link_id = 1;
                        }
                        else
                        {
                            $ledger1 = GeneralLedger::where('id',$gl)->first();
                            $link_id = $ledger1->link_id + 1;
                        }

                        $balance = 0;
                        $net_d_p = 0;
                        $debit_p = GeneralLedger::where('account_code',$account->Code)
                        ->where('w_id',$request->w_id)
                        ->get();
                        if($debit_p->isEmpty())
                        {
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Added product manually',
                                'account_name' => $account->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account->Code,
                                'currency_code' => 'PKR',
                                'stock_in' => $request->quantity,
                                'stock_out' => '0',
                                'net_value' => $request->quantity,
                                'balance' => $request->quantity,
                                'debit' => ($request->cost * $request->quantity),
                                'credit' => 0,
                                'amount' => ($request->cost),
                                'type' => $type,
                                'w_id' => $request->w_id
                            ]);
                        }
                        else
                        {
                            foreach ($debit_p as $key => $value) {
                                $balance+=$value->net_value;
                            }
                            $net_d_p = $balance + ( $request->quantity - 0 );
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Added product manually',
                                'account_name' => $account->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account->Code,
                                'currency_code' => 'PKR',
                                'stock_in' => $request->quantity,
                                'stock_out' => '0',
                                'net_value' => $request->quantity,
                                'balance' => $net_d_p,
                                'debit' => ($request->cost * $request->quantity),
                                'credit' => 0,
                                'amount' => ($request->cost),
                                'type' => $type,
                                'w_id' => $request->w_id
                            ]);
                        }


                        Products::where('id',$id)
                        ->update([
                            'cost' => $request->cost,
                            'updated_by' => $u_id
                        ]);
                        if($unit == 'Liter')
                        {
                            $unit_quantity = (1000) * $request->quantity;
                        }
                        else if($unit == 'Mililiter')
                        {
                            $unit_quantity =  $request->quantity;
                        }
                        else
                        {
                            $unit_quantity = null;
                        }
                        $cs=CurrentStock::where('p_id',$id)
                        ->where('w_id',$request->w_id)
                        ->where('type',$type)
                        ->first();
                        if($cs == null)
                        {
                            CurrentStock::create([
                                'p_id' => $id,
                                'w_id' => $request->w_id,
                                'quantity' => $request->quantity,
                                'unit_quantity' => $unit_quantity,
                                'type' => $type,
                            ]);
                        }
                        else
                        {
                            $quan = $cs->quantity + $request->quantity;
                            CurrentStock::where('p_id',$id)
                            ->where('w_id',$request->w_id)
                            ->where('type',$type)
                            ->update([
                                'quantity' => $quan
                            ]);
                        }


                        $fp = FinishProducts::with('rawproducts.unit','rawproducts2.product.unit')
                        ->where('id',$id)
                        ->get();
                        // dd($fp);
                        for ($i=0; $i <count($fp) ; $i++) {
                            $cs = CurrentStock::with('products','products.unit','variant.product')
                            ->where('p_id',$fp[$i]->p_id)
                            ->where('type',$fp[$i]->type)
                            ->where('w_id',$request->w_id)
                            ->first();
                            if($fp[$i]->type == 1)
                            {
                                $account_i = AccountDetails::where('name_of_account',$fp[$i]->rawproducts2->name)
                                ->first();
                            }
                            else
                            {
                                $account_i = AccountDetails::where('name_of_account',$fp[$i]->rawproducts->pro_code.' - '.$fp[$i]->rawproducts->pro_name)
                                ->first();
                            }
                            $balance = 0;
                            $balance_total = 0;
                            $credit_p = GeneralLedger::where('account_code',$account_i->Code)
                            ->where('w_id',$request->w_id)
                            ->get();
                            // dd($account_i);
                            foreach ($credit_p as $key => $value) {
                                $balance += $value->net_value;
                            }

                            if($fp[$i]->type == 1 ? $fp[$i]->rawproducts2->product->unit->u_name == 'Mililiter' : $fp[$i]->rawproducts->unit->u_name == 'Mililiter')
                            {
                                $final = $request->quantity * $fp[$i]->quantity;
                                $unit_quantity = $cs->unit_quantity - $final;
                                // $quan = $cs->products->weight * 1000;
                                $quantity  = $unit_quantity / 1000;
                                $balance_total = $balance + (0 - $final);
                                $rcv = StockReceiving::where('p_id',$fp[$i]->p_id)
                                ->where('u_id',$fp[$i]->type == 1 ? $fp[$i]->rawproducts2->product->unit_id : $fp[$i]->rawproducts->unit_id )
                                ->where('type',$fp[$i]->type)
                                ->where('w_id',$request->w_id)
                                ->first();

                                StockReceiving::where('p_id',$fp[$i]->p_id)
                                ->where('u_id',$fp[$i]->type == 1 ? $fp[$i]->rawproducts2->product->unit_id : $fp[$i]->rawproducts->unit_id )
                                ->where('type',$fp[$i]->type)
                                ->where('w_id',$request->w_id)
                                ->update([
                                    'total' =>  $unit_quantity
                                ]);
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Subtracted product for finish product manually',
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $final,
                                    'stock_in' => '0',
                                    'net_value' => 0 - $final,
                                    'balance' => $balance_total,
                                    'credit' => ( $fp[$i]->type == 1 ? $fp[$i]->rawproducts2->cost : $fp[$i]->rawproducts->cost * $quantity),
                                    'debit' => 0,
                                    'type' => $fp[$i]->type,
                                    'w_id' => $request->w_id,
                                    'amount' => ($fp[$i]->type == 1 ? $fp[$i]->rawproducts2->cost : $fp[$i]->rawproducts->cost)
                                ]);

                                StockOut::create([
                                    'p_id' => $fp[$i]->p_id,
                                    'quantity' => $final,
                                    'stock_date' => $request->stock_date,
                                    's_type' => 'Manual',
                                    'price' =>  $fp[$i]->type == 1 ? $fp[$i]->rawproducts2->price : $fp[$i]->rawproducts->price,
                                    'w_id' => $request->w_id,
                                    'created_by' => $u_id,
                                    'type' => $fp[$i]->type
                                ]);
                            }

                            else if($fp[$i]->type == 1 ? $fp[$i]->rawproducts2->product->unit->u_name == 'Liter' : $fp[$i]->rawproducts->unit->u_name == 'Liter' )
                            {
                                $final = $request->quantity *  $fp[$i]->quantity;
                                $unit_quantity = $cs->unit_quantity - $final;
                                // $quan = $cs->products->weight * 1000;
                                $quantity  = $unit_quantity / 1000;
                                $balance_total = $balance + (0 - $final);

                                $rcv = StockReceiving::where('p_id',$fp[$i]->p_id)
                                ->where('u_id',$fp[$i]->type == 1 ? $fp[$i]->rawproducts2->product->unit_id : $fp[$i]->rawproducts->unit_id )
                                ->where('type',$fp[$i]->type)
                                ->where('w_id',$request->w_id)
                                ->first();

                                StockReceiving::where('p_id',$fp[$i]->p_id)
                                ->where('u_id',$fp[$i]->type == 1 ? $fp[$i]->rawproducts2->product->unit_id : $fp[$i]->rawproducts->unit_id )
                                ->where('type',$fp[$i]->type)
                                ->where('w_id',$request->w_id)
                                ->update([
                                    'total' =>  $unit_quantity
                                ]);

                                StockOut::create([
                                    'p_id' => $fp[$i]->p_id,
                                    'quantity' => $final,
                                    'stock_date' => $request->stock_date,
                                    's_type' => 'Manual',
                                    'price' =>  $fp[$i]->type == 1 ? $fp[$i]->rawproducts2->price : $fp[$i]->rawproducts->price,
                                    'w_id' => $request->w_id,
                                    'created_by' => $u_id,
                                    'type' => $fp[$i]->type
                                ]);
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Subtracted product for finish product manually',
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $final,
                                    'stock_in' => '0',
                                    'net_value' => 0 - $final,
                                    'balance' => $balance_total,
                                    'credit' =>  ($fp[$i]->type == 1 ? $fp[$i]->rawproducts2->cost : $fp[$i]->rawproducts->cost * $quantity),
                                    'debit' => 0,
                                    'type' => $fp[$i]->type,
                                    'w_id' => $request->w_id,
                                    'amount' => ($fp[$i]->type == 1 ? $fp[$i]->rawproducts2->cost : $fp[$i]->rawproducts->cost)
                                ]);
                            }
                            else
                            {
                                $final = $fp[$i]->quantity * $request->quantity;
                                $quantity = $cs->quantity -  $final;
                                $unit_quantity = null;

                                $balance_total = $balance + (0 - $final);
                                StockOut::create([
                                    'p_id' => $fp[$i]->p_id,
                                    'quantity' => $final,
                                    'stock_date' => $request->stock_date,
                                    's_type' => 'Manual',
                                    'price' =>  $fp[$i]->type == 1 ? $fp[$i]->rawproducts2->price : $fp[$i]->rawproducts->price,
                                    'w_id' => $request->w_id,
                                    'created_by' => $u_id,
                                    'type' => $fp[$i]->type
                                ]);
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Subtracted product for finish product manually',
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $final,
                                    'stock_in' => '0',
                                    'net_value' => 0 - $final,
                                    'balance' => $balance_total,
                                    'credit' => ($fp[$i]->type == 1 ? $fp[$i]->rawproducts2->cost : $fp[$i]->rawproducts->cost * $quantity),
                                    'debit' => 0,
                                    'type' => $fp[$i]->type,
                                    'w_id' => $request->w_id,
                                    'amount' => ($fp[$i]->type == 1 ? $fp[$i]->rawproducts2->cost : $fp[$i]->rawproducts->cost)
                                ]);
                            }

                            CurrentStock::where('p_id',$fp[$i]->p_id)
                            ->where('w_id',$request->w_id)
                            ->where('type',$fp[$i]->type)
                            ->update([
                                'unit_quantity' => $unit_quantity,
                                'quantity' => $quantity,
                            ]);
                        }
                        toastr()->success('Stock added successfully!');
                        return redirect()->back();
                    }
                }
            }
            else
            {
                Stocks::create([
                    'p_id' => $id,
                    'w_id' => $request->w_id,
                    'quantity' => $request->quantity,
                    'stock_date' => $request->stock_date,
                    's_type' => 'Manual',
                    'cost' => $request->cost,
                    'created_by' => $u_id,
                    'type' => $type
                ]);
                // addStock($request->all(),$id);

                Products::where('id',$id)
                ->update([
                    'cost' => $request->cost,
                    'updated_by' => $u_id
                ]);

                $cs=CurrentStock::where('p_id',$id)
                ->where('w_id',$request->w_id)
                ->where('type',$type)
                ->first();


                if($cs == null)
                    {
                        CurrentStock::create([
                            'p_id' => $id,
                            'w_id' => $request->w_id,
                            'quantity' => $request->quantity,
                            'unit_quantity' =>null,
                            'type' => $type
                        ]);
                    }

                    else
                    {
                        $unit_quantity = null;
                        $quan = $cs->quantity + $request->quantity;
                        if($unit_quantity == null)
                        {
                            $u_quan=null;
                        }
                        else
                        {
                            $u_quan = $cs->unit_quantity + $unit_quantity;
                        }
                        CurrentStock::where('p_id',$id)
                        ->where('w_id',$request->w_id)
                        ->where('type',$type)
                        ->update([
                            'quantity' => $quan,
                            'unit_quantity' => $u_quan
                        ]);
                    }

                    $gl = GeneralLedger::max('id');
                    if($gl == null)
                    {
                        $link_id = 1;
                    }
                    else
                    {
                        $ledger1 = GeneralLedger::where('id',$gl)->first();
                        $link_id = $ledger1->link_id + 1;
                    }
                    $balance = 0;
                    $net_d_p = 0;
                    $debit_p = GeneralLedger::where('account_code',$account->Code)
                    ->where('w_id',$request->w_id)
                    ->get();
                    if($debit_p->isEmpty())
                    {
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Added product manually',
                            'account_name' => $account->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account->Code,
                            'currency_code' => 'PKR',
                            'stock_in' => $request->quantity,
                            'stock_out' => '0',
                            'net_value' => $request->quantity,
                            'balance' => $request->quantity,
                            'debit' => ($request->cost * $request->quantity),
                            'credit' => 0,
                            'amount' => ($request->cost),
                            'type' => $type,
                            'w_id' => $request->w_id
                        ]);
                    }
                    else
                    {
                        foreach ($debit_p as $key => $c) {
                            $balance+=$c->net_value;
                        }
                        $net_d_p = $balance + ( $request->quantity - 0);
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Added product manually',
                            'account_name' => $account->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account->Code,
                            'currency_code' => 'PKR',
                            'stock_in' => $request->quantity,
                            'stock_out' => '0',
                            'net_value' => $request->quantity,
                            'amount' => ($request->cost),
                            'balance' => $net_d_p,
                            'debit' => ($request->cost * $request->quantity),
                            'credit' => 0,
                            'type' => $type,
                            'w_id' => $request->w_id
                        ]);
                    }

                    toastr()->success('Stock added successfully!');
                    return redirect()->back();
            }


        }
    }

    public function search($id)
    {
        $cs=CurrentStock::with(['products','products.brands','products.unit','products.category','products.subcategory','variant.product.brands'])
        ->where('w_id',$id)
        ->get();
        return $cs;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
