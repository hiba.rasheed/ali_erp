<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Warehouse;
use App\Roles;
use App\Notifications;
use DataTables;
use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return  view('user.index',compact('permissions'));
    }

    public function datatable()
    {
        $user=User::with('role')->get();
        return DataTables::of($user)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' => false,
            'roles' =>  Roles::where('status',1)->get(),
            'ware' =>  Warehouse::all(),
        ];
        return view('user.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $password = Hash::make('12345678');
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $password,
            'r_id' => $request->r_id,
            'w_id' => $request->w_id,
        ]);

        toastr()->success('User added successfully!');
        return redirect(url('').'/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles=Roles::where('status',1)->get();
        $user=User::find($id);
        $data=[
            'isEdit' => true,
            'roles' =>$roles,
            'user' =>$user,
            'ware' =>  Warehouse::all(),
        ];
        return view('user.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->password == null)
        {
            User::where('id',$id)
            ->update([
                'name' => $request->name,
                'email' => $request->email,
                'r_id' => $request->r_id,
                'w_id' => $request->w_id,
            ]);
        }
        else
        {
            $password = Hash::make($request->password);
            User::where('id',$id)
            ->update([
                'name' => $request->name,
                'email' => $request->email,
                'password' => $password,
                'r_id' => $request->r_id,
                'w_id' => $request->w_id,
            ]);
        }

        toastr()->success('User updated successfully!');
        return redirect(url('').'/users');
    }

    public function editProfile($id)
    {
        $user = User::find($id);
        return view('user.profile',compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
