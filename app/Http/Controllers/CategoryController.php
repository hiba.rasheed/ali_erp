<?php

namespace App\Http\Controllers;
use App\Category;
use App\User;
use Illuminate\Http\Request;
use DataTables;
use App\Exports\CategoryExport;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('category.index',compact('permissions'));
    }

    public function datatable()
    {
        $cat=Category::all();
        return DataTables::of($cat)->make();
    }

    public function status(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';
        $u_id = Auth::user()->id;
        $id     = $request->input('id');
        $status = $request->input('status');

        $item = Category::find($id);
        if ($item->update(['status' => $status])) {
            Category::where('id',$id)
            ->update([
                'status' => $status,
                'updated_by' => $u_id
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' => false
        ];
        return view('category.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cat_name'      =>  'required|string|max:255|unique:category'
        ]);
        $u_id = Auth::user()->id;
        $role_id = Auth::user()->r_id;
        $env_a_id = env('ADMIN_ID');
        $env_m_id = env('MANAGER_ID');

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $status = 1;
        }
        else
        {
            $status = 0;
        }
        $data = [
            'cat_name' => $request->cat_name,
            'created_by' => $u_id,
            'status' => $status
        ];
        $u_name = Auth::user()->name;
        $user = User::where('r_id',env('ADMIN_ID'))->get();
        $data1 = [
            'notification' => 'New category has been added by '.$u_name,
            'link' => url('').'/category',
            'name' => 'View Categories',
        ];
        Notification::send($user, new AddNotification($data1));
        Category::create($data);
        toastr()->success('Category added successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(request()->ajax())
        {
            $cat=Category::find($id);
            return $cat;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat=Category::find($id);
        $data=[
            'isEdit' => true,
            'cat' =>$cat
        ];
        return view('category.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'cat_name'      =>  'required|string|max:255|unique:category,cat_name,'.$id
        ]);
        $u_id = Auth::user()->id;
        $cat=Category::where('id',$id)
        ->update([
            'cat_name' => $request->cat_name,
            'updated_by' => $u_id
        ]);
        toastr()->success('Category Updated successfully!');
        return redirect(url('').'/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function excel()
    {
        return Excel::download(new CategoryExport, 'Category.xlsx');
    }
}
