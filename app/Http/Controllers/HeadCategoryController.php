<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HeadCategory;
use App\HeadofAccounts;
use App\GeneralLedger;
use App\User;
use App\AccountDetails;
use Auth;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class HeadCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $hoa=HeadofAccounts::all();
        $hcat=HeadCategory::with(['hoa'])
        ->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('headCategory.index',compact('hoa','hcat','permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $hoa=HeadofAccounts::all();
        $data=[
            'hoa' => $hoa,
            'isEdit' => false
        ];
        return view('headCategory.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hcat=HeadCategory::where('name',$request->name)
        ->where('a_id',$request->a_id)
        ->first();
        $u_id = Auth::user()->id;
        $data = [
            'name' => $request->name,
            'a_id' => $request->a_id,
            'code' => $request->code,
            'created_by' => $u_id
        ];
        if($hcat==null)
        {
            HeadCategory::create($data);
            $u_name = Auth::user()->name;
            $user = User::where('r_id',env('ADMIN_ID'))->get();
            $data1 = [
                'notification' => 'New head category has been added by '.$u_name,
                'link' => url('').'/headcategory',
                'name' => 'View Head Categories',
            ];
            Notification::send($user, new AddNotification($data1));
            toastr()->success('Head Category added successfully!');
            return redirect()->back();
        }
        else
        {
            toastr()->error('Head Category already exist of this HOA!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ledger = GeneralLedger::where('account_code','like',$id.'%')
        ->get();
        if($ledger->isEmpty())
        {
            return $ledger=0;
        }
        else
        {
            return $ledger;
        }
    }
    public function history($id)
    {
        $ledger = GeneralLedger::with('createUser')
        ->where('account_code','like',$id.'%')
        ->get();
        $hcat=AccountDetails::all();
        return view('headCategory.history',compact('ledger','hcat','id'));
    }

    public function accountsName($id)
    {
        $account = AccountDetails::where('Code','like',$id.'%')
        ->get();
        return $account;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hcat = HeadCategory::find($id);
        $hoa=HeadofAccounts::all();
        $data=[
            'isEdit' => true,
            'hcat' => $hcat,
            'hoa' => $hoa
        ];
        return view('headCategory.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'      =>  'required|string|max:50'
        ]);
        $u_id = Auth::user()->id;
        HeadCategory::where('id',$id)
        ->update([
            'name' => $request->name,
            'updated_by' => $u_id
        ]);
        toastr()->success('Head Category updated successfully!');
        return redirect(url('').'/headcategory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
