<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sales;
use App\SaleCounter;
use App\SaleReturn;
use App\TransactionHistory;
use App\SaleDetails;
use App\SaleReturnDetails;
use App\Warehouse;
use App\Vendors;
use App\Products;
use App\ProductVariants;
use App\Unit;
use App\StockOut;
use App\Stocks;
use App\City;
use App\Bank;
use App\Brands;
use App\Category;
use App\Subcategory;
use App\Groups;
use App\HeadCategory;
use App\CurrentStock;
use App\AccountDetails;
use App\GeneralLedger;
use App\UserMenu;
use DataTables;
use Storage;
use DB;
use PDF;
use Auth;
use Response;
use Session;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\SalesExport;
use App\Exports\SalesYearlyExport;
use App\Exports\SalesMonthlyExport;
use App\Exports\SalesDateExport;
use App\Exports\SalesDateDifferenceExport;
use App\Exports\SalesStatusExport;
use App\Exports\SalesPaymentStatusExport;
use App\Exports\SalesCustomerExport;
use App\Exports\SalesSalePersonExport;
use App\Exports\SalesReportExport;
use App\Exports\SalesReportYearlyExport;
use App\Exports\SalesReportMonthlyExport;
use App\Exports\SalesReportDateExport;
use App\Exports\SalesReportDateDifferenceExport;
use App\Exports\SalesReportCustomerExport;
use App\Exports\SalesReportProductExport;
use App\Exports\SalesReportSalepersonExport;
use App\Exports\SalesReportWarehouseExport;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function sync()
    {
        $woocommerce        =   wooCommerce();
        $biller = Vendors::where('v_type','Biller')->where('name','Posch Care')->first();
        $customer = Vendors::where('v_type','Customer')->where('name','Customer')->first();
        $w_id = 1;
        $orders = listOrders();
        // return $orders;
        for ($i=0; $i < count($orders) ; $i++) {
            $sales = Sales::where('order_no_w',$orders[$i]['id'])->first();
            if($sales == null)
            {
                if(count($orders[$i]['line_items']) > 0)
                {
                    if($orders[$i]['prices_include_tax'] == false)
                    {
                        $tax_status = 'No';
                        $tax = null;
                    }
                    else {
                        $tax_status = 'Yes';
                        $tax = $orders[$i]['total_tax'];
                    }
                    if($orders[$i]['status'] == 'processing')
                    {
                        $s_status = 'Approved';
                        $p_status = 'Pending';
                    }
                    if($orders[$i]['status'] == 'cancelled')
                    {
                        $s_status = 'Pending';
                        $p_status = 'Pending';
                    }
                    if($orders[$i]['status'] == 'complete')
                    {
                        $s_status = 'Delivered';
                        $p_status = 'Paid';
                    }


                    $data=([
                        'sale_date' => Carbon::parse($orders[$i]['date_created'])->format('Y-m-d'),
                        'b_id' => $biller->id,
                        'c_id' => $customer->id,
                        'w_id' => $w_id,
                        's_address' => $orders[$i]['billing']['address_1'],
                        's_status' => $s_status,
                        'p_status' => $p_status,
                        'tax' => $tax,
                        'total' => $orders[$i]['total'],
                        'advance' => 'No',
                        'tax_status' => $tax_status,
                        'pay_type' => 'Cash on Delivery',
                        'return_status' => 'No Return',
                        's_type' => 'Woo Commerce'
                    ]);
                    $p=Sales::create($data);
                    if($s_status == 'Delivered')
                    {
                        $posted_date = Carbon::now()->format('Y-m-d');
                        $period = Carbon::now()->format('M-y');


                        $gl = GeneralLedger::max('id');
                        if($gl == null)
                        {
                            $link_id = 1;
                        }
                        else
                        {
                            $ledger = GeneralLedger::where('id',$gl)->first();
                            $link_id = $ledger->link_id + 1;
                        }

                        $customer = Vendors::find($sales->c_id);
                        $u_id = Auth::user()->id;
                        $account_s = AccountDetails::where('name_of_account','Sales')->first();
                        $sales = GeneralLedger::where('account_code',$account_s->Code)
                        ->get();
                        $account_c = AccountDetails::where('name_of_account',$customer->company.' - '.$customer->name)
                        ->first();
                        // dd($account_c);
                        $customer_l = GeneralLedger::where('account_code',$account_c->Code)
                        ->get();
                        $sales_n = 0;
                        $sales_d = 0;
                        $sales_c = 0;
                        if($sales->isEmpty())
                        {
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to '.$customer->name,
                                'account_name' => $account_s->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_s->Code,
                                // 'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => '0',
                                'credit' => $sales->total,
                                'net_value' =>  0 - $sales->total,
                                'balance' =>  0 - $sales->total
                            ]);
                        }
                        else
                        {
                            $balance = 0;
                            foreach ($sales as $key => $d) {
                                $sales_d+=$d->debit;
                                $sales_c+=$d->credit;
                                $balance+=$d->net_value;
                            }
                            $sales_c +=$sales->total;
                            // $sales_n = $sales_d - $sales_c;
                            $sales_n = $balance + (0 -$sales->total);

                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to '.$customer->name,
                                'account_name' => $account_s->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_s->Code,
                                // 'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => '0',
                                'credit' =>$sales->total,
                                'net_value' => 0 -$sales->total,
                                'balance' => $sales_n
                            ]);
                        }
                        $customer_d = 0;
                        $customer_c = 0;
                        $customer_n = 0;
                        if($customer_l->isEmpty())
                        {
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to'.$customer->name,
                                'account_name' => $account_c->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_c->Code,
                                // 'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => $sales->total,
                                'credit' => '0',
                                'net_value' => $sales->total,
                                'balance' => $sales->total
                            ]);
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to'.$customer->name,
                                'account_name' => $account_c->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_c->Code,
                                // 'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'credit' => $sales->total,
                                'debit' => '0',
                                'net_value' => 0 - $sales->total,
                                'balance' => 0 - $sales->total
                            ]);
                        }
                        else
                        {
                            $balance = 0;
                            foreach ($customer_l as $key => $c) {
                                $customer_c+=$c->credit;
                                $customer_d+=$c->debit;
                                $balance+=$c->net_value;
                            }
                            $customer_d += $sales->total;
                            // $customer_n = $customer_d - $customer_c;
                            $customer_n = $balance + ($sales->total - 0);

                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to '.$customer->name,
                                'account_name' => $account_c->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_c->Code,
                                // 'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => $sales->total,
                                'credit' => '0',
                                'net_value' => $sales->total,
                                'balance' => $customer_n
                            ]);

                            $balance = 0;
                            $customer_d = 0;
                            $customer_c = 0;
                            $customer_n = 0;
                            foreach ($customer_l as $key => $c) {
                                $customer_c+=$c->credit;
                                $customer_d+=$c->debit;
                                $balance+=$c->net_value;
                            }
                            $customer_d += $sales->total;
                            // $customer_n = $customer_d - $customer_c;
                            $customer_n = $balance + ( 0 - $sales->total);

                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to '.$customer->name,
                                'account_name' => $account_c->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_c->Code,
                                // 'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'credit' => 0 -$sales->total,
                                'debit' => '0',
                                'net_value' => 0 - $sales->total,
                                'balance' => $customer_n
                            ]);
                        }

                        $account_cash = AccountDetails::where('name_of_account','Cash In Hand')->first();
                        $cash = GeneralLedger::where('account_code',$account_cash->Code)
                        ->get();
                        if($cash->isEmpty())
                        {
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Amount Paid by: '.$customer->name,
                                'account_name' => $account_cash->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_cash->Code,
                                // 'transaction_no' => $ref_no,
                                'currency_code' =>'PKR',
                                'debit' => $sales->total,
                                'credit' => '0',
                                'net_value' => $sales->total - 0,
                                'balance' => $sales->total - 0,
                            ]);
                        }
                        else
                        {
                            $balance = 0;
                            foreach ($cash as $key => $c) {
                                $cash_c+=$c->credit;
                                $cash_d+=$c->debit;
                                $balance+=$c->net_value;
                            }
                            $cash_d += $sales->total;
                            $cash_n = $balance + ($sales->total - 0);

                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Amount Paid by: '.$customer->name,
                                'account_name' => $account_cash->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_cash->Code,
                                // 'transaction_no' => $ref_no,s
                                'currency_code' =>'PKR',
                                'debit' => $sales->total,
                                'credit' => '0',
                                'net_value' => $sales->total - 0,
                                'balance' => $cash_n
                            ]);
                        }


                        TransactionHistory::create([
                            'p_s_id' => $sales->id,
                            'p_type' => 'Sales',
                            't_type' => 'Received',
                            'paid_by' => 'Cash',
                            'total' => $sales->total
                        ]);
                    }

                    for ($j=0; $j < count($orders[$i]['line_items']) ; $j++) {
                        if ($s_status == 'Delivered') {
                            $deliverQty = $orders[$i]['line_items'][$j]['quantity'];
                        } else {
                            $deliverQty = null;
                        }
                        $pid = Products::where('wooId', $orders[$i]['line_items'][$j]['product_id'])->pluck('id');
                        SaleDetails::create([
                            's_id' => $p->id,
                            'p_id' => $pid[0],
                            'wooId' => $orders[$i]['line_items'][$j]['product_id'],
                            'quantity' => $orders[$i]['line_items'][$j]['quantity'],
                            'sub_total' => $orders[$i]['line_items'][$j]['subtotal'],
                            'price' => $orders[$i]['line_items'][$j]['subtotal'] /  $orders[$i]['line_items'][$j]['quantity'] ,
                            'type' => 0,
                            'delivered_quantity' => $deliverQty
                        ]);


                        if($s_status == 'Delivered')
                        {
                            $details=SaleDetails::where('s_id',$sales->id)
                            ->get();
                            $posted_date = Carbon::now()->format('Y-m-d');
                            $period = Carbon::now()->format('M-y');


                            $gl = GeneralLedger::max('id');
                            if($gl == null)
                            {
                                $link_id = 1;
                            }
                            else
                            {
                                $ledger = GeneralLedger::where('id',$gl)->first();
                                $link_id = $ledger->link_id ;
                            }


                            $product = Products::where('wooId',$orders[$i]['line_items'][$j]['product_id'])->first();


                            $prod=CurrentStock::where('p_id',$product->id)
                            ->where('w_id',$sales->w_id)
                            ->where('type',0)
                            ->first();

                            $quan=$prod->quantity -  $orders[$i]['line_items'][$j]['quantity'];
                            // $amount += $details[$j]->sub_total;
                            CurrentStock::where('p_id',$product->id)
                            ->where('w_id',$sales->w_id)
                            ->where('type',0)
                            ->update([
                                'quantity' => $quan
                            ]);
                            $date=Carbon::now()->format('Y-m-d');
                            StockOut::create([
                                'p_id' => $product->id,
                                'quantity' =>  $orders[$i]['line_items'][$j]['quantity'],
                                'stock_date' => $date,
                                'sale_d_id' => $details[$j]->id,
                                'price' => $request->price[$j],
                                'w_id' => $sales->w_id,
                                's_type' => 'Sales',
                                'type' => 0
                            ]);
                            $account_i = AccountDetails::where('name_of_account',$product->pro_code.' - '.$product->pro_name)
                            ->first();
                            $balance = 0;
                            $net_d_p = 0;
                            $debit_p = GeneralLedger::where('account_code',$account_i->Code)
                            ->where('w_id',$sales->w_id)
                            ->get();
                            if($debit_p->isEmpty())
                            {
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$sales->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    // 'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' =>  $orders[$i]['line_items'][$j]['quantity'],
                                    'stock_in' => '0',
                                    'net_value' => 0 -  $orders[$i]['line_items'][$j]['quantity'],
                                    'balance' => 0 -  $orders[$i]['line_items'][$j]['quantity'],
                                    'credit' =>  $product->cost *  $orders[$i]['line_items'][$j]['quantity'],
                                    'debit' => 0,
                                    'type' => 0,
                                    'w_id' => $sales->w_id,
                                    'amount' => 0 - $product->cost
                                ]);
                            }
                            else
                            {
                                foreach ($debit_p as $key => $c) {
                                    $balance+=$c->net_value;
                                }
                                $net_d_p = $balance + ( 0 - $orders[$i]['line_items'][$j]['quantity'] );
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$sales->id,
                                    'account_name' => $account_i->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i->Code,
                                    // 'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $orders[$i]['line_items'][$j]['quantity'],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $orders[$i]['line_items'][$j]['quantity'],
                                    'balance' => $net_d_p,
                                    'credit' =>  $product->cost * $orders[$i]['line_items'][$j]['quantity'],
                                    'debit' => 0,
                                    'type' => 0,
                                    'w_id' => $sales->w_id,
                                    'amount' => 0 - $product->cost
                                ]);
                            }
                        }
                    }
                }

            }
            else {
                if($sales->s_status == 'Approved' && $orders[$i]['status'] == 'complete')
                {
                    $details=SaleDetails::where('s_id',$sales->id)
                    ->get();
                    $s_status = 'Delivered';
                    $p_status = 'Paid';
                    Sales::where('id',$sales->id)->update([
                        'p_status' => $p_status,
                        's_status' => $s_status
                    ]);
                    $posted_date = Carbon::now()->format('Y-m-d');
                    $period = Carbon::now()->format('M-y');


                    $gl = GeneralLedger::max('id');
                    if($gl == null)
                    {
                        $link_id = 1;
                    }
                    else
                    {
                        $ledger = GeneralLedger::where('id',$gl)->first();
                        $link_id = $ledger->link_id + 1;
                    }


                    for ($j=0; $j < count($orders[$i]['line_items']) ; $j++) {
                        $product = Products::where('wooId',$orders[$i]['line_items'][$j]['product_id'])->first();

                        SaleDetails::where('s_id',$sales->id)
                        ->update([
                            'delivered_quantity' => $orders[$i]['line_items'][$j]['quantity']
                        ]);
                        $prod=CurrentStock::where('p_id',$product->id)
                        ->where('w_id',$sales->w_id)
                        ->where('type',0)
                        ->first();

                        $quan=$prod->quantity -  $orders[$i]['line_items'][$j]['quantity'];
                        // $amount += $details[$j]->sub_total;
                        CurrentStock::where('p_id',$product->id)
                        ->where('w_id',$sales->w_id)
                        ->where('type',0)
                        ->update([
                            'quantity' => $quan
                        ]);
                        $date=Carbon::now()->format('Y-m-d');
                        StockOut::create([
                            'p_id' => $product->id,
                            'quantity' =>  $orders[$i]['line_items'][$j]['quantity'],
                            'stock_date' => $date,
                            'sale_d_id' => $details[$j]->id,
                            'price' => $request->price[$j],
                            'w_id' => $sales->w_id,
                            's_type' => 'Sales',
                            'type' => 0
                        ]);
                        $account_i = AccountDetails::where('name_of_account',$product->pro_code.' - '.$product->pro_name)
                        ->first();
                        $balance = 0;
                        $net_d_p = 0;
                        $debit_p = GeneralLedger::where('account_code',$account_i->Code)
                        ->where('w_id',$sales->w_id)
                        ->get();
                        if($debit_p->isEmpty())
                        {
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sale product for sale number: '.$sales->id,
                                'account_name' => $account_i->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_i->Code,
                                // 'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'stock_out' =>  $orders[$i]['line_items'][$j]['quantity'],
                                'stock_in' => '0',
                                'net_value' => 0 -  $orders[$i]['line_items'][$j]['quantity'],
                                'balance' => 0 -  $orders[$i]['line_items'][$j]['quantity'],
                                'credit' =>  $product->cost *  $orders[$i]['line_items'][$j]['quantity'],
                                'debit' => 0,
                                'type' => 0,
                                'w_id' => $sales->w_id,
                                'amount' => 0 - $product->cost
                            ]);
                        }
                        else
                        {
                            foreach ($debit_p as $key => $c) {
                                $balance+=$c->net_value;
                            }
                            $net_d_p = $balance + ( 0 - $orders[$i]['line_items'][$j]['quantity'] );
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sale product for sale number: '.$sales->id,
                                'account_name' => $account_i->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_i->Code,
                                // 'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'stock_out' => $orders[$i]['line_items'][$j]['quantity'],
                                'stock_in' => '0',
                                'net_value' => 0 - $orders[$i]['line_items'][$j]['quantity'],
                                'balance' => $net_d_p,
                                'credit' =>  $product->cost * $orders[$i]['line_items'][$j]['quantity'],
                                'debit' => 0,
                                'type' => 0,
                                'w_id' => $sales->w_id,
                                'amount' => 0 - $product->cost
                            ]);
                        }



                    }
                    $customer = Vendors::find($sales->c_id);
                    $u_id = Auth::user()->id;
                    $account_s = AccountDetails::where('name_of_account','Sales')->first();
                    $sales = GeneralLedger::where('account_code',$account_s->Code)
                    ->get();
                    $account_c = AccountDetails::where('name_of_account',$customer->company.' - '.$customer->name)
                    ->first();
                    // dd($account_c);
                    $customer_l = GeneralLedger::where('account_code',$account_c->Code)
                    ->get();
                    $sales_n = 0;
                    $sales_d = 0;
                    $sales_c = 0;
                    if($sales->isEmpty())
                    {
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to '.$customer->name,
                            'account_name' => $account_s->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_s->Code,
                            // 'transaction_no' => $gdn_no,
                            'currency_code' => 'PKR',
                            'debit' => '0',
                            'credit' => $sales->total,
                            'net_value' =>  0 - $sales->total,
                            'balance' =>  0 - $sales->total
                        ]);
                    }
                    else
                    {
                        $balance = 0;
                        foreach ($sales as $key => $d) {
                            $sales_d+=$d->debit;
                            $sales_c+=$d->credit;
                            $balance+=$d->net_value;
                        }
                        $sales_c +=$sales->total;
                        // $sales_n = $sales_d - $sales_c;
                        $sales_n = $balance + (0 -$sales->total);

                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to '.$customer->name,
                            'account_name' => $account_s->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_s->Code,
                            // 'transaction_no' => $gdn_no,
                            'currency_code' => 'PKR',
                            'debit' => '0',
                            'credit' =>$sales->total,
                            'net_value' => 0 -$sales->total,
                            'balance' => $sales_n
                        ]);
                    }
                    $customer_d = 0;
                    $customer_c = 0;
                    $customer_n = 0;
                    if($customer_l->isEmpty())
                    {
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to'.$customer->name,
                            'account_name' => $account_c->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_c->Code,
                            // 'transaction_no' => $gdn_no,
                            'currency_code' => 'PKR',
                            'debit' => $sales->total,
                            'credit' => '0',
                            'net_value' => $sales->total,
                            'balance' => $sales->total
                        ]);
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to'.$customer->name,
                            'account_name' => $account_c->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_c->Code,
                            // 'transaction_no' => $gdn_no,
                            'currency_code' => 'PKR',
                            'credit' => $sales->total,
                            'debit' => '0',
                            'net_value' => 0 - $sales->total,
                            'balance' => 0 - $sales->total
                        ]);
                    }
                    else
                    {
                        $balance = 0;
                        foreach ($customer_l as $key => $c) {
                            $customer_c+=$c->credit;
                            $customer_d+=$c->debit;
                            $balance+=$c->net_value;
                        }
                        $customer_d += $sales->total;
                        // $customer_n = $customer_d - $customer_c;
                        $customer_n = $balance + ($sales->total - 0);

                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to '.$customer->name,
                            'account_name' => $account_c->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_c->Code,
                            // 'transaction_no' => $gdn_no,
                            'currency_code' => 'PKR',
                            'debit' => $sales->total,
                            'credit' => '0',
                            'net_value' => $sales->total,
                            'balance' => $customer_n
                        ]);

                        $balance = 0;
                        $customer_d = 0;
                        $customer_c = 0;
                        $customer_n = 0;
                        foreach ($customer_l as $key => $c) {
                            $customer_c+=$c->credit;
                            $customer_d+=$c->debit;
                            $balance+=$c->net_value;
                        }
                        $customer_d += $sales->total;
                        // $customer_n = $customer_d - $customer_c;
                        $customer_n = $balance + ( 0 - $sales->total);

                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to '.$customer->name,
                            'account_name' => $account_c->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_c->Code,
                            // 'transaction_no' => $gdn_no,
                            'currency_code' => 'PKR',
                            'credit' => 0 -$sales->total,
                            'debit' => '0',
                            'net_value' => 0 - $sales->total,
                            'balance' => $customer_n
                        ]);
                    }

                    $account_cash = AccountDetails::where('name_of_account','Cash In Hand')->first();
                    $cash = GeneralLedger::where('account_code',$account_cash->Code)
                    ->get();
                    if($cash->isEmpty())
                    {
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Amount Paid by: '.$customer->name,
                            'account_name' => $account_cash->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_cash->Code,
                            // 'transaction_no' => $ref_no,
                            'currency_code' =>'PKR',
                            'debit' => $sales->total,
                            'credit' => '0',
                            'net_value' => $sales->total - 0,
                            'balance' => $sales->total - 0,
                        ]);
                    }
                    else
                    {
                        $balance = 0;
                        foreach ($cash as $key => $c) {
                            $cash_c+=$c->credit;
                            $cash_d+=$c->debit;
                            $balance+=$c->net_value;
                        }
                        $cash_d += $sales->total;
                        $cash_n = $balance + ($sales->total - 0);

                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Amount Paid by: '.$customer->name,
                            'account_name' => $account_cash->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_cash->Code,
                            // 'transaction_no' => $ref_no,s
                            'currency_code' =>'PKR',
                            'debit' => $sales->total,
                            'credit' => '0',
                            'net_value' => $sales->total - 0,
                            'balance' => $cash_n
                        ]);
                    }


                    TransactionHistory::create([
                        'p_s_id' => $sales->id,
                        'p_type' => 'Sales',
                        't_type' => 'Received',
                        'paid_by' => 'Cash',
                        'total' => $sales->total
                    ]);

                }
            }
        }
        return redirect()->back();
    }

    public function index(Request $request)
    {
        $role_id = Auth::user()->r_id;
        $env_a_id = env('ADMIN_ID');
        $env_m_id = env('MANAGER_ID');
        $counter = null;
        // dd($env_a_id);
        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $counter = null;
        }
        else
        {
            $counter = Session::get('assign');
        }

        $sales=Sales::with(['warehouse','customer','biller','saleperson'])
        ->withCount(['transaction as total_amount' => function($query) {
            $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
        }])
        ->get();
        $customer=Vendors::where('v_type','Customer')->where('status',1)->get();
        $saleperson=Vendors::where('v_type','Saleperson')->where('status',1)->get();
        $bank=Bank::where('status',1)->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);

        // dd($sales);

        $index = 0;
        return view('sales.index',compact('customer','bank','permissions','menu_id','sales','saleperson','index','counter'));
    }

    public function product($id)
    {
        $sale = SaleDetails::with('products')
        ->with(['sale' => function($query) {
            $avg = $query->select(DB::raw('*'))->get();
            // ->join('warehouse','warehouse.id','sales.w_id');
        },'sale.warehouse' => function($query) {
            $avg = $query->select(DB::raw('*'));
        }
        ])
        ->where('p_id',$id)
        ->get();
        return $sale;
    }

    public function report()
    {
        $counter = Session::get('assign');
        $customer=Vendors::where('v_type','Customer')->get();
        $person=Vendors::where('v_type','Saleperson')->get();
        $ware=Warehouse::all();
        $saledetail=SaleDetails::with(['sale','sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
        ->get();
        $product=Products::with(['brands','category','variants'])
        ->where('status',1)
        ->get();
        $index = 0;
        return view('sales.report',compact('customer','ware','saledetail','product','person','index','counter'));
    }

    public function GDR($gdn,$sid)
    {
        $sales=Sales::with(['warehouse','customer','biller','saleperson'])
        ->where('id',$sid)
        ->first();
        $sdetail=StockOut::with(['products','products.unit','variant'])
        ->where('gdn_no',$gdn)
        ->get();
        // dd($sales,$sdetail);
        $pdf = PDF::loadView('sales.gdr', compact('sales','sdetail','gdn'));
        // return view('porder.grr', compact('purchase','pdetail','grn'));

        return $pdf->download('GDN.pdf');
    }

    public function GDR1($id)
    {
        $sid = substr($id, strrpos($id, '-') + 1);
        $gdn = substr($id, 0, strpos($id, "-"));
        $sales=Sales::with(['warehouse','customer','biller','saleperson'])
        ->where('id',$sid)
        ->first();
        $sdetail=StockOut::with(['products','products.unit','variant'])
        ->where('gdn_no',$gdn)
        ->get();
        $pdf = PDF::loadView('sales.gdr', compact('sales','sdetail','gdn'));
        // return view('porder.grr', compact('purchase','pdetail','grn'));

        return $pdf->download('GDN.pdf');
    }

    public function delivery(Request $request)
    {
        // dd($request->all());
        // dd($request->p_total1);
        $amount = 0;
        $u_id = Auth::user()->id;
        $date=Carbon::now()->format('Y-m-d');
        $gdn_no = rand();
        $item = Sales::find($request->s_id);
        $details=SaleDetails::where('s_id',$request->s_id)
        ->get();
        $customer = Vendors::find($item->c_id);
        $tr_no = TransactionHistory::where('p_s_id',$request->s_id)
        ->where('p_type','Sales')
        ->first();

        // dd($tr_no);
        for ($j=0; $j < count($request->p_id)  ; $j++) {
            if($request->type[$j] == 0)
            {

                $product = Products::with('unit')
                ->where('id',$request->p_id[$j])
                // ->where('type',$request->type[$j])
                ->first();

                if($request->delivered_quantity[$j] == 0)
                {

                }
                else
                {
                    $prod=CurrentStock::where('p_id',$request->p_id[$j])
                    ->where('w_id',$item->w_id)
                    ->where('type',$request->type[$j])
                    ->first();

                    if($product->unit->u_name == 'Liter')
                    {
                        $unit_quantity = ( 1000) * $request->quantity[$j];
                    }
                    else if($product->unit->u_name == 'Mililiter')
                    {
                        $unit_quantity =  $request->quantity[$j];
                    }
                    else
                    {
                        $unit_quantity = null;
                    }

                    if($prod->unit_quantity == null)//changes
                    {
                        $u_quan=null;
                    }
                    else
                    {
                        $u_quan = $prod->unit_quantity - $unit_quantity;
                    }

                    $quan=$prod->quantity - $request->delivered_quantity[$j];
                    $amount += $details[$j]->sub_total;
                    CurrentStock::where('p_id',$request->p_id[$j])
                    ->where('w_id',$item->w_id)
                    ->where('type',$request->type[$j])
                    ->update([
                        'quantity' => $quan,
                        'unit_quantity' => $u_quan
                    ]);
                    subSaleQty($request->all(),$request->p_id[$j],$request->s_status,$request->s_id);
                    StockOut::create([
                        'p_id' => $request->p_id[$j],
                        'quantity' => $request->delivered_quantity[$j],
                        'stock_date' => $date,
                        'sale_d_id' => $request->id[$j],
                        'price' => $request->price[$j],
                        'created_by' => $u_id,
                        'w_id' => $request->w_id,
                        'gdn_no' => $gdn_no,
                        's_type' => 'Sales',
                        'type' => $request->type[$j]
                    ]);
                }
            }
            else
            {
                $product = ProductVariants::with('product.unit')->where('id',$request->p_id[$j])
                ->first();

                if($request->delivered_quantity[$j] == 0)
                {

                }
                else
                {
                    $prod=CurrentStock::where('p_id',$request->p_id[$j])
                    ->where('w_id',$item->w_id)
                    ->where('type',$request->type[$j])
                    ->first();

                    if($product->product->unit->u_name == 'Liter')
                    {
                        $unit_quantity = ( 1000) * $request->quantity[$j];
                    }
                    else if($product->product->unit->u_name == 'Mililiter')
                    {
                        $unit_quantity =  $request->quantity[$j];
                    }
                    else
                    {
                        $unit_quantity = null;
                    }

                    if($prod->unit_quantity == null)//changes
                    {
                        $u_quan=null;
                    }
                    else
                    {
                        $u_quan = $prod->unit_quantity - $unit_quantity;
                    }

                    $quan=$prod->quantity - $request->delivered_quantity[$j];
                    $amount += $details[$j]->sub_total;
                    CurrentStock::where('p_id',$request->p_id[$j])
                    ->where('w_id',$item->w_id)
                    ->where('type',$request->type[$j])
                    ->update([
                        'quantity' => $quan,
                        'unit_quantity' => $u_quan
                    ]);
                    StockOut::create([
                        'p_id' => $request->p_id[$j],
                        'quantity' => $request->delivered_quantity[$j],
                        'stock_date' => $date,
                        'sale_d_id' => $request->id[$j],
                        'price' => $request->price[$j],
                        'created_by' => $u_id,
                        'w_id' => $request->w_id,
                        'gdn_no' => $gdn_no,
                        's_type' => 'Sales',
                        'type' => $request->type[$j]
                    ]);
                }
            }

        }

        Sales::where('id',$request->s_id)
        ->update([
            's_status' => $request->s_status,
            'updated_by' => $u_id
        ]);

        for ($j=0; $j < count($request->p_id)  ; $j++) {
            $delivered_quantity = 0;
            $pd=SaleDetails::find($request->id[$j]);
            if($pd->delivered_quantity == 0)
            {
                SaleDetails::where('id',$request->id[$j])
                ->update([
                    'delivered_quantity' => $request->delivered_quantity[$j]
                ]);
            }
            else
            {
                $delivered_quantity = $pd->delivered_quantity + $request->delivered_quantity[$j];
                SaleDetails::where('id',$request->id[$j])
                ->update([
                    'delivered_quantity' => $delivered_quantity
                ]);
            }
        }

        $posted_date = Carbon::now()->format('Y-m-d');
        $period = Carbon::now()->format('M-y');
        $product = [];
        $account_i = [];


        $gl = GeneralLedger::max('id');
        if($gl == null)
        {
            $link_id = 1;
        }
        else
        {
            $ledger = GeneralLedger::where('id',$gl)->first();
            $link_id = $ledger->link_id + 1;
        }

        for ($j=0; $j < count($request->p_id) ; $j++) {
            if($request->type[$j] == 0)
            {
                $product[] = Products::find($request->p_id[$j]);
            }
            else
            {
                $product[] = ProductVariants::find($request->p_id[$j]);
            }
        }

        for ($i=0; $i < count($request->p_id) ; $i++){
            if($request->type[$i] == 0)
            {
                $account_i[] = AccountDetails::where('name_of_account',$product[$i]->pro_code.' - '.$product[$i]->pro_name)
                ->first();
            }
            else
            {
                $account_i[] = AccountDetails::where('name_of_account',$product[$i]->name)
                ->first();
            }
        }

        $final_amount = 0;
        $taxAmount = 0;
        $subTotals = 0;

        if($request->s_status == 'Partial')
        {
            $final_amount = $request->p_total1;
        }
        if($request->s_status == 'Complete')
        {
            if($item->discount == null && $item->tax == null)
            {
                $final_amount = $request->p_total1;
            }
            if($item->discount != null && $item->tax == null)
            {
                $final_amount = $request->p_total1 - $item->discount;
            }
            if($item->discount == null && $item->tax != null)
            {
                foreach ($details as $key => $d) {
                    $subTotals+=$d->sub_total;
                }
                $taxAmount=$subTotals/$item->tax;

                $final_amount = $request->p_total1 + $taxAmount;
            }
            if($item->discount != null && $item->tax != null)
            {
                $final_amount = $request->p_total1 - $item->discount;
                foreach ($details as $key => $d) {
                    $subTotals+=$d->sub_total;
                }
                $taxAmount=$subTotals/$item->tax;

                $final_amount = $final_amount + $taxAmount;
            }
        }
        // dd($final_amount);

        if($tr_no == null)
        {
            // dd('no');
            $account_s = AccountDetails::where('name_of_account','Sales')->first();
            $sales = GeneralLedger::where('account_code',$account_s->Code)
            ->get();
            $account_c = AccountDetails::where('name_of_account',$customer->company.' - '.$customer->name)
            ->first();
            // dd($account_c);
            $customer_l = GeneralLedger::where('account_code',$account_c->Code)
            ->get();
            $sales_n = 0;
            $sales_d = 0;
            $sales_c = 0;
            if($sales->isEmpty())
            {
                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Sales to '.$customer->name,
                    'account_name' => $account_s->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_s->Code,
                    'transaction_no' => $gdn_no,
                    'currency_code' => 'PKR',
                    'debit' => '0',
                    'credit' => $final_amount,
                    'net_value' =>  0 - $final_amount,
                    'balance' =>  0 - $final_amount
                ]);
            }
            else
            {
                $balance = 0;
                foreach ($sales as $key => $d) {
                    $sales_d+=$d->debit;
                    $sales_c+=$d->credit;
                    $balance+=$d->net_value;
                }
                $sales_c += $final_amount;
                // $sales_n = $sales_d - $sales_c;
                $sales_n = $balance + (0 - $final_amount);

                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Sales to '.$customer->name,
                    'account_name' => $account_s->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_s->Code,
                    'transaction_no' => $gdn_no,
                    'currency_code' => 'PKR',
                    'debit' => '0',
                    'credit' => $final_amount,
                    'net_value' => 0 - $final_amount,
                    'balance' => $sales_n
                ]);
            }

            $customer_d = 0;
            $customer_c = 0;
            $customer_n = 0;
            if($customer_l->isEmpty())
            {
                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Sales to'.$customer->name,
                    'account_name' => $account_c->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_c->Code,
                    'transaction_no' => $gdn_no,
                    'currency_code' => 'PKR',
                    'debit' => $final_amount,
                    'credit' => '0',
                    'net_value' => $final_amount,
                    'balance' => $final_amount
                ]);
            }
            else
            {
                $balance = 0;
                foreach ($customer_l as $key => $c) {
                    $customer_c+=$c->credit;
                    $customer_d+=$c->debit;
                    $balance+=$c->net_value;
                }
                $customer_d += $final_amount;
                // $customer_n = $customer_d - $customer_c;
                $customer_n = $balance + ($final_amount - 0);

                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Sales to '.$customer->name,
                    'account_name' => $account_c->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_c->Code,
                    'transaction_no' => $gdn_no,
                    'currency_code' => 'PKR',
                    'debit' => $final_amount,
                    'credit' => '0',
                    'net_value' => $final_amount,
                    'balance' => $customer_n
                ]);
            }
            for ($k=0; $k < count($account_i) ; $k++) {
                if($request->delivered_quantity[$k] == 0)
                {

                }
                else
                {
                    $balance = 0;
                    $net_d_p = 0;
                    $debit_p = GeneralLedger::where('account_code',$account_i[$k]->Code)
                    ->where('w_id',$item->w_id)
                    ->get();
                    // dd($debit_p);
                    if($debit_p->isEmpty())
                    {
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sale product for sale number: '.$item->id,
                            'account_name' => $account_i[$k]->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_i[$k]->Code,
                            'transaction_no' => $gdn_no,
                            'currency_code' => 'PKR',
                            'stock_out' => $request->delivered_quantity[$k],
                            'stock_in' => '0',
                            'net_value' => 0 - $request->delivered_quantity[$k],
                            'balance' => 0 - $request->delivered_quantity[$k],
                            'credit' =>  $product[$k]->cost * $request->delivered_quantity[$k],
                            'debit' => 0,
                            'w_id' => $item->w_id,
                            'type' => $request->type[$k],
                            'amount' => 0 - $product[$k]->cost
                        ]);
                    }
                    else
                    {
                        foreach ($debit_p as $key => $c) {
                            $balance+=$c->net_value;
                        }
                        $net_d_p = $balance + ( 0 - $request->delivered_quantity[$k] );
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sale product for sale number: '.$item->id,
                            'account_name' => $account_i[$k]->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_i[$k]->Code,
                            'transaction_no' => $gdn_no,
                            'currency_code' => 'PKR',
                            'stock_out' => $request->delivered_quantity[$k],
                            'stock_in' => '0',
                            'net_value' => 0 - $request->delivered_quantity[$k],
                            'balance' => $net_d_p,
                            'credit' => $product[$k]->cost * $request->delivered_quantity[$k],
                            'debit' => 0,
                            'w_id' => $item->w_id ,
                            'type' => $request->type[$k],
                            'amount' => 0 - $product[$k]->cost
                        ]);
                    }

                }
            }
        }

        else
        {
            $liability_chk = GeneralLedger::where('account_code','like','CL-02%')
            // ->where('transaction_no',$tr_no->id)
            ->where('account_name',$customer->company. ' - '.$customer->name)
            ->latest('created_at')
            // ->orderBy('id','desc')
            ->get();
            // dd($liability_chk);
            if($liability_chk->isEmpty())
            {
                // dd('dsd');
                $account_s = AccountDetails::where('name_of_account','Sales')->first();
                $sales = GeneralLedger::where('account_code',$account_s->Code)
                ->get();

                $account_c = AccountDetails::where('name_of_account',$customer->company.' - '.$customer->name)
                ->first();
                $customer_l = GeneralLedger::where('account_code',$account_c->Code)
                ->get();
                $sales_n = 0;
                $sales_d = 0;
                $sales_c = 0;
                if($sales->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Sales to '.$customer->name,
                        'account_name' => $account_s->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_s->Code,
                        'transaction_no' => $gdn_no,
                        'currency_code' => 'PKR',
                        'debit' => '0',
                        'credit' => $final_amount,
                        'net_value' =>  0 - $final_amount,
                        'balance' =>  0 - $final_amount
                    ]);
                }
                else
                {
                    $balance = 0;
                    foreach ($sales as $key => $d) {
                        $sales_d+=$d->debit;
                        $sales_c+=$d->credit;
                        $balance+=$d->net_value;
                    }
                    $sales_c += $final_amount;
                    // $sales_n = $sales_d - $sales_c;
                    $sales_n = $balance + (0 - $final_amount);

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Sales to '.$customer->name,
                        'account_name' => $account_s->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_s->Code,
                        'transaction_no' => $gdn_no,
                        'currency_code' => 'PKR',
                        'debit' => '0',
                        'credit' => $final_amount,
                        'net_value' => 0 - $final_amount,
                        'balance' => $sales_n
                    ]);
                }

                $customer_d = 0;
                $customer_c = 0;
                $customer_n = 0;
                if($customer_l->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Sales to'.$customer->name,
                        'account_name' => $account_c->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_c->Code,
                        'transaction_no' => $gdn_no,
                        'currency_code' => 'PKR',
                        'debit' => $final_amount,
                        'credit' => '0',
                        'net_value' => $final_amount,
                        'balance' => $final_amount
                    ]);
                }
                else
                {
                    $balance = 0;
                    foreach ($customer_l as $key => $c) {
                        $customer_c+=$c->credit;
                        $customer_d+=$c->debit;
                        $balance+=$c->net_value;
                    }
                    $customer_d += $final_amount;
                    // $customer_n = $customer_d - $customer_c;
                    $customer_n = $balance + ($final_amount - 0);

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Sales to '.$customer->name,
                        'account_name' => $account_c->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_c->Code,
                        'transaction_no' => $gdn_no,
                        'currency_code' => 'PKR',
                        'debit' => $final_amount,
                        'credit' => '0',
                        'net_value' => $final_amount - 0,
                        'balance' => $customer_n
                    ]);
                }
                for ($k=0; $k < count($account_i) ; $k++) {
                    if($request->delivered_quantity[$k] == 0)
                    {

                    }
                    else
                    {
                        $balance = 0;
                        $net_d_p = 0;
                        $debit_p = GeneralLedger::where('account_code',$account_i[$k]->Code)
                        ->where('w_id',$item->w_id)
                        ->get();
                        if($debit_p->isEmpty())
                        {
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sale product for sale number: '.$item->id,
                                'account_name' => $account_i[$k]->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_i[$k]->Code,
                                'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'stock_out' => $request->delivered_quantity[$k],
                                'stock_in' => '0',
                                'net_value' => 0 - $request->delivered_quantity[$k],
                                'balance' => 0 - $request->delivered_quantity[$k],
                                'credit' =>  $product[$k]->cost * $request->delivered_quantity[$k],
                                'debit' => 0,
                                'type' => $request->type[$k],
                                'w_id' => $item->w_id,
                                'amount' => 0 - $product[$k]->cost
                            ]);
                        }
                        else
                        {
                            foreach ($debit_p as $key => $value) {
                                $balance+=$value->net_value;
                            }
                            $net_d_p = $balance + (0 - $request->delivered_quantity[$k]);
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sale product for sale number: '.$item->id,
                                'account_name' => $account_i[$k]->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_i[$k]->Code,
                                'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'stock_out' => $request->delivered_quantity[$k],
                                'stock_in' => '0',
                                'net_value' => 0 - $request->delivered_quantity[$k],
                                'balance' => $net_d_p,
                                'credit' => $product[$k]->cost * $request->delivered_quantity[$k],
                                'debit' => 0,
                                'type' => $request->type[$k],
                                'w_id' => $item->w_id,
                                'amount' => 0 - $product[$k]->cost
                            ]);
                        }

                    }
                }
            }
            else
            {
                // dd('dsds');

                $liability_d = 0 ;
                $liability_c = 0 ;
                $liability_n = 0 ;
                $liability_nf = 0 ;
                $balance = 0;
                foreach ($liability_chk as $key => $d) {
                    $liability_d+=$d->debit;
                    $liability_c+=$d->credit;
                    $balance+=$d->net_value;
                }
                $liability_n = $liability_c - $liability_d;
                $liability_nf = $balance + ($final_amount - 0);
                // dd($liability_nf);
                if($liability_n == 0)
                {
                    // dd('0');
                    $account_s = AccountDetails::where('name_of_account','Sales')->first();
                    $sales = GeneralLedger::where('account_code',$account_s->Code)
                    ->get();

                    $account_c = AccountDetails::where('name_of_account',$customer->company.' - '.$customer->name)
                    ->first();
                    $customer_l = GeneralLedger::where('account_code',$account_c->Code)
                    ->get();
                    $sales_n = 0;
                    $sales_d = 0;
                    $sales_c = 0;
                    if($sales->isEmpty())
                    {
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to '.$customer->name,
                            'account_name' => $account_s->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_s->Code,
                            'transaction_no' => $gdn_no,
                            'currency_code' => 'PKR',
                            'debit' => '0',
                            'credit' => $final_amount,
                            'net_value' =>  0 - $final_amount,
                            'balance' =>  0 - $final_amount,
                        ]);
                    }
                    else
                    {
                        $balance = 0;
                        foreach ($sales as $key => $d) {
                            $sales_d+=$d->debit;
                            $sales_c+=$d->credit;
                            $balance += $d->net_value;
                        }
                        $sales_c += $final_amount;
                        // $sales_n = $sales_d - $sales_c;
                        $sales_n = $balance + (0 - $final_amount);

                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to '.$customer->name,
                            'account_name' => $account_s->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_s->Code,
                            'transaction_no' => $gdn_no,
                            'currency_code' => 'PKR',
                            'debit' => '0',
                            'credit' => $final_amount,
                            'net_value' => 0 - $final_amount,
                            'balance' => $sales_n
                        ]);
                    }

                    $customer_d = 0;
                    $customer_c = 0;
                    $customer_n = 0;
                    if($customer_l->isEmpty())
                    {
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to'.$customer->name,
                            'account_name' => $account_c->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_c->Code,
                            'transaction_no' => $gdn_no,
                            'currency_code' => 'PKR',
                            'debit' => $final_amount,
                            'credit' => '0',
                            'net_value' => $final_amount,
                            'balance' => $final_amount
                        ]);
                    }
                    else
                    {
                        $balance = 0;
                        foreach ($customer_l as $key => $c) {
                            $customer_c+=$c->credit;
                            $customer_d+=$c->debit;
                            $balance+=$c->net_value;
                        }
                        $customer_d += $final_amount;
                        // $customer_n = $customer_d - $customer_c;
                        $customer_n = $balance + ($final_amount - 0);

                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to '.$customer->name,
                            'account_name' => $account_c->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_c->Code,
                            'transaction_no' => $gdn_no,
                            'currency_code' => 'PKR',
                            'debit' => $final_amount,
                            'credit' => '0',
                            'net_value' => $final_amount - 0,
                            'balance' => $customer_n
                        ]);
                    }
                    for ($k=0; $k < count($account_i) ; $k++) {
                        if($request->delivered_quantity[$k] == 0)
                        {

                        }
                        else
                        {
                            $balance = 0;
                            $net_d_p = 0;
                            $debit_p = GeneralLedger::where('account_code',$account_i[$k]->Code)
                            ->where('w_id',$item->w_id)
                            ->get();
                            if($debit_p->isEmpty())
                            {
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$item->id,
                                    'account_name' => $account_i[$k]->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i[$k]->Code,
                                    'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $request->delivered_quantity[$k],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $request->delivered_quantity[$k],
                                    'balance' => 0 - $request->delivered_quantity[$k],
                                    'credit' => $product[$k]->cost * $request->delivered_quantity[$k],
                                    'debit' => 0,
                                    'type' => $request->type[$k],
                                    'w_id' => $item->w_id,
                                    'amount' => 0 - $product[$k]->cost
                                ]);
                            }
                            else
                            {
                                foreach ($debit_p as $key => $c) {
                                    $balance+=$c->net_value;
                                }
                                $net_d_p = $balance + ( 0 - $request->delivered_quantity[$k] );
                                GeneralLedger::create([
                                    'source' => 'Automated',
                                    'description' => 'Sale product for sale number: '.$item->id,
                                    'account_name' => $account_i[$k]->name_of_account,
                                    'link_id' => $link_id,
                                    'created_by' => $u_id,
                                    'accounting_date' => $posted_date,
                                    'posted_date' => $posted_date,
                                    'period' => $period,
                                    'account_code' => $account_i[$k]->Code,
                                    'transaction_no' => $gdn_no,
                                    'currency_code' => 'PKR',
                                    'stock_out' => $request->delivered_quantity[$k],
                                    'stock_in' => '0',
                                    'net_value' => 0 - $request->delivered_quantity[$k],
                                    'balance' => $net_d_p,
                                    'credit' => $product[$k]->cost  * $request->delivered_quantity[$k],
                                    'debit' => 0,
                                    'type' => $request->type[$k],
                                    'w_id' => $item->w_id,
                                    'amount' => 0 - $product[$k]->cost
                                ]);
                            }
                        }
                    }
                }
                if($liability_n > 0)
                {
                    // dd('greater');
                    if($liability_n > $final_amount)
                    {
                        // dd('1');
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to '.$customer->name,
                            'account_name' => $liability_chk[0]->account_name,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' =>$posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $liability_chk[0]->account_code,
                            'transaction_no' => $gdn_no,
                            'currency_code' => 'PKR',
                            'debit' => $final_amount,
                            'credit' => '0',
                            'net_value' => $final_amount ,
                            'balance' =>  $liability_nf
                        ]);
                        $account_s = AccountDetails::where('name_of_account','Sales')->first();
                        $sales = GeneralLedger::where('account_code',$account_s->Code)
                        ->get();
                        $sales_n = 0;
                        $sales_d = 0;
                        $sales_c = 0;
                        if($sales->isEmpty())
                        {
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to '.$customer->name,
                                'account_name' => $account_s->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_s->Code,
                                'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => '0',
                                'credit' => $final_amount,
                                'net_value' =>  0 - $final_amount,
                                'balance' =>  0 - $final_amount,
                            ]);
                        }
                        else
                        {
                            $balance = 0;
                            foreach ($sales as $key => $d) {
                                $sales_d+=$d->debit;
                                $sales_c+=$d->credit;
                                $balance += $d->net_value;
                            }
                            $sales_c += $final_amount;
                            // $sales_n = $sales_d - $sales_c;
                            $sales_n = $balance + (0 - $final_amount);

                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to '.$customer->name,
                                'account_name' => $account_s->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_s->Code,
                                'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => '0',
                                'credit' => $final_amount,
                                'net_value' => 0 - $final_amount,
                                'balance' => $sales_n
                            ]);
                        }
                        for ($k=0; $k < count($account_i) ; $k++) {
                            if($request->delivered_quantity[$k] == 0)
                            {

                            }
                            else
                            {
                                $balance = 0;
                                $net_d_p = 0;
                                $debit_p = GeneralLedger::where('account_code',$account_i[$k]->Code)
                                ->where('w_id',$item->w_id)
                                ->get();
                                if($debit_p->isEmpty())
                                {
                                    GeneralLedger::create([
                                        'source' => 'Automated',
                                        'description' => 'Sale product for sale number: '.$item->id,
                                        'account_name' => $account_i[$k]->name_of_account,
                                        'link_id' => $link_id,
                                        'created_by' => $u_id,
                                        'accounting_date' => $posted_date,
                                        'posted_date' => $posted_date,
                                        'period' => $period,
                                        'account_code' => $account_i[$k]->Code,
                                        'transaction_no' => $gdn_no,
                                        'currency_code' => 'PKR',
                                        'stock_out' => $request->delivered_quantity[$k],
                                        'stock_in' => '0',
                                        'net_value' => 0 - $request->delivered_quantity[$k],
                                        'balance' => 0 - $request->delivered_quantity[$k],
                                        'credit' =>  $product[$k]->cost * $request->delivered_quantity[$k],
                                        'debit' => 0,
                                        'type' => $request->type[$k],
                                        'w_id' => $item->w_id,
                                        'amount' => 0 - $product[$k]->cost
                                    ]);
                                }
                                else
                                {
                                    foreach ($debit_p as $key => $c) {
                                        $balance+=$c->net_value;
                                    }
                                    $net_d_p = $balance + ( 0 - $request->delivered_quantity[$k] );
                                    GeneralLedger::create([
                                        'source' => 'Automated',
                                        'description' => 'Sale product for sale number: '.$item->id,
                                        'account_name' => $account_i[$k]->name_of_account,
                                        'link_id' => $link_id,
                                        'created_by' => $u_id,
                                        'accounting_date' => $posted_date,
                                        'posted_date' => $posted_date,
                                        'period' => $period,
                                        'account_code' => $account_i[$k]->Code,
                                        'transaction_no' => $gdn_no,
                                        'currency_code' => 'PKR',
                                        'stock_out' => $request->delivered_quantity[$k],
                                        'stock_in' => '0',
                                        'net_value' => 0 - $request->delivered_quantity[$k],
                                        'balance' => $net_d_p,
                                        'credit' => $product[$k]->cost * $request->delivered_quantity[$k],
                                        'debit' => 0,
                                        'type' => $request->type[$k],
                                        'w_id' => $item->w_id,
                                        'amount' => 0 - $product[$k]->cost
                                    ]);
                                }
                            }
                        }
                    }
                    if($liability_n == $final_amount)
                    {
                        // dd('2');
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to '.$customer->name,
                            'account_name' => $liability_chk[0]->account_name,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' =>$posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $liability_chk[0]->account_code,
                            'transaction_no' => $gdn_no,
                            'currency_code' => 'PKR',
                            'debit' => $final_amount,
                            'credit' => '0',
                            'net_value' => $final_amount ,
                            'balance' => $liability_nf
                        ]);
                        $account_s = AccountDetails::where('name_of_account','Sales')->first();
                        $sales = GeneralLedger::where('account_code',$account_s->Code)
                        ->get();
                        $sales_n = 0;
                        $sales_d = 0;
                        $sales_c = 0;
                        if($sales->isEmpty())
                        {
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to '.$customer->name,
                                'account_name' => $account_s->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_s->Code,
                                'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => '0',
                                'credit' => $final_amount,
                                'net_value' =>  0 - $final_amount,
                                'balance' =>  0 - $final_amount
                            ]);
                        }
                        else
                        {
                            $balance = 0;
                            foreach ($sales as $key => $d) {
                                $sales_d+=$d->debit;
                                $sales_c+=$d->credit;
                                $balance+=$d->net_value;
                            }
                            $sales_c += $final_amount;
                            // $sales_n = $sales_d - $sales_c;
                            $sales_n = $balance + (0 - $final_amount);

                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to '.$customer->name,
                                'account_name' => $account_s->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_s->Code,
                                'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => '0',
                                'credit' => $final_amount,
                                'net_value' => 0 - $final_amount,
                                'balance' => $sales_n
                            ]);
                        }
                        for ($k=0; $k < count($account_i) ; $k++) {
                            if($request->delivered_quantity[$k] == 0)
                            {

                            }
                            else
                            {
                                $balance = 0;
                                $net_d_p = 0;
                                $debit_p = GeneralLedger::where('account_code',$account_i[$k]->Code)
                                ->where('w_id',$item->w_id)
                                ->get();
                                if($debit_p->isEmpty())
                                {
                                    GeneralLedger::create([
                                        'source' => 'Automated',
                                        'description' => 'Sale product for sale number: '.$item->id,
                                        'account_name' => $account_i[$k]->name_of_account,
                                        'link_id' => $link_id,
                                        'created_by' => $u_id,
                                        'accounting_date' => $posted_date,
                                        'posted_date' => $posted_date,
                                        'period' => $period,
                                        'account_code' => $account_i[$k]->Code,
                                        'transaction_no' => $gdn_no,
                                        'currency_code' => 'PKR',
                                        'stock_out' => $request->delivered_quantity[$k],
                                        'stock_in' => '0',
                                        'net_value' => 0 - $request->delivered_quantity[$k],
                                        'balance' => 0 - $request->delivered_quantity[$k],
                                        'credit' =>  $product[$k]->cost * $request->delivered_quantity[$k],
                                        'debit' => 0,
                                        'type' => $request->type[$k],
                                        'w_id' => $item->w_id,
                                        'amount' => 0 - $product[$k]->cost
                                    ]);
                                }
                                else
                                {
                                    foreach ($debit_p as $key => $c) {
                                        $balance+=$c->net_value;
                                    }
                                    $net_d_p = $balance + ( 0 - $request->delivered_quantity[$k] );
                                    GeneralLedger::create([
                                        'source' => 'Automated',
                                        'description' => 'Sale product for sale number: '.$item->id,
                                        'account_name' => $account_i[$k]->name_of_account,
                                        'link_id' => $link_id,
                                        'created_by' => $u_id,
                                        'accounting_date' => $posted_date,
                                        'posted_date' => $posted_date,
                                        'period' => $period,
                                        'account_code' => $account_i[$k]->Code,
                                        'transaction_no' => $gdn_no,
                                        'currency_code' => 'PKR',
                                        'stock_out' => $request->delivered_quantity[$k],
                                        'stock_in' => '0',
                                        'net_value' => 0 - $request->delivered_quantity[$k],
                                        'balance' => $net_d_p,
                                        'credit' =>  $product[$k]->cost * $request->delivered_quantity[$k],
                                        'debit' => 0,
                                        'type' => $request->type[$k],
                                        'w_id' => $item->w_id,
                                        'amount' => 0 - $product[$k]->cost
                                    ]);
                                }
                            }
                        }
                    }
                    if($liability_n < $final_amount)
                    {
                        // dd('3');
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to '.$customer->name,
                            'account_name' => $liability_chk[0]->account_name,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' =>$posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $liability_chk[0]->account_code,
                            'transaction_no' => $gdn_no,
                            'currency_code' => 'PKR',
                            'debit' => $liability_n,
                            'credit' => '0',
                            'net_value' => $liability_n ,
                            'balance' => $liability_nf
                        ]);
                        $account_c = AccountDetails::where('name_of_account',$customer->company.' - '.$customer->name)
                        ->first();
                        $customer_l = GeneralLedger::where('account_code',$account_c->Code)
                        ->get();
                        $customer_d = 0;
                        $customer_c = 0;
                        $customer_n = 0;
                        if($customer_l->isEmpty())
                        {
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to'.$customer->name,
                                'account_name' => $account_c->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_c->Code,
                                'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => $final_amount - $liability_n,
                                'credit' => '0',
                                'net_value' => $final_amount - $liability_n,
                                'balance' => $final_amount - $liability_n
                            ]);
                        }
                        else
                        {
                            $balance = 0;
                            foreach ($customer_l as $key => $c) {
                                $customer_c+=$c->credit;
                                $customer_d+=$c->debit;
                                $balance += $c->net_value;
                            }
                            $customer_d += $final_amount - $liability_n;
                            // $customer_n = $customer_d - $customer_c;
                            $customer_n = $balance + ($final_amount - $liability_n - 0);

                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to '.$customer->name,
                                'account_name' => $account_c->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_c->Code,
                                'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => $final_amount - $liability_n,
                                'credit' => '0',
                                'net_value' => $final_amount - $liability_n,
                                'balance' => $customer_n
                            ]);
                        }
                        $account_s = AccountDetails::where('name_of_account','Sales')->first();
                        $sales = GeneralLedger::where('account_code',$account_s->Code)
                        ->get();
                        $sales_n = 0;
                        $sales_d = 0;
                        $sales_c = 0;
                        if($sales->isEmpty())
                        {
                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to '.$customer->name,
                                'account_name' => $account_s->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_s->Code,
                                'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => '0',
                                'credit' => $final_amount,
                                'net_value' =>  0 - $final_amount,
                                'balance' =>  0 - $final_amount
                            ]);
                        }
                        else
                        {
                            $balance = 0;
                            foreach ($sales as $key => $d) {
                                $sales_d+=$d->debit;
                                $sales_c+=$d->credit;
                                $balance+=$d->net_value;
                            }
                            $sales_c += $final_amount;
                            // $sales_n = $sales_d - $sales_c;
                            $sales_n = $balance + (0 - $final_amount);

                            GeneralLedger::create([
                                'source' => 'Automated',
                                'description' => 'Sales to '.$customer->name,
                                'account_name' => $account_s->name_of_account,
                                'link_id' => $link_id,
                                'created_by' => $u_id,
                                'accounting_date' => $posted_date,
                                'posted_date' => $posted_date,
                                'period' => $period,
                                'account_code' => $account_s->Code,
                                'transaction_no' => $gdn_no,
                                'currency_code' => 'PKR',
                                'debit' => '0',
                                'credit' => $final_amount,
                                'net_value' =>  0 - $final_amount,
                                'balance' => $sales_n
                            ]);
                        }
                        for ($k=0; $k < count($account_i) ; $k++) {
                            if($request->delivered_quantity[$k] == 0)
                            {

                            }
                            else
                            {
                                $balance = 0;
                                $net_d_p = 0;
                                $debit_p = GeneralLedger::where('account_code',$account_i[$k]->Code)
                                ->where('w_id',$item->w_id)
                                ->get();
                                if($debit_p->isEmpty())
                                {
                                    GeneralLedger::create([
                                        'source' => 'Automated',
                                        'description' => 'Sale product for sale number: '.$item->id,
                                        'account_name' => $account_i[$k]->name_of_account,
                                        'link_id' => $link_id,
                                        'created_by' => $u_id,
                                        'accounting_date' => $posted_date,
                                        'posted_date' => $posted_date,
                                        'period' => $period,
                                        'account_code' => $account_i[$k]->Code,
                                        'transaction_no' => $gdn_no,
                                        'currency_code' => 'PKR',
                                        'stock_out' => $request->delivered_quantity[$k],
                                        'stock_in' => '0',
                                        'net_value' => 0 - $request->delivered_quantity[$k],
                                        'balance' => 0 - $request->delivered_quantity[$k],
                                        'credit' =>  $product[$k]->cost * $request->delivered_quantity[$k],
                                        'debit' => 0,
                                        'type' => $request->type[$k],
                                        'w_id' => $item->w_id,
                                        'amount' => 0 - $product[$k]->cost
                                    ]);
                                }
                                else
                                {
                                    foreach ($debit_p as $key => $c) {
                                        $balance+=$c->net_value;
                                    }
                                    $net_d_p = $balance + ( 0 - $request->delivered_quantity[$k] );
                                    GeneralLedger::create([
                                        'source' => 'Automated',
                                        'description' => 'Sale product for sale number: '.$item->id,
                                        'account_name' => $account_i[$k]->name_of_account,
                                        'link_id' => $link_id,
                                        'created_by' => $u_id,
                                        'accounting_date' => $posted_date,
                                        'posted_date' => $posted_date,
                                        'period' => $period,
                                        'account_code' => $account_i[$k]->Code,
                                        'transaction_no' => $gdn_no,
                                        'currency_code' => 'PKR',
                                        'stock_out' => $request->delivered_quantity[$k],
                                        'stock_in' => '0',
                                        'net_value' => 0 - $request->delivered_quantity[$k],
                                        'balance' => $net_d_p,
                                        'credit' =>  $product[$k]->cost * $request->delivered_quantity[$k],
                                        'debit' => 0,
                                        'type' => $request->type[$k],
                                        'w_id' => $item->w_id,
                                        'amount' => 0 - $product[$k]->cost
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        }




        //tax ledger entry

        // if($item->tax != null)
        // {
        //     $taxAcc = AccountDetails::where('name_of_account','Tax')
        //     ->first();
        //     $tax = $amount / $item->tax;
        //     GeneralLedger::create([
        //         'source' => 'Automated',
        //         'description' => 'Tax for sale number: '.$item->ref_no,
        //         'account_name' => $taxAcc->name_of_account,
        //         'link_id' => $link_id,
        //         'created_by' => $u_id,
        //         'accounting_date' => $posted_date,
        //         'posted_date' => $posted_date,
        //         'period' => $period,
        //         'account_code' => $taxAcc->Code,
        //         'transaction_no' => $item->ref_no,
        //         'currency_code' => 'PKR',
        //         'credit' => $tax,
        //         'debit' => '0',
        //         'net_value' => $tax,
        //         'amount' => $item->tax
        //     ]);
        //     //discount ledger entry
        // }

        // if($item->discount != null)
        // {
        //     if($item->discount_type == 'Trade')
        //     {

        //     }
        //     else
        //     {
        //         $dis = AccountDetails::where('name_of_account','Discount')
        //         ->first();
        //         GeneralLedger::create([
        //             'source' => 'Automated',
        //             'description' => 'Discount for sale number: '.$item->ref_no,
        //             'account_name' => $dis->name_of_account,
        //             'link_id' => $link_id,
        //             'created_by' => $u_id,
        //             'accounting_date' => $posted_date,
        //             'posted_date' => $posted_date,
        //             'period' => $period,
        //             'account_code' => $dis->Code,
        //             'transaction_no' => $item->ref_no,
        //             'currency_code' => 'PKR',
        //             'debit' => $item->discount,
        //             'credit' => '0',
        //             'net_value' => $item->discount
        //         ]);
        //     }
        // }

            // return redirect('/sales');
        return $this->GDR($gdn_no,$request->s_id);


    }

    public function return(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d');
        $u_id = Auth::user()->id;
        $sales = Sales::find($request->s_id);
        $r_type = $sales->s_type;

        // dd($sales);
        Sales::where('id',$request->s_id)->update(['return_status' => 'Requested']);
        $s = SaleReturn::create([
            'return_date' => $date,
            'c_id' => $sales->c_id,
            'sale_id' => $request->s_id,
            'w_id' => $sales->w_id,
            'status' => 'Pending',
            'total' => $sales->total,
            'created_by' => $u_id,
            'r_type' => $r_type,
        ]);

        for ($i=0; $i <count($request->id) ; $i++) {
            SaleReturnDetails::create([
                'ret_id' => $s->id,
                'sd_id' => $request->id[$i],
                'quantity' => $request->returnQty[$i]
            ]);
        }
        toastr()->success('Sale Return Request Added successfully!');
        return redirect(url('').'/sales');
    }


    public function returnView(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        $return=SaleReturn::with(['warehouse','customer'])
        ->get();
        $role_id = Auth::user()->r_id;
        $env_a_id = env('ADMIN_ID');
        $env_m_id = env('MANAGER_ID');

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $counter = null;
        }
        else
        {
            $counter = Session::get('assign');
        }

        $index = 0;
        // dd($return);
        return view('salereturn.index',compact('permissions','return','index','menu_id','counter'));
    }

    public function returnShow($id)
    {
        $p=SaleReturn::with(['warehouse','customer'])
        ->where('id',$id)
        ->first();
        $d=SaleReturnDetails::with(['saledetail','saledetail.products','saledetail.products.brands','saledetail.variant','saledetail.variant.product.brands'])
        ->where('ret_id',$id)
        ->get();
        return [$p,$d];
    }

    public function returnStatus(Request $request)
    {
        // dd($request->all());
        $id = $request->id;
        $total = 0;
        $gl = GeneralLedger::max('id');
        $ledger1 = GeneralLedger::where('id',$gl)->first();
        $link_id = $ledger1->link_id + 1;
        $u_id = Auth::user()->id;
        $date = Carbon::now()->format('Y-m-d');
        $status = $request->status;
        $posted_date = Carbon::now()->format('Y-m-d');
        $period = Carbon::now()->format('M-y');

        if($status == 'Approved')
        {
            $return = SaleReturn::with(['customer'])->where('id',$id)->first();
            if($return->r_type == 'Pos')
            {
                SaleCounter::create([
                    'u_id' => $u_id,
                    'co_id' => 1,
                    's_id' => $return->sale_id,
                    'date' => $posted_date,
                ]);
            }
            // dd($return);
            if ($return->update(['status' => $status])) {
                SaleReturn::where('id',$id)
                ->update([
                    'status' => $status,
                ]);

            }
            $detailsQty = SaleReturnDetails::where('ret_id',$id)->sum('quantity');
            $sdetailsQty = SaleDetails::where('s_id',$return->sale_id)->sum('quantity');
            $details = SaleReturnDetails::where('ret_id',$id)->get();
            if($detailsQty == $sdetailsQty)
            {
                Sales::where('id',$return->sale_id)
                ->update([
                    'return_status' => 'Complete',
                    'p_status' => 'Return',
                    's_status' => 'Return',
                ]);
            }
            if($detailsQty < $sdetailsQty)
            {
                Sales::where('id',$return->sale_id)
                ->update([
                    'return_status' => 'Partial',
                    'p_status' => 'Partial',
                    's_status' => 'Partial',
                ]);
            }
            for($i = 0 ; $i < count($details) ; $i++)
            {
                SaleDetails::where('id',$details[$i]->sd_id)->update(['returnQty' => $details[$i]->quantity]);
                $sdetails = SaleDetails::with(['products.unit','variant.product.unit'])->where('id',$details[$i]->sd_id)->first();
                $total +=$details[$i]->quantity * $sdetails->price;
                if($sdetails->type == 0)
                {
                    $account = AccountDetails::where('name_of_account',$sdetails->products->pro_code.' - '.$sdetails->products->pro_name)
                    ->first();
                    $unit = $sdetails->products->unit->u_name;
                    $cost = $sdetails->products->cost;
                }
                else {
                    $account = AccountDetails::where('name_of_account',$sdetails->variant->name)
                    ->first();
                    $unit = $sdetails->variant->product->unit->u_name;
                    $cost = $sdetails->variant->cost;
                }

                Stocks::create([
                    'p_id' => $sdetails->p_id,
                    'w_id' => $return->w_id,
                    'quantity' => $details[$i]->quantity,
                    'stock_date' => $date,
                    's_type' => 'Return',
                    'cost' => $cost,
                    'created_by' => $u_id,
                    'type' => $sdetails->type
                ]);
                $cs=CurrentStock::where('p_id',$sdetails->p_id)
                ->where('w_id',$return->w_id)
                ->where('type', $sdetails->type)
                ->first();

                if($unit == 'Liter')
                {
                    $unit_quantity = (1000) * $details[$i]->quantity;
                }
                else if($unit == 'Mililiter')
                {
                    $unit_quantity = $details[$i]->quantity;
                }
                else
                {
                    $unit_quantity = null;
                }
                if($cs == null)
                {
                    CurrentStock::create([
                        'p_id' => $sdetails->p_id,
                        'w_id' => $return->w_id,
                        'quantity' => $details[$i]->quantity,
                        'unit_quantity' => $unit_quantity,
                        'type' => $sdetails->type
                    ]);
                }

                else
                {
                    $quan = $cs->quantity + $details[$i]->quantity;
                    if($unit_quantity == null)
                    {
                        $u_quan=null;
                    }
                    else
                    {
                        $u_quan = $cs->unit_quantity + $unit_quantity;
                    }
                    CurrentStock::where('p_id',$sdetails->p_id)
                    ->where('w_id',$return->w_id)
                    ->where('type',$sdetails->type)
                    ->update([
                        'quantity' => $quan,
                        'unit_quantity' => $u_quan
                    ]);
                }

                $balance = 0;
                $net_d_p = 0;
                $debit_p = GeneralLedger::where('account_code',$account->Code)
                ->where('w_id',$return->w_id)
                ->get();
                foreach ($debit_p as $key => $c) {
                    $balance+=$c->net_value;
                }
                $net_d_p = $balance + ( $details[$i]->quantity - 0);
                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Product Return On Sale Number '.$return->sale_id,
                    'account_name' => $account->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account->Code,
                    'currency_code' => 'PKR',
                    'stock_in' => $details[$i]->quantity,
                    'stock_out' => '0',
                    'net_value' => $details[$i]->quantity,
                    'amount' => ($cost),
                    'balance' => $net_d_p,
                    'debit' => ($cost * $details[$i]->quantity),
                    'credit' => 0,
                    'type' => $sdetails->type,
                    'w_id' => $return->w_id
                ]);
            }

            TransactionHistory::create([
                'p_s_id' => $return->sale_id,
                'p_type' => 'Return',
                't_type' => 'Sent',
                'paid_by' => 'Cash',
                'total' => $total,
                'created_by' => $u_id
            ]);
            $ref_no = TransactionHistory::max('id');
            $balance_c = 0;
            $net_c = 0;
            $debit_c = GeneralLedger::where('account_code','CA-03-001')
            ->get();
            foreach ($debit_c as $key => $c) {
                $balance_c+=$c->net_value;
            }
            $net_c = $balance_c + (0 - $total);
            GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Amount return on sale number: '.$return->sale_id,
                'account_name' => 'Cash In Hand',
                'link_id' => $link_id,
                'created_by' => $u_id,
                'accounting_date' => $posted_date,
                'posted_date' => $posted_date,
                'period' => $period,
                'account_code' => 'CA-03-001',
                'transaction_no' => $ref_no,
                'currency_code' =>'PKR',
                'debit' => '0',
                'credit' => $total,
                'net_value' => 0 - $total,
                'balance' => $net_c
            ]);

            $balancecus = 0 ;
            $netCus = 0;
            $debit_cus = GeneralLedger::where('account_name',$return->customer->company.' - '.$return->customer->name)
            ->get();
            foreach ($debit_cus as $key => $c) {
                $balancecus+=$c->net_value;
            }
            $netCus = $balancecus + ( $total - 0);

            GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Amount return on sale number: '.$return->sale_id,
                'account_name' => $return->customer->company.' - '.$return->customer->name,
                'link_id' => $link_id,
                'created_by' => $u_id,
                'accounting_date' => $posted_date,
                'posted_date' => $posted_date,
                'period' => $period,
                'transaction_no' => $ref_no,
                'account_code' => $debit_cus[0]->account_code,
                'currency_code' => 'PKR',
                'debit' => $total,
                'credit' => '0',
                'net_value' => $total,
                'balance' => $netCus
            ]);

            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        else
        {
            return response()->json($response, 409);
        }
    }

    public function status(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
        $u_id = Auth::user()->id;
        // $gdn_no = rand();

        if($status == 'Pending' || $status == 'Approved' || $status == 'Delivered')
        {
            $item = Sales::find($id);
            if ($item->update(['s_status' => $status])) {
                Sales::where('id',$id)
                ->update([
                    's_status' => $status,
                    'updated_by' => $u_id
                ]);
                $response['status'] = $status;
                $response['message'] = 'status updated successfully.';
                return response()->json($response, 200);
            }
        }
        else
        {
            return response()->json($response, 409);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $menu_id =   getMenuId($request);
        $sales=Sales::max('id');
        // dd($sales);
        if($sales == null)
        {
            $id=1;
        }
        else
        {
            $id=$sales+1;
        }
        $biller=Vendors::where('v_type','Biller')->where('status',1)->get();
        $person=Vendors::where('v_type','Saleperson')->where('status',1)->get();
        $customer=Vendors::where('v_type','Customer')->where('status',1)->get();
        $ware=Warehouse::all();
        $bank=Bank::where('status',1)->get();
        // $product=Products::with(['brands','unit','category','subcategory'])
        // ->where('status',1)
        // ->get();
        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $city = City::where('status',1)->get();
        $cgroup=Groups::where('g_type','Customer')->get();
        $pgroup=Groups::where('g_type','Price')->get();
        $date=Carbon::now()->format('Y-m-d');
        $data=[
            'isEdit' => false,
            'customer' => $customer,
            'biller' => $biller,
            'person' => $person,
            'ware' => $ware,
            'unit' => $unit,
            'cat' => $cat,
            'bank' => $bank,
            'sub' => $sub,
            'brands' => $brands,
            // 'product' => $product,
            'city' => $city,
            'cgroup' => $cgroup,
            'pgroup' => $pgroup,
            'date' => $date,
            'id' => $id,
            'permissions' => getRolePermission($menu_id)
        ];
        return view('sales.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //isset doc, baqyin ma doc dekhna
        // dd($request->all());
        $request->validate([
            'ref_no'      =>  'required|max:11|unique:sales'
        ]);
        $s_status='';
        $u_id = Auth::user()->id;
        if(isset($request->p_status))
        {
            $p_status = $request->p_status;
        }
        else
        {
            $p_status = $request->p_status1;
        }
        $s_status = 'Pending';

        if(isset($request->doc))
        {
            $data=([
                'ref_no' => $request->ref_no,
                'sale_date' => $request->sale_date,
                'b_id' => $request->b_id,
                'c_id' => $request->c_id,
                'w_id' => $request->w_id,
                's_address' => $request->s_address,
                's_status' => $s_status,
                'p_status' => $p_status,
                'doc' => $request->doc,
                'tax' => $request->tax,
                'total' => $request->final,
                'note' => $request->editor1,
                'created_by' => $u_id,
                'sp_id' => $request->sp_id,
                'discount_type' => $request->discount_type,
                'expected_date' => $request->expected_date,
                'advance' => $request->advance,
                'tax_status' => $request->tax_status,
                'pay_type' => $request->pay_type,
                'return_status' => 'No Return',
            ]);
            $data['doc']= Storage::disk('uploads')->putFile('',$request->doc);
        }
        else
        {
            $data=([
                'ref_no' => $request->ref_no,
                'sale_date' => $request->sale_date,
                'b_id' => $request->b_id,
                'c_id' => $request->c_id,
                'w_id' => $request->w_id,
                's_address' => $request->s_address,
                's_status' => $s_status,
                'p_status' => $p_status,
                'tax' => $request->tax,
                'total' => $request->final,
                'note' => $request->editor1,
                'created_by' => $u_id,
                'sp_id' => $request->sp_id,
                'discount_type' => $request->discount_type,
                'expected_date' => $request->expected_date,
                'advance' => $request->advance,
                'tax_status' => $request->tax_status,
                'pay_type' => $request->pay_type,
                'return_status' => 'No Return',
            ]);
        }
        $order_no = createOrder($request->all());
        $data['order_no_w'] = $order_no->id;

        // $p=Sales::create($data);
        dd("adfadsf");

        for ($i=0; $i <count($request->p_id) ; $i++) {
            SaleDetails::create([
                's_id' => $p->id,
                'p_id' => $request->p_id[$i],
                'quantity' => $request->quantity[$i],
                'sub_total' => $request->sub_total[$i],
                'price' => $request->price[$i],
                'discount_percent' => $request->discount_percent[$i],
                'discounted_amount' => $request->discounted_amount[$i],
                'type' => $request->type[$i],
            ]);
        }
        $order_no = createOrder($request->all());
        $total =0;
        if($request->p_total1 ==  '' && $request->p_total != null)
        {
            $total = $request->p_total;
        }
        else
        {
            $total = $request->p_total1;
        }
        if($p_status != 'Pending')
        {
            TransactionHistory::create([
                'p_s_id' =>  $p->id,
                'p_type' => 'Sales',
                't_type' => 'Received',
                'paid_by' => $request->paid_by,
                // 'ref_no' => $request->p_ref_no,
                'total' => $total,
                'cheque_no' => $request->cheque_no,
                'cc_no' => $request->cc_no,
                'gift_no' => $request->gift_no,
                'cc_holder' => $request->cc_holder,
                'note' => $request->editor2,
                'b_id' => $request->bank_id,
                'created_by' => $u_id
            ]);
        }
        $ref_no = TransactionHistory::max('id');
        ////cash, bank and receivables ledger entry
        if($p_status == 'Partial')
        {
            //ledger entry
            $gl = GeneralLedger::max('id');
            $posted_date = Carbon::now()->format('Y-m-d');
            $period = Carbon::now()->format('M-y');
            $liability_d = 0 ;
            $liability_c = 0 ;
            $liability_n = 0 ;
            if($gl == null)
            {
                $link_id = 1;
            }
            else
            {
                $ledger = GeneralLedger::where('id',$gl)->first();
                $link_id = $ledger->link_id + 1;
            }

            $customer=Vendors::find($request->c_id);
            //liability ledger entry
            $account = AccountDetails::where('Code','like','CL-02')->latest('created_at')->first();;
            $hcat = HeadCategory::where('code','like','CL-02')->first();
            // dd($hcat);
            if($account == null)
            {
                $id = 001;
            }
            else
            {
                $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
            }
            $str_length = strlen((string)$id)+2;
            $id = substr("0000{$id}", -$str_length);
            $code = $hcat->code.'-'.$id;
            $accountName = $customer->company.' - '.$customer->name;

            $account_l=AccountDetails::where('name_of_account',$accountName)
            ->where('c_id',$hcat->id)
            ->first();
            if($account_l == null)
            {
                $data = [
                    'Code' => $code,
                    'name_of_account' => $accountName,
                    'c_id' => $hcat->id,
                    'created_by' => $u_id
                ];
                $liability = AccountDetails::create($data);
                $cash_d = 0;
                $cash_c = 0;
                $cash_n = 0;
                $bank_d = 0;
                $bank_c = 0;
                $bank_n = 0;
                if($request->paid_by == 'Cash' || $request->paid_by == 'Gift')
                {
                    $account_cash = AccountDetails::where('name_of_account','Cash In Hand')->first();
                    $cash = GeneralLedger::where('account_code',$account_cash->Code)
                    ->get();
                    if($cash->isEmpty())
                    {
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Amount Paid by: '.$customer->name,
                            'account_name' => $account_cash->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_cash->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' =>'PKR',
                            'debit' => $total,
                            'credit' => '0',
                            'net_value' => $total - 0,
                            'balance' => $total - 0
                        ]);
                    }
                    else
                    {
                        $balance = 0;
                        foreach ($cash as $key => $c) {
                            $cash_c+=$c->credit;
                            $cash_d+=$c->debit;
                            $balance+=$c->net_value;
                        }
                        $cash_d += $total;
                        // $cash_n = $cash_d - $cash_c;
                        $cash_n = $balance + ($total - 0);

                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Amount Paid by: '.$customer->name,
                            'account_name' => $account_cash->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_cash->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' =>'PKR',
                            'debit' => $total,
                            'credit' => '0',
                            'net_value' => $total - 0,
                            'balance' => $cash_n
                        ]);
                    }
                }
                else
                {
                    $bank=Bank::find($request->bank_id);
                    $account_b = AccountDetails::where('name_of_account',$bank->name.' - '.$bank->branch)->first();
                    $bank_l = GeneralLedger::where('account_code',$account_b->Code)
                    ->get();
                    if($bank_l->isEmpty())
                    {
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Amount Paid By: '.$customer->name,
                            'account_name' => $account_b->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_b->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' =>'PKR',
                            'debit' => $total,
                            'credit' => '0',
                            'net_value' => $total - 0,
                            'balance' => $total - 0
                        ]);
                    }
                    else
                    {
                        $balance = 0;
                        foreach ($bank_l as $key => $c) {
                            $bank_c+=$c->credit;
                            $bank_d+=$c->debit;
                            $balance+=$c->net_value;
                        }
                        $bank_d += $total;
                        // $bank_n = $bank_d - $bank_c;
                        $bank_n = $balance + ($total - 0);

                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Amount Paid By: '.$customer->name,
                            'account_name' => $account_b->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_b->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' =>'PKR',
                            'credit' => '0',
                            'debit' => $total,
                            'net_value' => $total - 0,
                            'balance' => $bank_n
                        ]);
                    }


                }

                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Sales to '.$customer->name,
                    'account_name' => $liability->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' =>$posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $liability->Code,
                    'transaction_no' => $ref_no,
                    'currency_code' => 'PKR',
                    'debit' => '0',
                    'credit' => $total,
                    'net_value' => 0 - $total,
                    'balance' => 0 - $total
                ]);
            }
            else
            {
                $liability = GeneralLedger::where('account_code',$account_l->Code)
                ->get();
                $cash_d = 0;
                $cash_c = 0;
                $cash_n = 0;
                $bank_d = 0;
                $bank_c = 0;
                $bank_n = 0;
                if($request->paid_by == 'Cash' || $request->paid_by == 'Gift')
                {
                    $account_cash = AccountDetails::where('name_of_account','Cash In Hand')->first();
                    $cash = GeneralLedger::where('account_code',$account_cash->Code)
                    ->get();
                    if($cash->isEmpty())
                    {
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Amount Paid by: '.$customer->name,
                            'account_name' => $account_cash->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_cash->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' =>'PKR',
                            'debit' => $total,
                            'credit' => '0',
                            'net_value' => $total - 0,
                            'balance' => $total - 0
                        ]);
                    }
                    else
                    {
                        $balance = 0;
                        foreach ($cash as $key => $c) {
                            $cash_c+=$c->credit;
                            $cash_d+=$c->debit;
                            $balance+=$c->net_value;
                        }
                        $cash_d += $total;
                        // $cash_n = $cash_d - $cash_c;
                        $cash_n = $balance + ($total - 0);

                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Amount Paid by: '.$customer->name,
                            'account_name' => $account_cash->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_cash->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' =>'PKR',
                            'debit' => $total,
                            'credit' => '0',
                            'net_value' => $total - 0,
                            'balance' => $cash_n
                        ]);
                    }
                }
                else
                {
                    $bank=Bank::find($request->bank_id);
                    $account_b = AccountDetails::where('name_of_account',$bank->name.' - '.$bank->branch)->first();
                    $bank_l = GeneralLedger::where('account_code',$account_b->Code)
                    ->get();
                    if($bank_l->isEmpty())
                    {
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Amount Paid By: '.$customer->name,
                            'account_name' => $account_b->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_b->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' =>'PKR',
                            'debit' => $total,
                            'credit' => '0',
                            'net_value' => $total - 0,
                            'balance' => $total - 0,
                        ]);
                    }
                    else
                    {
                        $balance = 0;
                        foreach ($bank_l as $key => $c) {
                            $bank_c+=$c->credit;
                            $bank_d+=$c->debit;
                            $balance+=$c->net_value;
                        }
                        $bank_d += $total;
                        // $bank_n = $bank_d - $bank_c;
                        $bank_n = $balance + ($total - 0);
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Amount Paid By: '.$customer->name,
                            'account_name' => $account_b->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_b->Code,
                            'transaction_no' => $ref_no,
                            'credit' => '0',
                            'currency_code' =>'PKR',
                            'debit' => $total,
                            'net_value' => $total - 0,
                            'balance' => $bank_n
                        ]);
                    }


                }
                $balance = 0;
                foreach ($liability as $key => $d) {
                    $liability_d+=$d->debit;
                    $liability_c+=$d->credit;
                    $balance+=$d->net_value;
                }
                $liability_c += $total;
                // $liability_n = $liability_d - $liability_c;
                $liability_n = $balance + (0 - $total);

                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Sales to '.$customer->name,
                    'account_name' => $account_l->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_l->Code,
                    'transaction_no' => $ref_no,
                    'currency_code' => 'PKR',
                    'debit' => '0',
                    'credit' => $total,
                    'net_value' => 0 - $total,
                    'balance' => $liability_n
                ]);
            }

        }

        if($p_status == 'Paid')
        {
            //ledger entry
            $gl = GeneralLedger::max('id');
            $posted_date = Carbon::now()->format('Y-m-d');
            $period = Carbon::now()->format('M-y');
            $liability_d = 0 ;
            $liability_c = 0 ;
            $liability_n = 0 ;
            if($gl == null)
            {
                $link_id = 1;
            }
            else
            {
                $ledger = GeneralLedger::where('id',$gl)->first();
                $link_id = $ledger->link_id + 1;
            }
            $customer=Vendors::find($request->c_id);
            //liability ledger entry
            $account = AccountDetails::where('Code','like','CL-02')->first();
            $hcat = HeadCategory::where('code','like','CL-02')->first();
            // dd($hcat);
            if($account == null)
            {
                $id = 001;
            }
            else
            {
                $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
            }
            $str_length = strlen((string)$id)+2;
            $id = substr("0000{$id}", -$str_length);
            $code = $hcat->code.'-'.$id;
            $accountName = $customer->company.' - '.$customer->name;

            $account_l=AccountDetails::where('name_of_account',$accountName)
            ->where('c_id',$hcat->id)
            ->first();
            if($account_l == null)
            {
                $data = [
                    'Code' => $code,
                    'name_of_account' => $accountName,
                    'c_id' => $hcat->id,
                    'created_by' => $u_id
                ];
                $liability = AccountDetails::create($data);
                $cash_d = 0;
                $cash_c = 0;
                $cash_n = 0;
                $bank_d = 0;
                $bank_c = 0;
                $bank_n = 0;
                if($request->paid_by == 'Cash' || $request->paid_by == 'Gift')
                {
                    $account_cash = AccountDetails::where('name_of_account','Cash In Hand')->first();
                    $cash = GeneralLedger::where('account_code',$account_cash->Code)
                    ->get();
                    if($cash->isEmpty())
                    {
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Amount Paid by: '.$customer->name,
                            'account_name' => $account_cash->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_cash->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' =>'PKR',
                            'debit' => $total,
                            'credit' => '0',
                            'net_value' => $total - 0,
                            'balance' => $total - 0
                        ]);
                    }
                    else
                    {
                        $balance = 0;
                        foreach ($cash as $key => $c) {
                            $cash_c+=$c->credit;
                            $cash_d+=$c->debit;
                            $balance+=$c->net_value;
                        }
                        $cash_d += $total;
                        // $cash_n = $cash_d - $cash_c;
                        $cash_n = $balance + ($total - 0);

                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Amount Paid by: '.$customer->name,
                            'account_name' => $account_cash->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_cash->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' =>'PKR',
                            'debit' => $total,
                            'credit' => '0',
                            'net_value' => $total - 0,
                            'balance' => $cash_n
                        ]);
                    }
                }
                else
                {
                    $bank=Bank::find($request->bank_id);
                    $account_b = AccountDetails::where('name_of_account',$bank->name.' - '.$bank->branch)->first();
                    $bank_l = GeneralLedger::where('account_code',$account_b->Code)
                    ->get();
                    if($bank_l->isEmpty())
                    {
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Amount Paid By: '.$customer->name,
                            'account_name' => $account_b->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_b->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' =>'PKR',
                            'debit' => $total,
                            'credit' => '0',
                            'net_value' => $total - 0,
                            'balance' => $total - 0
                        ]);
                    }
                    else
                    {
                        $balance = 0;
                        foreach ($bank_l as $key => $c) {
                            $bank_c+=$c->credit;
                            $bank_d+=$c->debit;
                            $balance+=$c->net_value;
                        }
                        $bank_d += $total;
                        $bank_n = $balance + ($total - 0);
                        // $bank_n = $bank_d - $bank_c;

                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Amount Paid By: '.$customer->name,
                            'account_name' => $account_b->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_b->Code,
                            'transaction_no' => $ref_no,
                            'credit' => '0',
                            'currency_code' =>'PKR',
                            'debit' => $total,
                            'net_value' => $total - 0,
                            'balance' => $bank_n
                        ]);
                    }


                }

                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Sales to '.$customer->name,
                    'account_name' => $liability->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' =>$posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $liability->Code,
                    'transaction_no' => $ref_no,
                    'currency_code' => 'PKR',
                    'debit' => '0',
                    'credit' => $total,
                    'net_value' => 0 - $total,
                    'balance' => 0 - $total
                ]);
            }
            else
            {
                $liability = GeneralLedger::where('account_code',$account_l->Code)
                ->get();
                $cash_d = 0;
                $cash_c = 0;
                $cash_n = 0;
                $bank_d = 0;
                $bank_c = 0;
                $bank_n = 0;
                if($request->paid_by == 'Cash' || $request->paid_by == 'Gift')
                {
                    $account_cash = AccountDetails::where('name_of_account','Cash In Hand')->first();
                    $cash = GeneralLedger::where('account_code',$account_cash->Code)
                    ->get();
                    if($cash->isEmpty())
                    {
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Amount Paid by: '.$customer->name,
                            'account_name' => $account_cash->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_cash->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' =>'PKR',
                            'debit' => $total,
                            'credit' => '0',
                            'net_value' => $total - 0,
                            'balance' => $total - 0
                        ]);
                    }
                    else
                    {
                        $balance = 0;
                        foreach ($cash as $key => $c) {
                            $cash_c+=$c->credit;
                            $cash_d+=$c->debit;
                            $balance+=$c->net_value;
                        }
                        $cash_d += $total;
                        // $cash_n = $cash_d - $cash_c;
                        $cash_n = $balance + ($total - 0);

                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Amount Paid by: '.$customer->name,
                            'account_name' => $account_cash->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_cash->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' =>'PKR',
                            'debit' => $total,
                            'credit' => '0',
                            'net_value' => $total - 0,
                            'balance' => $cash_n
                        ]);
                    }
                }
                else
                {
                    $bank=Bank::find($request->bank_id);
                    $account_b = AccountDetails::where('name_of_account',$bank->name.' - '.$bank->branch)->first();
                    $bank_l = GeneralLedger::where('account_code',$account_b->Code)
                    ->get();
                    if($bank_l->isEmpty())
                    {
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Amount Paid By: '.$customer->name,
                            'account_name' => $account_b->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_b->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' =>'PKR',
                            'debit' => $total,
                            'credit' => '0',
                            'net_value' => $total - 0,
                            'balance' => $total - 0
                        ]);
                    }
                    else
                    {
                        $balance = 0;
                        foreach ($bank_l as $key => $c) {
                            $bank_c+=$c->credit;
                            $bank_d+=$c->debit;
                            $balance+=$c->net_value;
                        }
                        $bank_d += $total;
                        // $bank_n = $bank_d - $bank_c;
                        $bank_n = $balance + ($total - 0);

                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Amount Paid By: '.$customer->name,
                            'account_name' => $account_b->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_b->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' =>'PKR',
                            'debit' => $total,
                            'credit' => '0',
                            'net_value' => $total - 0,
                            'balance' => $bank_n
                        ]);
                    }


                }
                $balance = 0;
                foreach ($liability as $key => $d) {
                    $liability_d+=$d->debit;
                    $liability_c+=$d->credit;
                    $balance+=$d->net_value;
                }
                $liability_c += $total;
                // $liability_n = $liability_d - $liability_c;
                $liability_n = $balance + (0 - $total);

                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Sales to '.$customer->name,
                    'account_name' => $account_l->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_l->Code,
                    'transaction_no' => $ref_no,
                    'currency_code' => 'PKR',
                    'debit' => '0',
                    'credit' => $total,
                    'net_value' => 0 - $total,
                    'balance' => $liability_n
                ]);
            }
        }

        for ($j=0; $j < count($request->price) ; $j++) {
            if($request->type[$j] == 1)
            {
                ProductVariants::where('id',$request->p_id[$j])
                ->update([
                    'price' => $request->price[$j]
                ]);
            }
            else
            {
                Products::where('id',$request->p_id[$j])
                ->update([
                    'price' => $request->price[$j]
                ]);
            }
        }

        toastr()->success('Sales Added successfully!');
        // Session::flash('download', '/sales/pdf');
        return redirect(url('').'/sales');
    }

    public function pdf1()
    {
        $id=Sales::max('id');
        return $this->pdf($id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $p=Sales::with(['warehouse','customer','biller','saleperson'])
        ->where('id',$id)
        ->first();
        $d=SaleDetails::with(['products','products.brands','products.unit','sale','variant','variant.product.brands','variant.product.unit'])
        ->where('s_id',$id)
        ->get();
        return [$p,$d];
    }

    public function pdf($id)
    {
        $credit = 0;
        $debit = 0;
        $sales=Sales::with(['warehouse','customer','biller','saleperson'])
        ->where('id',$id)
        ->first();
        $saledetail=SaleDetails::with(['products','products.brands','products.unit','sale','variant','variant.product.brands','variant.product.unit'])
        ->where('s_id',$id)
        ->get();
        $th=TransactionHistory::where('p_s_id',$id)
        ->where('p_type','Sales')
        ->sum('total');
        $account=AccountDetails::with('generalLedger')->where('name_of_account','Like','%'.$sales->customer->name)
        ->where('Code','like','CA-01%')
        ->first();
        if($account!=null)
        {
            for($k = 0 ; $k < count($account->generalLedger) ; $k++)
            {
                $credit += $account->generalLedger[$k]->credit;

                $debit += $account->generalLedger[$k]->debit;
            }
            $balance =  $debit - $credit;
        }
        else {
            $balance = 0;
        }
        // dd($th);
        $pdf = PDF::loadView('sales.pdf', compact('sales','saledetail','th','balance'));
        // return view('sales.pdf', compact('sales','saledetail'));

        return $pdf->download('sales.pdf');
        // return redirect()->back();
    }

    public function document($id)
    {
        $sales = Sales::find($id);
        $file= public_path(). "/uploads/". $sales->doc;
        // return  Storage::download($file);
        $headers = array(
                'Content-Type: application/pdf',
                );

        return Response::download($file, $sales->doc, $headers);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = UserMenu::where('route','sales.create')->first();
        $permissions     =   getRolePermission($menu->id);
        // dd($permissions);
        $sale=Sales::with(['warehouse','customer','biller','saleperson'])
        ->where('id',$id)
        ->first();
        $saledetail=SaleDetails::with(['products.currentstocks','products','products.brands','products.unit','sale',
        'variant.currentstocks','variant.product.brands','variant.product.unit'])

        ->where('s_id',$id)
        ->get();
        // dd($saledetail);
        $biller=Vendors::where('v_type','Biller')->where('status',1)->get();
        $person=Vendors::where('v_type','Saleperson')->where('status',1)->get();
        $customer=Vendors::where('v_type','Customer')->where('status',1)->get();
        $ware=Warehouse::all();
        $bank=Bank::all();
        $product=CurrentStock::with(['products','products.brands','products.unit','products.category','products.subcategory','variant.product.brands'])
        ->where('w_id',$sale->w_id)
        ->get();
        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $city = City::where('status',1)->get();
        $cgroup=Groups::where('g_type','Customer')->get();
        $pgroup=Groups::where('g_type','Price')->get();
        $date=Carbon::now()->format('Y-m-d');
        $data=[
            'isEdit' => true,
            'customer' => $customer,
            'biller' => $biller,
            'person' => $person,
            'ware' => $ware,
            'unit' => $unit,
            'cat' => $cat,
            'bank' => $bank,
            'sub' => $sub,
            'brands' => $brands,
            'product' => $product,
            'city' => $city,
            'cgroup' => $cgroup,
            'pgroup' => $pgroup,
            'date' => $date,
            'id' => $id,
            'sale' => $sale,
            'saledetail' => $saledetail,
            'permissions' => $permissions
        ];
        return view('sales.create',$data);
    }


    public function GDN($id)
    {
        // dd($id);
        $p = Sales::find($id);
        $purchase = SaleDetails::with('stock','products','variant')
        ->where('s_id',$id)
        ->get();
        return $purchase;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all(),$id);
        $s_status='';
        $u_id = Auth::user()->id;
        if($request->p_status != 'Pending')
        {
            $s_status = 'Approved';
        }
        if($request->p_status == 'Pending')
        {
            $s_status = 'Pending';
        }
        if($request->doc == null)
        {
            $doc = $request->doc1;
        }
        else
        {
            $doc= Storage::disk('uploads')->putFile('',$request->doc);
        }
        $p_status = 'Pending';
        Sales::where('id',$id)
        ->update([
            'ref_no' => $request->ref_no,
            'sale_date' => $request->sale_date,
            'b_id' => $request->b_id,
            'c_id' => $request->c_id,
            'w_id' => $request->w_id,
            's_address' => $request->s_address,
            's_status' => $s_status,
            'p_status' => $p_status,
            'doc' => $doc,
            'tax' => $request->tax,
            'total' => $request->final,
            'note' => $request->editor1,
            'updated_by' => $u_id,
            'sp_id' => $request->sp_id,
            'discount_type' => $request->discount_type,
            'expected_date' => $request->expected_date,
            'advance' => $request->advance,
            'tax_status' => $request->tax_status,
            'pay_type' => $request->pay_type,
        ]);
        DB::table('sale_details')->where('s_id', $id)->delete();
        for ($i=0; $i <count($request->p_id) ; $i++) {
            SaleDetails::create([
                's_id' => $id,
                'p_id' => $request->p_id[$i],
                'quantity' => $request->quantity[$i],
                'sub_total' => $request->sub_total[$i],
                'price' => $request->price[$i],
                'discount_percent' => $request->discount_percent[$i],
                'discounted_amount' => $request->discounted_amount[$i],
                'type' => $request->type[$i],
            ]);
        }

        if($p_status != 'Pending')
        {
            if($request->p_total1 ==  '' && $request->p_total != null)
            {
                $total = $request->p_total;
            }
            else
            {
                $total = $request->p_total1;
            }
            TransactionHistory::create([
                'p_s_id' =>  $p->id,
                'p_type' => 'Sales',
                't_type' => 'Received',
                'paid_by' => $request->paid_by,
                // 'ref_no' => $request->p_ref_no,
                'total' => $total,
                'cheque_no' => $request->cheque_no,
                'cc_no' => $request->cc_no,
                'gift_no' => $request->gift_no,
                'cc_holder' => $request->cc_holder,
                'note' => $request->editor2,
                'b_id' => $request->bank_id,
                'created_by' => $u_id
            ]);
        }

        for ($j=0; $j < count($request->price) ; $j++) {
            Products::where('id',$request->p_id[$j])
            ->update([
                'price' => $request->price[$j],
                'updated_by' => $u_id
            ]);
        }

        toastr()->success('Sales Updated successfully!');
        // Session::flash('download', '/sales/pdf');
        return redirect(url('').'/sales');
    }

    public function invoice($id)
    {
        $credit = 0;
        $debit = 0;
        $th=TransactionHistory::find($id);
        $sales=Sales::with(['warehouse','customer','biller','saleperson'])
        ->where('id',$th->p_s_id)
        ->first();
        $saledetail=SaleDetails::with(['products','products.brands','products.unit','sale','variant','variant.product.brands','variant.product.unit'])
        ->where('s_id',$th->p_s_id)
        ->get();
        $account=AccountDetails::with('generalLedger')->where('name_of_account','Like','%'.$sales->customer->name)
        ->where('Code','like','CA-01%')
        ->first();
        if($account!=null)
        {
            for($k = 0 ; $k < count($account->generalLedger) ; $k++)
            {
                $credit += $account->generalLedger[$k]->credit;

                $debit += $account->generalLedger[$k]->debit;
            }
            $balance =  $debit - $credit;
        }
        else {
            $balance = 0;
        }
        $pdf = PDF::loadView('sales.invoice', compact('sales','saledetail','th','balance'));
        // return view('porder.invoice', compact('purchase','pdetail','stock'));

        return $pdf->download('paymentInvoice.pdf');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function excel()
    {
        return Excel::download(new SalesExport, 'Sales.xlsx');
    }

    public function saleYExcel($year,$c,$sp,$ss,$ps,$check)
    {
        return Excel::download(new SalesYearlyExport($year,$c,$sp,$ss,$ps,$check), 'SalesYearlyExport.xlsx');
    }
    public function saleMExcel($month,$c,$sp,$ss,$ps,$check)
    {
        return Excel::download(new SalesMonthlyExport($month,$c,$sp,$ss,$ps,$check), 'SalesMonthlyExport.xlsx');
    }
    public function saleDExcel($date,$c,$sp,$ss,$ps,$check)
    {
        return Excel::download(new SalesDateExport($date,$c,$sp,$ss,$ps,$check), 'SalesDateExport.xlsx');
    }
    public function saleDatesExcel($from,$to,$c,$sp,$ss,$ps,$check)
    {
        return Excel::download(new SalesDateDifferenceExport($from,$to,$c,$sp,$ss,$ps,$check), 'SalesDateDifferenceExport.xlsx');
    }
    public function saleSSExcel($c,$sp,$ss,$check)
    {
        return Excel::download(new SalesStatusExport($c,$sp,$ss,$check), 'SalesStatusExport.xlsx');
    }
    public function salePSExcel($c,$sp,$ps,$check)
    {
        return Excel::download(new SalesPaymentStatusExport($c,$sp,$ps,$check), 'SalesPaymentStatusExport.xlsx');
    }
    public function saleCExcel($c)
    {
        return Excel::download(new SalesCustomerExport($c), 'SalesCustomerExport.xlsx');
    }
    public function saleSPExcel($sp)
    {
        return Excel::download(new SalesSalePersonExport($sp), 'SalesSalePersonExport.xlsx');
    }

    public function reportExcel()
    {
        return Excel::download(new SalesReportExport, 'SalesReportExport.xlsx');
    }

    public function reportExcelYExcel($year,$c,$sp,$p,$types,$w,$check)
    {
        return Excel::download(new SalesReportYearlyExport($year,$c,$sp,$p,$types,$w,$check), 'SalesReportYearlyExport.xlsx');
    }
    public function reportExcelMExcel($month,$c,$sp,$p,$types,$w,$check)
    {
        return Excel::download(new SalesReportMonthlyExport($month,$c,$sp,$p,$types,$w,$check), 'SalesReportMonthlyExport.xlsx');
    }
    public function reportExcelDExcel($date,$c,$sp,$p,$types,$w,$check)
    {
        return Excel::download(new SalesReportDateExport($date,$c,$sp,$p,$types,$w,$check), 'SalesReportDateExport.xlsx');
    }
    public function reportExcelDatesExcel($from,$to,$c,$sp,$p,$types,$w,$check)
    {
        return Excel::download(new SalesReportDateDifferenceExport($from,$to,$c,$sp,$p,$types,$w,$check), 'SalesReportDateDifferenceExport.xlsx');
    }
    public function reportCExcel($c)
    {
        return Excel::download(new SalesReportCustomerExport($c), 'SalesReportCustomerExport.xlsx');
    }
    public function reportPExcel($p,$types)
    {
        return Excel::download(new SalesReportProductExport($p,$types), 'SalesReportProductExport.xlsx');
    }
    public function reportWExcel($w)
    {
        return Excel::download(new SalesReportWarehouseExport($w), 'SalesReportWarehouseExport.xlsx');
    }
    public function reportSPExcel($sp)
    {
        return Excel::download(new SalesReportSalpersonExport($sp), 'SalesReportSalpersonExport.xlsx');
    }

    public function search(Request $request)
    {
        $menu_id            =   $request->menuid;
        $permissions        =   getRolePermission($menu_id);
        $customer=Vendors::where('v_type','Customer')->where('status',1)->get();
        $bank=Bank::where('status',1)->get();
        $saleperson=Vendors::where('v_type','Saleperson')->where('status',1)->get();
        if($request->optradio == 'Year')
        {
            $year = $request->year;
            $index = 1;
            if($request->status == null && $request->c_id == null && $request->sp_id == null && $request->p_status == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y'))"),$request->year)
                ->orderBy('sale_date','desc')
                ->get();
                $c = 0;
                $sp = 0;
                $ss = 0;
                $ps = 0;
                $check = 0;
            }
            if($request->status != null && $request->c_id == null && $request->sp_id == null && $request->p_status == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y'))"),$request->year)
                ->where('s_status',$request->status)
                ->orderBy('sale_date','desc')
                ->get();
                $c = 0;
                $sp = 0;
                $ss = $request->status;
                $ps = 0;
                $check = 1;
            }
            if($request->status == null && $request->c_id == null && $request->sp_id == null && $request->p_status != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y'))"),$request->year)
                ->where('p_status',$request->p_status)
                ->orderBy('sale_date','desc')
                ->get();
                $c = 0;
                $sp = 0;
                $ss = 0;
                $ps = $request->p_status;
                $check = 2;
            }
            if($request->c_id != null && $request->sp_id == null && $request->status == null && $request->p_status == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y'))"),$request->year)
                ->where('c_id',$request->c_id)
                ->orderBy('sale_date','desc')
                ->get();
                $c = $request->c_id;
                $sp = 0;
                $ss = 0;
                $ps = 0;
                $check = 3;
            }

            if($request->status == null && $request->c_id == null && $request->sp_id != null && $request->p_status == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y'))"),$request->year)
                ->where('sp_id',$request->sp_id)
                ->orderBy('sale_date','desc')
                ->get();
                $c = 0;
                $sp = $request->sp_id;
                $ss = 0;
                $ps = 0;
                $check = 4;
            }

            if($request->status != null && $request->c_id != null  && $request->sp_id == null && $request->p_status == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y'))"),$request->year)
                ->where('s_status',$request->status)
                ->where('c_id',$request->c_id)
                ->orderBy('sale_date','desc')
                ->get();
                $c = $request->c_id;
                $sp = 0;
                $ss = $request->status;
                $ps = 0;
                $check = 5;
            }

            if($request->status == null && $request->c_id != null  && $request->sp_id != null && $request->p_status == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y'))"),$request->year)
                ->where('sp_id',$request->sp_id)
                ->where('c_id',$request->c_id)
                ->orderBy('sale_date','desc')
                ->get();
                $c = $request->c_id;
                $sp = $request->sp_id;
                $ss = 0;
                $ps = 0;
                $check = 6;
            }

            if($request->status == null && $request->c_id != null  && $request->sp_id == null && $request->p_status != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y'))"),$request->year)
                ->where('p_status',$request->p_status)
                ->where('c_id',$request->c_id)
                ->orderBy('sale_date','desc')
                ->get();
                $c = $request->c_id;
                $sp = 0;
                $ss = 0;
                $ps = $request->p_status;
                $check = 7;
            }

            if($request->status == null && $request->c_id == null  && $request->sp_id != null && $request->p_status != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y'))"),$request->year)
                ->where('p_status',$request->p_status)
                ->where('sp_id',$request->sp_id)
                ->orderBy('sale_date','desc')
                ->get();
                $c = 0;
                $sp = $request->sp_id;
                $ss = 0;
                $ps = $request->p_status;
                $check = 8;
            }

            if($request->status != null && $request->c_id == null  && $request->sp_id != null && $request->p_status == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y'))"),$request->year)
                ->where('s_status',$request->status)
                ->where('sp_id',$request->sp_id)
                ->orderBy('sale_date','desc')
                ->get();
                $c = 0;
                $sp = $request->sp_id;
                $ss = $request->s_status;
                $ps = 0;
                $check = 9;
            }

            if($request->status != null && $request->c_id == null  && $request->sp_id == null && $request->p_status != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y'))"),$request->year)
                ->where('s_status',$request->status)
                ->where('p_status',$request->p_status)
                ->orderBy('sale_date','desc')
                ->get();
                $c = 0;
                $sp = 0;
                $ss = $request->status;
                $ps = $request->p_status;
                $check = 10;
            }

            if($request->status != null && $request->c_id != null  && $request->sp_id != null && $request->p_status == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y'))"),$request->year)
                ->where('s_status',$request->status)
                ->where('c_id',$request->c_id)
                ->where('sp_id',$request->sp_id)
                ->orderBy('sale_date','desc')
                ->get();
                $c = $request->c_id;
                $sp = $request->sp_id;
                $ss = $request->status;
                $ps = 0;
                $check = 11;
            }

            if($request->status == null && $request->c_id != null  && $request->sp_id != null && $request->p_status != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y'))"),$request->year)
                ->where('p_status',$request->p_status)
                ->where('c_id',$request->c_id)
                ->where('sp_id',$request->sp_id)
                ->orderBy('sale_date','desc')
                ->get();
                $c = $request->c_id;
                $sp = $request->sp_id;
                $ss = 0;
                $ps = $request->p_status;
                $check = 12;
            }

            if($request->status != null && $request->c_id != null  && $request->sp_id == null && $request->p_status != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y'))"),$request->year)
                ->where('p_status',$request->p_status)
                ->where('c_id',$request->c_id)
                ->where('s_status',$request->status)
                ->orderBy('sale_date','desc')
                ->get();
                $c = $request->c_id;
                $sp = 0;
                $ss = $request->status;
                $ps = $request->p_status;
                $check = 13;
            }

            if($request->status != null && $request->c_id == null  && $request->sp_id != null && $request->p_status != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y'))"),$request->year)
                ->where('p_status',$request->p_status)
                ->where('sp_id',$request->sp_id)
                ->where('s_status',$request->status)
                ->orderBy('sale_date','desc')
                ->get();
                $sp = $request->sp_id;
                $c = 0;
                $ss = $request->status;
                $ps = $request->p_status;
                $check = 14;
            }

            if($request->status != null && $request->c_id != null  && $request->sp_id != null && $request->p_status != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y'))"),$request->year)
                ->where('p_status',$request->p_status)
                ->where('sp_id',$request->sp_id)
                ->where('c_id',$request->c_id)
                ->where('s_status',$request->status)
                ->orderBy('sale_date','desc')
                ->get();
                $sp = $request->sp_id;
                $c = $request->c_id;
                $ss = $request->status;
                $ps = $request->p_status;
                $check = 15;
            }
            return view('sales.index',compact('customer','permissions','bank','menu_id','sales','year','c','sp','ss','ps','check','index','saleperson'));

        }
        else if($request->optradio == 'Month')
        {
            $month = $request->month;
            $index = 2;
            if($request->status == null && $request->c_id == null && $request->sp_id == null && $request->p_status == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$request->month)
                ->orderBy('sale_date','desc')
                ->get();
                $c = 0;
                $sp = 0;
                $ss = 0;
                $ps = 0;
                $check = 0;
            }
            if($request->status != null && $request->c_id == null && $request->sp_id == null && $request->p_status == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$request->month)
                ->where('s_status',$request->status)
                ->orderBy('sale_date','desc')
                ->get();
                $c = 0;
                $sp = 0;
                $ss = $request->status;
                $ps = 0;
                $check = 1;
            }
            if($request->status == null && $request->c_id == null && $request->sp_id == null && $request->p_status != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$request->month)
                ->where('p_status',$request->p_status)
                ->orderBy('sale_date','desc')
                ->get();
                $c = 0;
                $sp = $request->p_status;
                $ss = 0;
                $ps = 0;
                $check = 2;
            }
            if($request->c_id != null && $request->sp_id == null && $request->status == null && $request->p_status == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$request->month)
                ->where('c_id',$request->c_id)
                ->orderBy('sale_date','desc')
                ->get();
                $c = $request->c_id;
                $sp = 0;
                $ss = 0;
                $ps = 0;
                $check = 3;
            }

            if($request->status == null && $request->c_id == null && $request->sp_id != null && $request->p_status == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$request->month)
                ->where('sp_id',$request->sp_id)
                ->orderBy('sale_date','desc')
                ->get();
                $c = 0;
                $sp = $request->sp_id;
                $ss = 0;
                $ps = 0;
                $check = 4;
            }

            if($request->status != null && $request->c_id != null  && $request->sp_id == null && $request->p_status == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$request->month)
                ->where('s_status',$request->status)
                ->where('c_id',$request->c_id)
                ->orderBy('sale_date','desc')
                ->get();
                $c = $request->c_id;
                $sp = 0;
                $ss = $request->status;
                $ps = 0;
                $check = 5;
            }

            if($request->status == null && $request->c_id != null  && $request->sp_id != null && $request->p_status == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$request->month)
                ->where('sp_id',$request->sp_id)
                ->where('c_id',$request->c_id)
                ->orderBy('sale_date','desc')
                ->get();
                $c = $request->c_id;
                $sp = $request->sp_id;
                $ss = 0;
                $ps = 0;
                $check = 6;
            }

            if($request->status == null && $request->c_id != null  && $request->sp_id == null && $request->p_status != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$request->month)
                ->where('p_status',$request->p_status)
                ->where('c_id',$request->c_id)
                ->orderBy('sale_date','desc')
                ->get();
                $c = $request->c_id;
                $sp = 0;
                $ss = 0;
                $ps = $request->p_status;
                $check = 7;
            }

            if($request->status == null && $request->c_id == null  && $request->sp_id != null && $request->p_status != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$request->month)
                ->where('p_status',$request->p_status)
                ->where('sp_id',$request->sp_id)
                ->orderBy('sale_date','desc')
                ->get();
                $c = 0;
                $sp = $request->sp_id;
                $ss = 0;
                $ps = $request->p_status;
                $check = 8;
            }

            if($request->status != null && $request->c_id == null  && $request->sp_id != null && $request->p_status == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$request->month)
                ->where('s_status',$request->status)
                ->where('sp_id',$request->sp_id)
                ->orderBy('sale_date','desc')
                ->get();
                $c = 0;
                $sp = $request->sp_id;
                $ss = $request->s_status;
                $ps = 0;
                $check = 9;
            }

            if($request->status != null && $request->c_id == null  && $request->sp_id == null && $request->p_status != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$request->month)
                ->where('s_status',$request->status)
                ->where('p_status',$request->p_status)
                ->orderBy('sale_date','desc')
                ->get();
                $c = 0;
                $sp = 0;
                $ss = $request->status;
                $ps = $request->p_status;
                $check = 10;
            }

            if($request->status != null && $request->c_id != null  && $request->sp_id != null && $request->p_status == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$request->month)
                ->where('s_status',$request->status)
                ->where('c_id',$request->c_id)
                ->where('sp_id',$request->sp_id)
                ->orderBy('sale_date','desc')
                ->get();
                $c = $request->c_id;
                $sp = $request->sp_id;
                $ss = $request->status;
                $ps = 0;
                $check = 11;
            }

            if($request->status == null && $request->c_id != null  && $request->sp_id != null && $request->p_status != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$request->month)
                ->where('p_status',$request->p_status)
                ->where('c_id',$request->c_id)
                ->where('sp_id',$request->sp_id)
                ->orderBy('sale_date','desc')
                ->get();
                $c = $request->c_id;
                $sp = $request->sp_id;
                $ss = 0;
                $ps = $request->p_status;
                $check = 12;
            }

            if($request->status != null && $request->c_id != null  && $request->sp_id == null && $request->p_status != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$request->month)
                ->where('p_status',$request->p_status)
                ->where('c_id',$request->c_id)
                ->where('s_status',$request->status)
                ->orderBy('sale_date','desc')
                ->get();
                $c = $request->c_id;
                $sp = 0;
                $ss = $request->status;
                $ps = $request->p_status;
                $check = 13;
            }

            if($request->status != null && $request->c_id == null  && $request->sp_id != null && $request->p_status != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$request->month)
                ->where('p_status',$request->p_status)
                ->where('sp_id',$request->sp_id)
                ->where('s_status',$request->status)
                ->orderBy('sale_date','desc')
                ->get();
                $sp = $request->sp_id;
                $c = 0;
                $ss = $request->status;
                $ps = $request->p_status;
                $check = 14;
            }

            if($request->status != null && $request->c_id != null  && $request->sp_id != null && $request->p_status != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$request->month)
                ->where('p_status',$request->p_status)
                ->where('sp_id',$request->sp_id)
                ->where('c_id',$request->c_id)
                ->where('s_status',$request->status)
                ->orderBy('sale_date','desc')
                ->get();
                $sp = $request->sp_id;
                $c = $request->c_id;
                $ss = $request->status;
                $ps = $request->p_status;
                $check = 15;
            }

            return view('sales.index',compact('customer','permissions','bank','menu_id','sales','month','c','sp','ss','ps','check','index','saleperson'));
        }
        else if($request->optradio == 'Date')
        {
            if($request->from != null && $request->to != null)
            {
                $from = $request->from;
                $to = $request->to;
                $index = 4;
                if($request->status == null && $request->c_id == null && $request->sp_id == null && $request->p_status == null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = 0;
                    $sp = 0;
                    $ss = 0;
                    $ps = 0;
                    $check = 0;
                }
                if($request->status != null && $request->c_id == null && $request->sp_id == null && $request->p_status == null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('s_status',$request->status)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = 0;
                    $sp = 0;
                    $ss = $request->status;
                    $ps = 0;
                    $check = 1;
                }
                if($request->status == null && $request->c_id == null && $request->sp_id == null && $request->p_status != null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_status',$request->p_status)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = 0;
                    $sp = $request->p_status;
                    $ss = 0;
                    $ps = 0;
                    $check = 2;
                }
                if($request->c_id != null && $request->sp_id == null && $request->status == null && $request->p_status == null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('c_id',$request->c_id)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = $request->c_id;
                    $sp = 0;
                    $ss = 0;
                    $ps = 0;
                    $check = 3;
                }

                if($request->status == null && $request->c_id == null && $request->sp_id != null && $request->p_status == null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('sp_id',$request->sp_id)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = 0;
                    $sp = $request->sp_id;
                    $ss = 0;
                    $ps = 0;
                    $check = 4;
                }

                if($request->status != null && $request->c_id != null  && $request->sp_id == null && $request->p_status == null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('s_status',$request->status)
                    ->where('c_id',$request->c_id)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = $request->c_id;
                    $sp = 0;
                    $ss = $request->status;
                    $ps = 0;
                    $check = 5;
                }

                if($request->status == null && $request->c_id != null  && $request->sp_id != null && $request->p_status == null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('sp_id',$request->sp_id)
                    ->where('c_id',$request->c_id)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = $request->c_id;
                    $sp = $request->sp_id;
                    $ss = 0;
                    $ps = 0;
                    $check = 6;
                }

                if($request->status == null && $request->c_id != null  && $request->sp_id == null && $request->p_status != null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_status',$request->p_status)
                    ->where('c_id',$request->c_id)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = $request->c_id;
                    $sp = 0;
                    $ss = 0;
                    $ps = $request->p_status;
                    $check = 7;
                }

                if($request->status == null && $request->c_id == null  && $request->sp_id != null && $request->p_status != null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_status',$request->p_status)
                    ->where('sp_id',$request->sp_id)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = 0;
                    $sp = $request->sp_id;
                    $ss = 0;
                    $ps = $request->p_status;
                    $check = 8;
                }

                if($request->status != null && $request->c_id == null  && $request->sp_id != null && $request->p_status == null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('s_status',$request->status)
                    ->where('sp_id',$request->sp_id)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = 0;
                    $sp = $request->sp_id;
                    $ss = $request->s_status;
                    $ps = 0;
                    $check = 9;
                }

                if($request->status != null && $request->c_id == null  && $request->sp_id == null && $request->p_status != null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('s_status',$request->status)
                    ->where('p_status',$request->p_status)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = 0;
                    $sp = 0;
                    $ss = $request->status;
                    $ps = $request->p_status;
                    $check = 10;
                }

                if($request->status != null && $request->c_id != null  && $request->sp_id != null && $request->p_status == null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('s_status',$request->status)
                    ->where('c_id',$request->c_id)
                    ->where('sp_id',$request->sp_id)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = $request->c_id;
                    $sp = $request->sp_id;
                    $ss = $request->status;
                    $ps = 0;
                    $check = 11;
                }

                if($request->status == null && $request->c_id != null  && $request->sp_id != null && $request->p_status != null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_status',$request->p_status)
                    ->where('c_id',$request->c_id)
                    ->where('sp_id',$request->sp_id)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = $request->c_id;
                    $sp = $request->sp_id;
                    $ss = 0;
                    $ps = $request->p_status;
                    $check = 12;
                }

                if($request->status != null && $request->c_id != null  && $request->sp_id == null && $request->p_status != null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_status',$request->p_status)
                    ->where('c_id',$request->c_id)
                    ->where('s_status',$request->status)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = $request->c_id;
                    $sp = 0;
                    $ss = $request->status;
                    $ps = $request->p_status;
                    $check = 13;
                }

                if($request->status != null && $request->c_id == null  && $request->sp_id != null && $request->p_status != null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_status',$request->p_status)
                    ->where('sp_id',$request->sp_id)
                    ->where('s_status',$request->status)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $sp = $request->sp_id;
                    $c = 0;
                    $ss = $request->status;
                    $ps = $request->p_status;
                    $check = 14;
                }

                if($request->status != null && $request->c_id != null  && $request->sp_id != null && $request->p_status != null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                    ->whereBetween(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_status',$request->p_status)
                    ->where('sp_id',$request->sp_id)
                    ->where('c_id',$request->c_id)
                    ->where('s_status',$request->status)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $sp = $request->sp_id;
                    $c = $request->c_id;
                    $ss = $request->status;
                    $ps = $request->p_status;
                    $check = 15;
                }

                return view('sales.index',compact('customer','check','permissions','bank','menu_id','sales','from','to','c','sp','ss','ps','index','saleperson'));
            }
            else
            {
                if($request->to == null)
                {
                    $date = $request->from;
                }
                if($request->from == null)
                {
                    $date = $request->to;
                }
                $index = 3;
                if($request->status == null && $request->c_id == null && $request->sp_id == null && $request->p_status == null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                     ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),$date)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = 0;
                    $sp = 0;
                    $ss = 0;
                    $ps = 0;
                    $check = 0;
                }
                if($request->status != null && $request->c_id == null && $request->sp_id == null && $request->p_status == null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                     ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),$date)
                    ->where('s_status',$request->status)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = 0;
                    $sp = 0;
                    $ss = $request->status;
                    $ps = 0;
                    $check = 1;
                }
                if($request->status == null && $request->c_id == null && $request->sp_id == null && $request->p_status != null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                     ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),$date)
                    ->where('p_status',$request->p_status)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = 0;
                    $sp = $request->p_status;
                    $ss = 0;
                    $ps = 0;
                    $check = 2;
                }
                if($request->c_id != null && $request->sp_id == null && $request->status == null && $request->p_status == null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                     ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),$date)
                    ->where('c_id',$request->c_id)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = $request->c_id;
                    $sp = 0;
                    $ss = 0;
                    $ps = 0;
                    $check = 3;
                }

                if($request->status == null && $request->c_id == null && $request->sp_id != null && $request->p_status == null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                     ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),$date)
                    ->where('sp_id',$request->sp_id)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = 0;
                    $sp = $request->sp_id;
                    $ss = 0;
                    $ps = 0;
                    $check = 4;
                }

                if($request->status != null && $request->c_id != null  && $request->sp_id == null && $request->p_status == null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                     ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),$date)
                    ->where('s_status',$request->status)
                    ->where('c_id',$request->c_id)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = $request->c_id;
                    $sp = 0;
                    $ss = $request->status;
                    $ps = 0;
                    $check = 5;
                }

                if($request->status == null && $request->c_id != null  && $request->sp_id != null && $request->p_status == null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                     ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),$date)
                    ->where('sp_id',$request->sp_id)
                    ->where('c_id',$request->c_id)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = $request->c_id;
                    $sp = $request->sp_id;
                    $ss = 0;
                    $ps = 0;
                    $check = 6;
                }

                if($request->status == null && $request->c_id != null  && $request->sp_id == null && $request->p_status != null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                     ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),$date)
                    ->where('p_status',$request->p_status)
                    ->where('c_id',$request->c_id)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = $request->c_id;
                    $sp = 0;
                    $ss = 0;
                    $ps = $request->p_status;
                    $check = 7;
                }

                if($request->status == null && $request->c_id == null  && $request->sp_id != null && $request->p_status != null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                     ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),$date)
                    ->where('p_status',$request->p_status)
                    ->where('sp_id',$request->sp_id)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = 0;
                    $sp = $request->sp_id;
                    $ss = 0;
                    $ps = $request->p_status;
                    $check = 8;
                }

                if($request->status != null && $request->c_id == null  && $request->sp_id != null && $request->p_status == null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                     ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),$date)
                    ->where('s_status',$request->status)
                    ->where('sp_id',$request->sp_id)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = 0;
                    $sp = $request->sp_id;
                    $ss = $request->s_status;
                    $ps = 0;
                    $check = 9;
                }

                if($request->status != null && $request->c_id == null  && $request->sp_id == null && $request->p_status != null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                     ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),$date)
                    ->where('s_status',$request->status)
                    ->where('p_status',$request->p_status)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = 0;
                    $sp = 0;
                    $ss = $request->status;
                    $ps = $request->p_status;
                    $check = 10;
                }

                if($request->status != null && $request->c_id != null  && $request->sp_id != null && $request->p_status == null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                     ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),$date)
                    ->where('s_status',$request->status)
                    ->where('c_id',$request->c_id)
                    ->where('sp_id',$request->sp_id)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = $request->c_id;
                    $sp = $request->sp_id;
                    $ss = $request->status;
                    $ps = 0;
                    $check = 11;
                }

                if($request->status == null && $request->c_id != null  && $request->sp_id != null && $request->p_status != null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                     ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),$date)
                    ->where('p_status',$request->p_status)
                    ->where('c_id',$request->c_id)
                    ->where('sp_id',$request->sp_id)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = $request->c_id;
                    $sp = $request->sp_id;
                    $ss = 0;
                    $ps = $request->p_status;
                    $check = 12;
                }

                if($request->status != null && $request->c_id != null  && $request->sp_id == null && $request->p_status != null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                     ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),$date)
                    ->where('p_status',$request->p_status)
                    ->where('c_id',$request->c_id)
                    ->where('s_status',$request->status)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $c = $request->c_id;
                    $sp = 0;
                    $ss = $request->status;
                    $ps = $request->p_status;
                    $check = 13;
                }

                if($request->status != null && $request->c_id == null  && $request->sp_id != null && $request->p_status != null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                     ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),$date)
                    ->where('p_status',$request->p_status)
                    ->where('sp_id',$request->sp_id)
                    ->where('s_status',$request->status)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $sp = $request->sp_id;
                    $c = 0;
                    $ss = $request->status;
                    $ps = $request->p_status;
                    $check = 14;
                }

                if($request->status != null && $request->c_id != null  && $request->sp_id != null && $request->p_status != null)
                {
                    $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                    ->withCount(['transaction as total_amount' => function($query) {
                        $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                    }])
                     ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m-%d'))"),$date)
                    ->where('p_status',$request->p_status)
                    ->where('sp_id',$request->sp_id)
                    ->where('c_id',$request->c_id)
                    ->where('s_status',$request->status)
                    ->orderBy('sale_date','desc')
                    ->get();
                    $sp = $request->sp_id;
                    $c = $request->c_id;
                    $ss = $request->status;
                    $ps = $request->p_status;
                    $check = 15;
                }

                return view('sales.index',compact('customer','check','permissions','bank','menu_id','sales','date','c','sp','ss','ps','index','saleperson'));
            }

        }
        else if($request->optradio == 'Status')
        {
            $index = 5;
            $ss = $request->status;
            if($request->c_id == null && $request->sp_id == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where('s_status',$request->status)
                ->orderBy('sale_date','desc')
                ->get();
                $c = 0;
                $sp = 0;
                $check = 0;
            }
            if($request->c_id != null && $request->sp_id == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where('c_id',$request->c_id)
                ->where('s_status',$request->status)
                ->orderBy('sale_date','desc')
                ->get();
                $c = $request->c_id;
                $sp = 0;
                $check = 1;
            }

            if($request->c_id == null && $request->sp_id != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where('sp_id',$request->sp_id)
                ->where('s_status',$request->status)
                ->orderBy('sale_date','desc')
                ->get();
                $c = 0;
                $sp = $request->sp_id;
                $check = 2;
            }

            if($request->c_id != null && $request->sp_id != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where('sp_id',$request->sp_id)
                ->where('c_id',$request->c_id)
                ->where('s_status',$request->status)
                ->orderBy('sale_date','desc')
                ->get();
                $c = $request->c_id;
                $sp = $request->sp_id;
                $check = 3;
            }
            return view('sales.index',compact('customer','check','permissions','bank','menu_id','sales','c','sp','ss','index','saleperson'));
        }

        else if($request->optradio == 'PStatus')
        {
            $index = 6;
            $ps = $request->p_status;
            if($request->c_id == null && $request->sp_id == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where('p_status',$request->p_status)
                ->orderBy('sale_date','desc')
                ->get();
                $c = 0;
                $sp = 0;
                $check = 0;
            }
            if($request->c_id != null && $request->sp_id == null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where('c_id',$request->c_id)
                ->where('p_status',$request->p_status)
                ->orderBy('sale_date','desc')
                ->get();
                $c = $request->c_id;
                $sp = 0;
                $check = 1;
            }

            if($request->c_id == null && $request->sp_id != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where('sp_id',$request->sp_id)
                ->where('p_status',$request->p_status)
                ->orderBy('sale_date','desc')
                ->get();
                $c = 0;
                $sp = $request->sp_id;
                $check = 2;
            }

            if($request->c_id != null && $request->sp_id != null)
            {
                $sales=Sales::with(['warehouse','customer','biller','saleperson'])
                ->withCount(['transaction as total_amount' => function($query) {
                    $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
                }])
                ->where('sp_id',$request->sp_id)
                ->where('c_id',$request->c_id)
                ->where('p_status',$request->p_status)
                ->orderBy('sale_date','desc')
                ->get();
                $c = $request->c_id;
                $sp = $request->sp_id;
                $check = 3;
            }
            return view('sales.index',compact('customer','check','permissions','bank','menu_id','sales','c','sp','ps','index','saleperson'));
        }

        else if($request->optradio == 'customer')
        {
            $index = 7;
            $c = $request->c_id;
            $sales=Sales::with(['warehouse','customer','biller','saleperson'])
            ->withCount(['transaction as total_amount' => function($query) {
                $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
            }])
            ->where('c_id',$request->c_id)
            ->orderBy('sale_date','desc')
            ->get();
            return view('sales.index',compact('customer','permissions','bank','menu_id','sales','c','index','saleperson'));
        }

        else if($request->optradio == 'saleperson')
        {
            $index = 8 ;
            $sp = $request->sp_id;
            $sales=Sales::with(['warehouse','customer','biller','saleperson'])
            ->withCount(['transaction as total_amount' => function($query) {
                $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
            }])
            ->where('sp_id',$request->sp_id)
            ->orderBy('sale_date','desc')
            ->get();
            return view('sales.index',compact('customer','permissions','bank','menu_id','sales','sp','index','saleperson'));
        }

    }

    public function reportSearch(Request $request)
    {
        // dd($request->all());
        $customer=Vendors::where('v_type','Customer')->get();
        $person=Vendors::where('v_type','Saleperson')->get();
        $ware=Warehouse::all();
        $product=Products::with(['brands','category','variants'])
        ->where('status',1)
        ->get();

        if($request->optradio == 'Year')
        {
            $year = $request->year;
            $index = 1;
            if($request->p_id == null && $request->c_id == null && $request->sp_id == null && $request->w_id == null)
            {
                $saledetail=SaleDetails::with(['sale','sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->get();
                $c = 0;
                $sp = 0;
                $w = 0;
                $p = 0;
                $types = 0;
                $check = 0;
            }
            if($request->p_id != null && $request->c_id == null && $request->sp_id == null && $request->w_id == null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $saledetail=SaleDetails::with(['sale','sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->get();
                $c = 0;
                $sp = 0;
                $p = $p_id;
                $types = $type;
                $w = 0;
                $check = 1;
            }
            if($request->p_id == null && $request->c_id == null && $request->sp_id == null && $request->w_id != null)
            {
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('w_id',$request->w_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->get();
                $c = 0;
                $sp = 0;
                $p = 0;
                $types = 0;
                $w =  $request->w_id;
                $check = 2;
            }
            if($request->c_id != null && $request->sp_id == null && $request->p_id == null && $request->w_id == null)
            {
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('c_id',$request->c_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->get();
                // dd($saledetail);
                $c = $request->c_id;
                $sp = 0;
                $p = 0;
                $types = 0;
                $w = 0;
                $check = 3;
            }

            if($request->p_id == null && $request->c_id == null && $request->sp_id != null && $request->w_id == null)
            {
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('sp_id',$request->sp_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->get();
                $c = 0;
                $sp = $request->sp_id;
                $p = 0;
                $w = 0;
                $types = 0;
                $check = 4;
            }

            if($request->p_id != null && $request->c_id != null  && $request->sp_id == null && $request->w_id == null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('c_id',$request->c_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->get();
                $c = $request->c_id;
                $sp = 0;
                $p = $p_id;
                $types = $type;
                $w = 0;
                $check = 5;
            }

            if($request->p_id == null && $request->c_id != null  && $request->sp_id != null && $request->w_id == null)
            {
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('c_id',$request->c_id)->where('sp_id',$request->sp_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->get();
                $c = $request->c_id;
                $sp = $request->sp_id;
                $p = 0;
                $types = 0;
                $w = 0;
                $check = 6;
            }

            if($request->p_id == null && $request->c_id != null  && $request->sp_id == null && $request->w_id != null)
            {
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('c_id',$request->c_id)->where('w_id',$request->w_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->get();
                $c = $request->c_id;
                $sp = 0;
                $p = 0;
                $types = 0;
                $w = $request->w_id;
                $check = 7;
            }

            if($request->p_id == null && $request->c_id == null  && $request->sp_id != null && $request->w_id != null)
            {
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('sp_id',$request->sp_id)->where('w_id',$request->w_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->get();
                $c = 0;
                $types = 0;
                $sp = $request->sp_id;
                $p = 0;
                $w = $request->w_id;
                $check = 8;
            }

            if($request->p_id != null && $request->c_id == null  && $request->sp_id != null && $request->w_id == null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('sp_id',$request->sp_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->get();
                $c = 0;
                $sp = $request->sp_id;
                $p = $p_id;
                $types = $type;
                $w = 0;
                $check = 9;
            }

            if($request->p_id != null && $request->c_id == null  && $request->sp_id == null && $request->w_id != null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('w_id',$request->w_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->get();
                $c = 0;
                $sp = 0;
                $p = $p_id;
                $types = $type;
                $w = $request->w_id;
                $check = 10;
            }

            if($request->p_id != null && $request->c_id != null  && $request->sp_id != null && $request->w_id == null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('c_id',$request->c_id)->where('sp_id',$request->sp_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->get();
                $c = $request->c_id;
                $sp = $request->sp_id;
                $p = $p_id;
                $types = $type;
                $w = 0;
                $check = 11;
            }

            if($request->p_id == null && $request->c_id != null  && $request->sp_id != null && $request->w_id != null)
            {
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('c_id',$request->c_id)->where('sp_id',$request->sp_id)
                    ->where('w_id',$request->w_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->get();
                $c = $request->c_id;
                $sp = $request->sp_id;
                $p = 0;
                $types = 0;
                $w = $request->w_id;
                $check = 12;
            }

            if($request->p_id != null && $request->c_id != null  && $request->sp_id == null && $request->w_id != null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('c_id',$request->c_id)->where('w_id',$request->w_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->get();
                $c = $request->c_id;
                $sp = 0;
                $p = $p_id;
                $types = $type;
                $w = $request->w_id;
                $check = 13;
            }

            if($request->p_id != null && $request->c_id == null  && $request->sp_id != null && $request->w_id != null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('sp_id',$request->sp_id)->where('w_id',$request->w_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->get();
                $sp = $request->sp_id;
                $c = 0;
                $p = $p_id;
                $types = $type;
                $w = $request->w_id;
                $check = 14;
            }

            if($request->p_id != null && $request->c_id != null  && $request->sp_id != null && $request->w_id != null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('c_id',$request->c_id)->where('sp_id',$request->sp_id)
                    ->where('w_id',$request->w_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->get();
                $sp = $request->sp_id;
                $c = $request->c_id;
                $p = $p_id;
                $types = $type;
                $w = $request->w_id;
                $check = 15;
            }
            return view('sales.report',compact('types','ware','product','customer','saledetail','year','c','sp','p','w','check','index','person'));

        }

        else if($request->optradio == 'Month')
        {
            $month = $request->month;
            $index = 2;
            if($request->p_id == null && $request->c_id == null && $request->sp_id == null && $request->w_id == null)
            {
                $saledetail=SaleDetails::with(['sale','sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->get();
                $c = 0;
                $sp = 0;
                $w = 0;
                $p = 0;
                $types = 0;
                $check = 0;
            }
            if($request->p_id != null && $request->c_id == null && $request->sp_id == null && $request->w_id == null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $saledetail=SaleDetails::with(['sale','sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->get();
                $c = 0;
                $sp = 0;
                $p = $p_id;
                $types = $type;
                $w = 0;
                $check = 1;
            }
            if($request->p_id == null && $request->c_id == null && $request->sp_id == null && $request->w_id != null)
            {
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('w_id',$request->w_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->get();
                $c = 0;
                $sp = 0;
                $p = 0;
                $types = 0;
                $w =  $request->w_id;
                $check = 2;
            }
            if($request->c_id != null && $request->sp_id == null && $request->p_id == null && $request->w_id == null)
            {
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('c_id',$request->c_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->get();
                $c = $request->c_id;
                $sp = 0;
                $p = 0;
                $types = 0;
                $w = 0;
                $check = 3;
            }

            if($request->p_id == null && $request->c_id == null && $request->sp_id != null && $request->w_id == null)
            {
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('sp_id',$request->sp_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->get();
                $c = 0;
                $sp = $request->sp_id;
                $types = 0;
                $p = 0;
                $w = 0;
                $check = 4;
            }

            if($request->p_id != null && $request->c_id != null  && $request->sp_id == null && $request->w_id == null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('c_id',$request->c_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->get();
                $c = $request->c_id;
                $sp = 0;
                $p = $p_id;
                $types = $type;
                $w = 0;
                $check = 5;
            }

            if($request->p_id == null && $request->c_id != null  && $request->sp_id != null && $request->w_id == null)
            {
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('c_id',$request->c_id)->where('sp_id',$request->sp_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->get();
                $c = $request->c_id;
                $sp = $request->sp_id;
                $p = 0;
                $w = 0;
                $types = 0;
                $check = 6;
            }

            if($request->p_id == null && $request->c_id != null  && $request->sp_id == null && $request->w_id != null)
            {
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('c_id',$request->c_id)->where('w_id',$request->w_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->get();
                $c = $request->c_id;
                $sp = 0;
                $types = 0;
                $p = 0;
                $w = $request->w_id;
                $check = 7;
            }

            if($request->p_id == null && $request->c_id == null  && $request->sp_id != null && $request->w_id != null)
            {
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('sp_id',$request->csp_id)->where('w_id',$request->w_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->get();
                $c = 0;
                $types = 0;
                $sp = $request->sp_id;
                $p = 0;
                $w = $request->w_id;
                $check = 8;
            }

            if($request->p_id != null && $request->c_id == null  && $request->sp_id != null && $request->w_id == null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('sp_id',$request->sp_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->get();
                $c = 0;
                $sp = $request->sp_id;
                $p = $p_id;
                $types = $type;
                $w = 0;
                $check = 9;
            }

            if($request->p_id != null && $request->c_id == null  && $request->sp_id == null && $request->w_id != null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('w_id',$request->w_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->get();
                $c = 0;
                $sp = 0;
                $p = $p_id;
                $types = $type;
                $w = $request->w_id;
                $check = 10;
            }

            if($request->p_id != null && $request->c_id != null  && $request->sp_id != null && $request->w_id == null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('c_id',$request->c_id)->where('sp_id',$request->sp_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->get();
                $c = $request->c_id;
                $sp = $request->sp_id;
                $p = $p_id;
                $types = $type;
                $w = 0;
                $check = 11;
            }

            if($request->p_id == null && $request->c_id != null  && $request->sp_id != null && $request->w_id != null)
            {
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('c_id',$request->c_id)->where('sp_id',$request->sp_id)
                    ->where('w_id',$request->w_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->get();
                $c = $request->c_id;
                $sp = $request->sp_id;
                $p = 0;
                $types = 0;
                $w = $request->w_id;
                $check = 12;
            }

            if($request->p_id != null && $request->c_id != null  && $request->sp_id == null && $request->w_id != null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('c_id',$request->c_id)->where('w_id',$request->w_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->get();
                $c = $request->c_id;
                $sp = 0;
                $p = $p_id;
                $types = $type;
                $w = $request->w_id;
                $check = 13;
            }

            if($request->p_id != null && $request->c_id == null  && $request->sp_id != null && $request->w_id != null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('sp_id',$request->sp_id)->where('w_id',$request->w_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->get();
                $sp = $request->sp_id;
                $c = 0;
                $p = $p_id;
                $types = $type;
                $w = $request->w_id;
                $check = 14;
            }

            if($request->p_id != null && $request->c_id != null  && $request->sp_id != null && $request->w_id != null)
            {
                $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                    $item->where('c_id',$request->c_id)->where('sp_id',$request->sp_id)
                    ->where('w_id',$request->w_id)->get();
                },
                'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->where('p_id',$p_id)
                ->where('type',$type)
                ->get();
                $sp = $request->sp_id;
                $c = $request->c_id;
                $p = $p_id;
                $types = $type;
                $w = $request->w_id;
                $check = 15;
            }

            return view('sales.report',compact('types','ware','product','customer','saledetail','month','c','sp','p','w','check','index','person'));

        }

        else if($request->optradio == 'Date')
        {
            // dd($request->all());
            if($request->from != null && $request->to != null)
            {
                $from = $request->from;
                $to = $request->to;
                $index = 4;
                if($request->p_id == null && $request->c_id == null && $request->sp_id == null && $request->w_id == null)
                {
                    $saledetail=SaleDetails::with(['sale','sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->get();
                    $c = 0;
                    $sp = 0;
                    $w = 0;
                    $p = 0;
                    $check = 0;
                }
                if($request->p_id != null && $request->c_id == null && $request->sp_id == null && $request->w_id == null)
                {
                    $saledetail=SaleDetails::with(['sale','sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_id',$request->p_id)
                    ->get();
                    $c = 0;
                    $sp = 0;
                    $p = $request->p_id;
                    $w = 0;
                    $check = 1;
                }
                if($request->p_id == null && $request->c_id == null && $request->sp_id == null && $request->w_id != null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('w_id',$request->w_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->get();
                    $c = 0;
                    $sp = 0;
                    $p = 0;
                    $w =  $request->w_id;
                    $check = 2;
                }
                if($request->c_id != null && $request->sp_id == null && $request->p_id == null && $request->w_id == null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('c_id',$request->c_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->get();
                    $c = $request->c_id;
                    $sp = 0;
                    $p = 0;
                    $w = 0;
                    $check = 3;
                }

                if($request->p_id == null && $request->c_id == null && $request->sp_id != null && $request->w_id == null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('sp_id',$request->sp_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->get();
                    $c = 0;
                    $sp = $request->sp_id;
                    $p = 0;
                    $w = 0;
                    $check = 4;
                }

                if($request->p_id != null && $request->c_id != null  && $request->sp_id == null && $request->w_id == null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('c_id',$request->c_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_id',$request->p_id)
                    ->get();
                    $c = $request->c_id;
                    $sp = 0;
                    $p = $request->p_id;
                    $w = 0;
                    $check = 5;
                }

                if($request->p_id == null && $request->c_id != null  && $request->sp_id != null && $request->w_id == null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('c_id',$request->c_id)->where('sp_id',$request->sp_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->get();
                    $c = $request->c_id;
                    $sp = $request->sp_id;
                    $p = 0;
                    $w = 0;
                    $check = 6;
                }

                if($request->p_id == null && $request->c_id != null  && $request->sp_id == null && $request->w_id != null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('c_id',$request->c_id)->where('w_id',$request->w_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->get();
                    $c = $request->c_id;
                    $sp = 0;
                    $p = 0;
                    $w = $request->w_id;
                    $check = 7;
                }

                if($request->p_id == null && $request->c_id == null  && $request->sp_id != null && $request->w_id != null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('sp_id',$request->csp_id)->where('w_id',$request->w_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->get();
                    $c = 0;
                    $sp = $request->sp_id;
                    $p = 0;
                    $w = $request->w_id;
                    $check = 8;
                }

                if($request->p_id != null && $request->c_id == null  && $request->sp_id != null && $request->w_id == null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('sp_id',$request->sp_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_id',$request->p_id)
                    ->get();
                    $c = 0;
                    $sp = $request->sp_id;
                    $p = $request->p_id;
                    $w = 0;
                    $check = 9;
                }

                if($request->p_id != null && $request->c_id == null  && $request->sp_id == null && $request->w_id != null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('w_id',$request->w_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_id',$request->p_id)
                    ->get();
                    $c = 0;
                    $sp = 0;
                    $p = $request->p_id;
                    $w = $request->w_id;
                    $check = 10;
                }

                if($request->p_id != null && $request->c_id != null  && $request->sp_id != null && $request->w_id == null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('c_id',$request->c_id)->where('sp_id',$request->sp_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_id',$request->p_id)
                    ->get();
                    $c = $request->c_id;
                    $sp = $request->sp_id;
                    $p = $request->p_id;
                    $w = 0;
                    $check = 11;
                }

                if($request->p_id == null && $request->c_id != null  && $request->sp_id != null && $request->w_id != null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('c_id',$request->c_id)->where('sp_id',$request->sp_id)
                        ->where('w_id',$request->w_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->get();
                    $c = $request->c_id;
                    $sp = $request->sp_id;
                    $p = 0;
                    $w = $request->w_id;
                    $check = 12;
                }

                if($request->p_id != null && $request->c_id != null  && $request->sp_id == null && $request->w_id != null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('c_id',$request->c_id)->where('w_id',$request->w_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_id',$request->p_id)
                    ->get();
                    $c = $request->c_id;
                    $sp = 0;
                    $p = $request->P_id;
                    $w = $request->w_id;
                    $check = 13;
                }

                if($request->p_id != null && $request->c_id == null  && $request->sp_id != null && $request->w_id != null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('sp_id',$request->sp_id)->where('w_id',$request->w_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_id',$request->p_id)
                    ->get();
                    $sp = $request->sp_id;
                    $c = 0;
                    $p = $request->p_id;
                    $w = $request->w_id;
                    $check = 14;
                }

                if($request->p_id != null && $request->c_id != null  && $request->sp_id != null && $request->w_id != null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('c_id',$request->c_id)->where('sp_id',$request->sp_id)
                        ->where('w_id',$request->w_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('p_id',$request->p_id)
                    ->get();
                    $sp = $request->sp_id;
                    $c = $request->c_id;
                    $p = $request->p_id;
                    $w = $request->w_id;
                    $check = 15;
                }

                return view('sales.report',compact('ware','product','customer','saledetail','from','to','c','sp','p','w','check','index','person'));
            }
            else
            {
                if($request->to == null)
                {
                    $date = $request->from;
                }
                if($request->from == null)
                {
                    $date = $request->to;
                }
                $index = 3;
                // dd($date);

                if($request->p_id == null && $request->c_id == null && $request->sp_id == null && $request->w_id == null)
                {
                    $saledetail=SaleDetails::with(['sale','sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->get();
                    $c = 0;
                    $sp = 0;
                    $types = 0;
                    $w = 0;
                    $p = 0;
                    $check = 0;
                    // dd($saledetail);
                }
                if($request->p_id != null && $request->c_id == null && $request->sp_id == null && $request->w_id == null)
                {
                    $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                    $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                    $saledetail=SaleDetails::with(['sale','sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->where('p_id',$p_id)
                    ->where('type',$type)
                    ->get();
                    $c = 0;
                    $sp = 0;
                    $p = $p_id;
                    $types = $type;
                    $w = 0;
                    $check = 1;
                }
                if($request->p_id == null && $request->c_id == null && $request->sp_id == null && $request->w_id != null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('w_id',$request->w_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->get();
                    $c = 0;
                    $sp = 0;
                    $p = 0;
                    $w =  $request->w_id;
                    $types = 0;
                    $check = 2;
                }
                if($request->c_id != null && $request->sp_id == null && $request->p_id == null && $request->w_id == null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('c_id',$request->c_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->get();
                    $c = $request->c_id;
                    $sp = 0;
                    $p = 0;
                    $types = 0;
                    $w = 0;
                    $check = 3;
                }

                if($request->p_id == null && $request->c_id == null && $request->sp_id != null && $request->w_id == null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('sp_id',$request->sp_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->get();
                    $c = 0;
                    $types = 0;
                    $sp = $request->sp_id;
                    $p = 0;
                    $w = 0;
                    $check = 4;
                }

                if($request->p_id != null && $request->c_id != null  && $request->sp_id == null && $request->w_id == null)
                {
                    $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                    $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('c_id',$request->c_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->where('p_id',$p_id)
                    ->where('type',$type)
                    ->get();
                    $c = $request->c_id;
                    $sp = 0;
                    $p = $p_id;
                    $types = $type;
                    $w = 0;
                    $check = 5;
                }

                if($request->p_id == null && $request->c_id != null  && $request->sp_id != null && $request->w_id == null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('c_id',$request->c_id)->where('sp_id',$request->sp_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->get();
                    $c = $request->c_id;
                    $sp = $request->sp_id;
                    $p = 0;
                    $w = 0;
                    $types = 0;
                    $check = 6;
                }

                if($request->p_id == null && $request->c_id != null  && $request->sp_id == null && $request->w_id != null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('c_id',$request->c_id)->where('w_id',$request->w_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->get();
                    $c = $request->c_id;
                    $sp = 0;
                    $p = 0;
                    $types = 0;
                    $w = $request->w_id;
                    $check = 7;
                }

                if($request->p_id == null && $request->c_id == null  && $request->sp_id != null && $request->w_id != null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('sp_id',$request->csp_id)->where('w_id',$request->w_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->get();
                    $c = 0;
                    $sp = $request->sp_id;
                    $p = 0;
                    $types = 0;
                    $w = $request->w_id;
                    $check = 8;
                }

                if($request->p_id != null && $request->c_id == null  && $request->sp_id != null && $request->w_id == null)
                {
                    $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                    $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('sp_id',$request->sp_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->where('p_id',$p_id)
                    ->where('type',$type)
                    ->get();
                    $c = 0;
                    $sp = $request->sp_id;
                    $p = $p_id;
                    $types = $type;
                    $w = 0;
                    $check = 9;
                }

                if($request->p_id != null && $request->c_id == null  && $request->sp_id == null && $request->w_id != null)
                {
                    $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                    $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('w_id',$request->w_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->where('p_id',$p_id)
                    ->where('type',$type)
                    ->get();
                    $c = 0;
                    $sp = 0;
                    $p = $p_id;
                    $types = $type;
                    $w = $request->w_id;
                    $check = 10;
                }

                if($request->p_id != null && $request->c_id != null  && $request->sp_id != null && $request->w_id == null)
                {
                    $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                    $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('c_id',$request->c_id)->where('sp_id',$request->sp_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->where('p_id',$p_id)
                    ->where('type',$type)
                    ->get();
                    $c = $request->c_id;
                    $sp = $request->sp_id;
                    $p = $p_id;
                    $types = $type;
                    $w = 0;
                    $check = 11;
                }

                if($request->p_id == null && $request->c_id != null  && $request->sp_id != null && $request->w_id != null)
                {
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('c_id',$request->c_id)->where('sp_id',$request->sp_id)
                        ->where('w_id',$request->w_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->get();
                    $c = $request->c_id;
                    $sp = $request->sp_id;
                    $p = 0;
                    $types = 0;
                    $w = $request->w_id;
                    $check = 12;
                }

                if($request->p_id != null && $request->c_id != null  && $request->sp_id == null && $request->w_id != null)
                {
                    $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                    $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('c_id',$request->c_id)->where('w_id',$request->w_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->where('p_id',$p_id)
                    ->where('type',$type)
                    ->get();
                    $c = $request->c_id;
                    $sp = 0;
                    $p = $p_id;
                    $types = $type;
                    $w = $request->w_id;
                    $check = 13;
                }

                if($request->p_id != null && $request->c_id == null  && $request->sp_id != null && $request->w_id != null)
                {
                    $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                    $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('sp_id',$request->sp_id)->where('w_id',$request->w_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->where('p_id',$p_id)
                    ->where('type',$type)
                    ->get();
                    $sp = $request->sp_id;
                    $c = 0;
                    $p = $p_id;
                    $types = $type;
                    $w = $request->w_id;
                    $check = 14;
                }

                if($request->p_id != null && $request->c_id != null  && $request->sp_id != null && $request->w_id != null)
                {
                    $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
                    $type = substr($request->p_id,strpos($request->p_id,'-')+1);
                    $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                        $item->where('c_id',$request->c_id)->where('sp_id',$request->sp_id)
                        ->where('w_id',$request->w_id)->get();
                    },
                    'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->where('p_id',$p_id)
                    ->where('type',$type)
                    ->get();
                    $sp = $request->sp_id;
                    $c = $request->c_id;
                    $p = $p_id;
                    $types = $type;
                    $w = $request->w_id;
                    $check = 15;
                }
                // dd($check);
                return view('sales.report',compact('types','ware','product','customer','saledetail','date','c','sp','p','w','check','index','person'));
            }
        }

        else if($request->optradio == 'Product')
        {
            $p_id = substr($request->p_id,0,strpos($request->p_id,'-'));
            $type = substr($request->p_id,strpos($request->p_id,'-')+1);
            $saledetail=SaleDetails::with(['sale','sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
            ->where('p_id',$p_id)
            ->where('type',$type)
            ->get();
            $index = 6;
            $p=$p_id;
            $types = $type;
            return view('sales.report',compact('types','ware','product','customer','saledetail','p','index','person'));
        }

        else if($request->optradio == 'Warehouse')
        {
            $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                $item->where('w_id',$request->w_id)->get();
            },
            'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
            ->get();
            $index = 5;
            $w=$request->w_id;
            return view('sales.report',compact('ware','product','customer','saledetail','w','index','person'));
        }

        else if($request->optradio == 'customer')
        {
            $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                $item->where('c_id',$request->c_id)->get();
            },
            'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
            ->get();
            $index = 7;
            $c=$request->c_id;
            return view('sales.report',compact('ware','product','customer','saledetail','c','index','person'));
        }

        else if($request->optradio == 'saleperson')
        {
            $saledetail=SaleDetails::with(['sale'=>function($item) use ($request){
                $item->where('sp_id',$request->sp_id)->get();
            },
            'sale.saleperson','sale.warehouse','sale.customer','sale.biller','products','variant'])
            ->get();
            $index = 8;
            $sp=$request->sp_id;
            return view('sales.report',compact('ware','product','customer','saledetail','sp','index','person'));
        }
    }

}
