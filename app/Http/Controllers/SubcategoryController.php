<?php

namespace App\Http\Controllers;
use App\Subcategory;
use App\Category;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\PseudoTypes\False_;
use PHPUnit\Framework\Constraint\IsFalse;
use DataTables;
use Auth;
use App\User;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class SubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        $cat=Category::where('status',1)->get();
        return view('subcategory.index',compact('cat','permissions'));
    }

    public function datatable()
    {
        $sub=Subcategory::with(['category'])
        ->get();
        return DataTables::of($sub)->make();
    }

    public function status(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');
        $u_id = Auth::user()->id;
        $item = Subcategory::find($id);
        if ($item->update(['status' => $status])) {
            Subcategory::where('id',$id)
            ->update([
                'status' => $status,
                'updated_by' => $u_id
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $cat=Category::where('status',1)->get();
        $menu_id =   getMenuId($request);
        $data=[
            'isEdit' => false,
            'cat' => $cat,
            'permissions' => getRolePermission($menu_id)
        ];
        // dd($cat);
        return view('subcategory.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $sub=Subcategory::where('s_cat_name',$request->s_cat_name)
        ->where('cat_id',$request->cat_id)
        ->first();
        $u_id = Auth::user()->id;
        $role_id = Auth::user()->r_id;
        $env_a_id = env('ADMIN_ID');
        $env_m_id = env('MANAGER_ID');
        // dd($c_id);
        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $status = 1;
        }
        else
        {
            $status = 0;
        }
        $data = [
            's_cat_name' => $request->s_cat_name,
            'cat_id' => $request->cat_id,
            'created_by' => $u_id,
            'status' => $status,
        ];
        if($sub==null)
        {
            Subcategory::create($data);
            $u_name = Auth::user()->name;
            $user = User::where('r_id',env('ADMIN_ID'))->get();
            $data1 = [
                'notification' => 'New Sub-category has been added by '.$u_name,
                'link' => url('').'/subcategory',
                'name' => 'View Sub-categories',
            ];
            Notification::send($user, new AddNotification($data1));
            toastr()->success('Subcategory added successfully!');
            return redirect()->back();
        }
        else
        {
            toastr()->error('Subcategory already exist of this type!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(request()->ajax())
        {
            $sub=Subcategory::with(['category'])
            ->where('id',$id)
            ->first();
            return $sub;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sub=Subcategory::with(['category'])
        ->where('id',$id)
        ->first();
        $cat=Category::where('status',1)->get();
        $data=[
            'isEdit' => true,
            'sub' => $sub,
            'cat' => $cat
        ];
        return view('subcategory.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sub=Subcategory::where('s_cat_name',$request->s_cat_name)
        ->where('cat_id',$request->cat_id)
        ->first();
        $u_id = Auth::user()->id;
        if($sub==null)
        {
            Subcategory::where('id',$id)
            ->update([
                's_cat_name' => $request->s_cat_name,
                'cat_id' => $request->cat_id,
                'updated_by' => $u_id
            ]);
            toastr()->success('Subcategory updated successfully!');
            return redirect(url('').'/subcategory');
        }
        else
        {
            toastr()->error('Subcategory already exist of this category type!');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
