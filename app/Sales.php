<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $table = 'sales';
    protected $primaryKey = 'id';
    protected $fillable = [
        'sale_date',
        'ref_no',
        'b_id',
        'c_id',
        'w_id',
        's_address',
        's_status',
        'p_status',
        'doc',
        'tax',
        'discount',
        'total',
        'note',
        'created_by',
        'updated_by',
        'sp_id',
        'discount_type',
        'expected_date',
        'advance',
        'tax_status',
        'pay_type',
        'return_status',
        's_type',
        'order_no_w',
    ];

    public function warehouse()
    {
        return $this->hasOne('App\Warehouse','id','w_id');
    }

    public function customer()
    {
        return $this->hasOne('App\Vendors','id','c_id');
    }

    public function biller()
    {
        return $this->hasOne('App\Vendors','id','b_id');
    }

    public function saleperson()
    {
        return $this->hasOne('App\Vendors','id','sp_id');
    }

    public function sdetails()
    {
        return $this->hasMany('App\SaleDetails','s_id','id');
    }

    public function transaction()
    {
        return $this->hasMany('App\TransactionHistory', 'p_s_id', 'id');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }


    public function return()
    {
        return $this->hasOne('App\SaleReturn','sale_id','id');
    }

    // public function transaction()
    // {
    //     return $this->belongsTo('App\TransactionHistory');
    // }

    // public function sp()
    // {
    //     return $this->morphMany('App\TransactionHistory', 'order');
    // }
}
