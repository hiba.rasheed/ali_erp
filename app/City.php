<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'city';
    protected $primaryKey = 'id';
    protected $fillable = [
        'c_name',
        'created_by',
        'updated_by',
        'status',
    ];

    public function warehouse()
    {
        return $this->belongsTo('App\Warehouse');
    }
    public function vendor()
    {
        return $this->belongsTo('App\Vendors');
    }

    public function bank()
    {
        return $this->belongsTo('App\Bank');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }
}
