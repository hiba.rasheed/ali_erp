<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockOutRequestDetails extends Model
{
    protected $table = 'stock_out_request_details';
    protected $primaryKey = 'id';
    protected $fillable = [
        'r_id',
        'p_id',
        'quantity',
        'type'
    ];

    public function products()
    {
        return $this->hasOne('App\Products','id','p_id');
    }

    public function variant()
    {
        return $this->hasOne('App\ProductVariants','id','p_id');
    }


    public function request()
    {
        return $this->hasOne('App\StockOutRequest','id','r_id');
    }

}
