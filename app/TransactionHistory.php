<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionHistory extends Model
{
    protected $table = 'transaction_history';
    protected $primaryKey = 'id';
    protected $fillable = [
        'p_s_id',
        'p_type',
        't_type',
        'paid_by',
        'ref_no',
        'total',
        'cheque_no',
        'cc_no',
        'gift_no',
        'cc_holder',
        'note',
        'doc',
        'created_by',
        'updated_by',
        'b_id',
        'merchant_type'
    ];

    public function sales()
    {
        return $this->hasOne('App\Sales','id','p_s_id');
    }

    public function purchase()
    {
        return $this->hasOne('App\PurchaseOrder','id','p_s_id');
    }

    public function bank()
    {
        return $this->hasOne('App\Bank','id','b_id');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }

}
