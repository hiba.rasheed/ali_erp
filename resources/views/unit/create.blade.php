@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/unit">Unit</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Unit</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-balance-scale font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Unit</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('unit.update',$unit->id) :  route('unit.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Unit Name*</label>
                                        <input value="{{$unit->u_name ?? old('u_name')}}" class="form-control" type="text" placeholder="Enter Unit Name" name="u_name" required>
                                        <span class="text-danger">{{$errors->first('u_name') ? 'unit already exist' : null}}</span>
                                    </div>
                                </div>
                            </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>

@endsection
@section('custom-script')
@toastr_js
@toastr_render
@endsection
