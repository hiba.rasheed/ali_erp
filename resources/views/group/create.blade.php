@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/group">Group</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Group</span>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-users font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Group</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('group.update',$group->id) :  route('group.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Group Name*</label>
                                        <input value="{{$group->name ?? old('name')}}" class="form-control" type="text" placeholder="Enter Group Name" name="name" required>
                                        <span class="text-danger">{{$errors->first('name') ? 'Group already exist' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Group Type*</label>
                                        <select class="form-control selectpicker" name="g_type" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                            <option {{$group->g_type == 'Customer' ? 'selected' : null }} value="Customer">Customer</option>
                                            <option {{$group->g_type == 'Price' ? 'selected' : null }} value="Price">Price</option>
                                            @else
                                            <option value="Customer">Customer</option>
                                            <option value="Price">Price</option>

                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0 col-md-9">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>

@endsection
@section('custom-script')
@toastr_js
@toastr_render
@endsection
