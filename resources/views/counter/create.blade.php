@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/counter">Counter</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Counter</span>
</li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-gg font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Counter</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('counter.update',$counter->id) :  route('counter.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Counter No*</label>
                                        <input value="{{$counter->counterNo ?? old('counterNo')}}" {{$isEdit ? 'readonly' : ''}} class="form-control" type="text" placeholder="Enter counter No" name="counterNo" required>
                                        {{-- <span class="text-danger">{{$errors->first('b_name') ? 'brand already exist' : null}}</span> --}}
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Opening Shift*</label>
                                        <input value="{{$counter->open ?? old('open')}}" class="form-control" type="time" placeholder="Enter Opening Shift Time" required name="open">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Closing Shift*</label>
                                        <input value="{{$counter->close ?? old('close')}}" class="form-control" type="time" placeholder="Enter Closing Shift Time" required name="close">
                                    </div>
                                    {{-- <span class="text-danger">{{$errors->first('c_p_contactNo') ? 'Contact number not be greater than 11 digits' : null}}</span> --}}
                                </div>
                            </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>


@endsection
@section('custom-script')
@toastr_js
@toastr_render
@endsection
