@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/stocks">Stock</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title"  style="background: #32c5d2;">

                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-tasks font-white"></i>Current Stock
                                            </div>
                                            <div class="col-md-5 col-sm-5"></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                                @if(in_array('excel',$permissions))
                                                    <a style="margin-left:-20px"  href="{{route('stocks.excel')}}">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if(in_array('pdf',$permissions))
                                                    <a style="margin-left:-50px;" href="{{route('stocks.pdf')}}">
                                                        <i class="fa fa-file-pdf-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if(in_array('Add',$permissions))
                                                    <a id="GFG" href="{{route('stocks.create')}}">
                                                        <button style="background: #00CCFF; margin-left:20px; margin-top:-20px" type="button" class="btn btn-block btn-primary btn-md ">Add Stock</button>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Product Name</label>
                                                            <select class="form-control selectpicker" data-live-search="true" name="p_name" >
                                                                <option value=""  selected>Select...</option>
                                                                @foreach ($product as $s)
                                                                    @if ($s->vstatus == 0)
                                                                        <option value="{{$s->pro_code}} - {{$s->pro_name}}">{{$s->pro_code}} - {{$s->pro_name}} - {{$s->p_type}} - {{$s->brands->b_name}}</option>
                                                                    @else
                                                                        @foreach ($s->variants as $v)
                                                                            <option value="{{$v->name}}">{{$v->name}} - {{$s->p_type}} - {{$s->brands->b_name}}</option>
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Warehouse Name</label>
                                                            <select class="form-control selectpicker" data-live-search="true" name="w_name" >
                                                                <option value="" selected>Select...</option>
                                                                @foreach ($warehouse as $u)
                                                                <option  value="{{$u->w_name}}">{{$u->w_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">WareHouse Type</label>
                                                            <select class="form-control selectpicker" data-live-search="true" name="w_type" id="cat_id" >
                                                                <option value="" selected>Select...</option>
                                                                <option value="Standard">Standard</option>
                                                                <option value="Finished">Finished</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Code</label>
                                                            <input type="text" name="code"  class="form-control" placeholder="Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Brand Name</label>
                                                            <select class="form-control selectpicker" data-live-search="true" name="b_name" >
                                                                <option value="" selected>Select...</option>
                                                                @foreach ($brand as $u)
                                                                <option  value="{{$u->b_name}}">{{$u->b_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Category Name</label>
                                                            <select class="form-control selectpicker" data-live-search="true" name="c_name" id="cat_id" >
                                                                <option value="" selected>Select...</option>
                                                                @foreach ($cat as $u)
                                                                <option  value="{{$u->cat_name}}">{{$u->cat_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Product Type</label>
                                                            <select name="ptype" class="form-control selectpicker ">
                                                            <option selected="" value="">No Filter</option>
                                                            <option>Raw Material</option>
                                                            <option>Packaging</option>
                                                            <option>Finished</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8"></div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        {{-- <label for="">Unit Name</label> --}}
                                                        <label for="" style="visibility: hidden">.</label>
                                                        <button style="background: #32c5d2" id="search" style="color: " class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Warehouse Name</th>
                                                            <th>Warehouse Type</th>
                                                            <th>Code - Product Name</th>
                                                            <th>Product Type</th>
                                                            <th>Weight</th>
                                                            <th>Brand</th>
                                                            <th>Category</th>
                                                            <th>Quantity</th>
                                                            <th>Cost</th>
                                                            <th>Price</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a = 1;
                                                            $cost = 0 ;
                                                            $count = 0 ;
                                                            $price = 0 ;
                                                            $count1 = 0 ;
                                                        @endphp
                                                        @foreach ($stock as $s)
                                                            <tr>
                                                                <td>
                                                                    {{$a}}
                                                                </td>
                                                                <td>
                                                                    {{$s->warehouse->w_name}}
                                                                </td>
                                                                <td>
                                                                    {{$s->warehouse->w_type}}
                                                                </td>
                                                                @if ($s->type == 1)
                                                                    <td>
                                                                        {{$s->variant->name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->p_type}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->variant->product->weight == null ? $s->variant->product->unit->u_name : $s->variant->product->weight.$s->variant->product->unit->u_name }}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->variant->product->brands->b_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->variant->product->category->cat_name}}
                                                                    </td>
                                                                @else
                                                                    <td>
                                                                        {{$s->products->pro_code}} - {{$s->products->pro_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->p_type}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->weight == null ? $s->products->unit->u_name : $s->products->weight.$s->products->unit->u_name }}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->brands->b_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->category->cat_name}}
                                                                    </td>
                                                                @endif
                                                                <td>
                                                                    {{$s->quantity}}
                                                                </td>
                                                                <td>
                                                                    @if ($s->stock->isEmpty())
                                                                        {{$s->type == 1 ? $s->variant->cost : $s->products->cost}}
                                                                    @else
                                                                        @php
                                                                            $count = count($s->stock);
                                                                        @endphp
                                                                        @foreach ($s->stock as $p)
                                                                            @php
                                                                                $cost+=$p->cost;
                                                                            @endphp
                                                                        @endforeach
                                                                        @if ($count > 0)
                                                                        {{round($cost/$count)}}
                                                                        @else
                                                                        {{0}}
                                                                        @endif
                                                                    @endif
                                                                </td>
                                                                @if ($s->sales->isEmpty())
                                                                    <td>
                                                                        {{$s->type == 1 ? $s->variant->price : $s->products->price}}
                                                                    </td>
                                                                @else
                                                                    @php
                                                                        $count1 = count($s->sales);
                                                                    @endphp
                                                                    @foreach ($s->sales as $t)
                                                                        @php
                                                                            $price+=$t->price;
                                                                        @endphp
                                                                    @endforeach
                                                                    <td>
                                                                        @if ($count > 0)
                                                                        {{round($price/$count1)}}
                                                                        @else
                                                                        {{0}}
                                                                        @endif

                                                                    </td>
                                                                @endif
                                                            </tr>
                                                            @php
                                                                $a++;
                                                                $count = 0 ;
                                                                $count1 = 0 ;
                                                                $price = 0 ;
                                                                $cost = 0 ;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('custom-script')
    @toastr_js
    @toastr_render
        <script type="text/javascript">
            $(document).ready(function () {
                var table = $('#example').DataTable({
                    scrollX: true
                });

                $('#advanceSearch').submit(function(e){
                e.preventDefault();

                table.columns(1).search($('select[name="w_name"]').val());
                table.columns(2).search($('select[name="w_type"]').val());
                table.columns(3).search($('input[name="code"]').val());
                table.columns(3).search($('select[name="p_name"]').val());
                table.columns(4).search($('select[name="ptype"]').val());
                table.columns(6).search($('select[name="b_name"]').val());
                table.columns(7).search($('select[name="c_name"]').val());
                table.draw();
            });
        });
    </script>
    @endsection
@endsection
