@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .form-check-inline {
    display: inline-flex;
    align-items: center;
    padding-left: 0;
    margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
    position: static;
    margin-top: 0;
    margin-right: .3125rem;
    margin-left: 0;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/stocks">Stocks</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-tasks font-white"></i>Stock In History
                                            </div>
                                            <div class="col-md-5 col-sm-5 "></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                                @if(in_array('excel',$permissions))
                                                    <a style="margin-left:-20px;" href="{{route('stocks.excel')}}">
                                                        <i class="fa fa-file-excel-o font-white"></i>
                                                    </a>
                                                @endif
                                                @if(in_array('Add',$permissions))
                                                    <a id="GFG" href="{{route('stocks.create')}}" >
                                                        <button style="background: #00CCFF; margin-left:20px; margin-top:-20px" type="button"  class="btn btn-block btn-primary btn-md ">Add Stock</button>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="{{url('')}}/stocks/stockInSearch" method="POST" id="advanceSearch">
                                                @csrf

                                                <input type="hidden" name="menuid" value="{{$menu_id}}">
                                                <div class="tableview">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio2">
                                                            <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Date
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Supplier Name</label>
                                                                <select class="form-control " data-live-search="true" name="s_id" id="s_name"  disabled>
                                                                    <option value=""  selected>Select...</option>
                                                                    @foreach ($supplier as $u)
                                                                    <option  value="{{$u->id}}">{{$u->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Product Name</label>
                                                                <select class="form-control " data-live-search="true" name="p_id" id="p_name" disabled>
                                                                    <option value=""  selected>Select...</option>
                                                                    @foreach ($product as $s)
                                                                        @if ($s->vstatus == 0)
                                                                            <option value="{{$s->id.'-'.'0'}}">{{$s->pro_code}} - {{$s->pro_name}} - {{$s->p_type}} - {{$s->brands->b_name}}</option>
                                                                        @else
                                                                            @foreach ($s->variants as $v)
                                                                                <option value="{{$v->id.'-'.'1'}}">{{$v->name}} - {{$s->p_type}} - {{$s->brands->b_name}}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Year</label>
                                                                <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Month</label>
                                                                <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">From</label>
                                                                <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">To</label>
                                                                <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="" style="visibility: hidden">.</label>
                                                                <button disabled id="search" style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                                <i class="fa fa-search pr-1"></i> Search</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Supplier</th>
                                                            <th>Code - Product Name</th>
                                                            <th>Cost</th>
                                                            <th>Warehouse</th>
                                                            <th>Brand</th>
                                                            <th>Category</th>
                                                            <th>Weight</th>
                                                            <th>Received Quantity</th>
                                                            <th>Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a = 1;
                                                        @endphp
                                                        @foreach ($stock as $s)
                                                            <tr>
                                                                <td>
                                                                    {{$a}}
                                                                </td>
                                                                <td>
                                                                    {{$s->supplier->name ??  'no spplier'}}
                                                                </td>
                                                                @if ($s->type == 1)
                                                                    <td>
                                                                        {{$s->variant->name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->cost}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->warehouse->w_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->variant->product->brands->b_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->variant->product->category->cat_name}}
                                                                    </td>
                                                                    @if ($s->variant->product->weight == null)
                                                                        <td>
                                                                            {{$s->variant->product->unit->u_name}}
                                                                        </td>
                                                                    @else
                                                                        <td>
                                                                            {{$s->variat->product->weight.$s->variant->product->unit->u_name}}
                                                                        </td>
                                                                    @endif
                                                                @else
                                                                    <td>
                                                                        {{$s->products->pro_code}} - {{$s->products->pro_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->cost}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->warehouse->w_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->brands->b_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->category->cat_name}}
                                                                    </td>
                                                                    @if ($s->products->weight == null)
                                                                        <td>
                                                                            {{$s->products->unit->u_name}}
                                                                        </td>
                                                                    @else
                                                                        <td>
                                                                            {{$s->products->weight.$s->products->unit->u_name}}
                                                                        </td>
                                                                    @endif
                                                                @endif
                                                                <td>
                                                                    {{$s->quantity}}
                                                                </td>
                                                                <td>
                                                                    {{$s->stock_date}}
                                                                </td>
                                                            </tr>
                                                            @php
                                                                $a++;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('custom-script')
        @toastr_js
        @toastr_render
        <script type="text/javascript">
            $(document).ready(function () {
                var table = $('#example').DataTable({
                    order: [[ 0, "desc" ]],
                    scrollX: true,
                });
                $('input:radio[name="optradio"]').change(function(){
                    if ($(this).is(':checked')) {
                        $('#search').prop('disabled',false);
                        var val = $(this).val();
                        if(val == 'Year')
                        {
                            $('#year').prop('disabled',false);
                            $('#year').attr('required',true);
                            $('#month').attr('required',false);
                            $('#month').prop('disabled',true);
                            $('#from').prop('disabled',true);
                            $('#from').attr('required',false);
                            $('#to').prop('disabled',true);
                            $('#to').attr('required',false);
                            $('#p_name').prop('disabled',false);
                            $('#s_name').prop('disabled',false);
                            $('#p_name').addClass('selectpicker');
                            $('.selectpicker').selectpicker('render');
                            $('#s_name').addClass('selectpicker');
                            $('.selectpicker').selectpicker('render');
                        }
                        if(val == 'Month')
                        {
                            $('#month').prop('disabled',false);
                            $('#s_name').prop('disabled',false);
                            $('#p_name').prop('disabled',false);
                            $('#month').attr('required',true);
                            $('#year').prop('disabled',true);
                            $('#from').prop('disabled',true);
                            $('#to').prop('disabled',true);
                            $('#from').attr('required',false);
                            $('#to').attr('required',false);
                            $('#year').attr('required',false);
                            $('#p_name').addClass('selectpicker');
                            $('.selectpicker').selectpicker('render');
                            $('#s_name').addClass('selectpicker');
                            $('.selectpicker').selectpicker('render');
                        }
                        if(val == 'Date')
                        {
                            $('#from').prop('disabled',false);
                            $('#from').attr('required',true);
                            $('#to').prop('disabled',false);
                            $('#to').attr('required',false);
                            $('#s_name').prop('disabled',false);
                            $('#p_name').prop('disabled',false);
                            $('#month').attr('required',false);
                            $('#year').prop('disabled',true);
                            $('#month').prop('disabled',true);
                            $('#year').attr('required',false);
                            $('#p_name').addClass('selectpicker');
                            $('.selectpicker').selectpicker('render');
                            $('#s_name').addClass('selectpicker');
                            $('.selectpicker').selectpicker('render');
                        }
                    }
                });

            });

        </script>
    @endsection
@endsection
