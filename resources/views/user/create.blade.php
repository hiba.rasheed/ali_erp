@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/users">Users</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} User</span>
</li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-user font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} User</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('users.update',$user->id) :  route('users.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">
                            <input type="hidden" name="password" id="">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >User Name*</label>
                                        <input value="{{$user->name ?? old('name')}}"  class="form-control" type="text" placeholder="Enter User Name" name="name" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Email*</label>
                                        <input value="{{$user->email ?? old('email')}}" class="form-control" type="email" placeholder="Enter Email" name="email" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Role*</label>
                                        <select style="overflow-y: scroll;" id="r_id" size="1" class="form-control selectpicker" data-live-search="true" name="r_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                                @foreach ($roles as $s)
                                                <option {{$s->id == $user->r_id ? 'selected' : null}} value="{{$s->id}}">{{$s->name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($roles as $s)
                                                <option value="{{$s->id}}">{{$s->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Warehouse*</label>
                                        <select style="overflow-y: scroll;" id="w_id" size="1" class="form-control selectpicker" data-live-search="true" name="w_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                                @foreach ($ware as $s)
                                                <option {{$s->id == $user->w_id ? 'selected' : null}} value="{{$s->id}}">{{$s->w_name}} - {{$s->w_type}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($ware as $s)
                                                <option value="{{$s->id}}">{{$s->w_name}} - {{$s->w_type}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>


@endsection
@section('custom-script')
@toastr_js
@toastr_render
@endsection
