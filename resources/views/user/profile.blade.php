@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="#">Users</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>Update Profile</span>
</li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-user font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">Update Profile</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{ route('users.update',$user->id) }} " class="form-horizontal" method="PUT" >
                        @csrf
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >User Name*</label>
                                        <input readonly value="{{$user->name ?? old('name')}}"  class="form-control" type="text" placeholder="Enter User Name" name="name" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Email*</label>
                                        <input readonly value="{{$user->email ?? old('email')}}" class="form-control" type="email" placeholder="Enter Email" name="email" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Password*</label>
                                        <input type="password" name="password" class="form-control" required id="">
                                    </div>
                                </div>
                            </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>


@endsection
@section('custom-script')
@toastr_js
@toastr_render
@endsection
