@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/product">Product</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-list-alt font-white"></i>View Finish Products
                                            </div>
                                            <div class="col-md-5 col-sm-5 "></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                                @if(in_array('excel',$permissions))
                                                    <a style="margin-left:-20px"  href="{{route('product.finish.excel')}}">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if(in_array('Add',$permissions))
                                                    <a id="GFG" href="{{route('product.add')}}" >
                                                        <button style="background: #00CCFF; margin-left:20px; margin-top:-20px" type="button"  class="btn btn-block btn-primary btn-md ">Add Finish Product</button>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Product Name</label>
                                                            <input type="text" name="p_name"  class="form-control" placeholder="Product Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Code</label>
                                                            <input type="text" name="code"  class="form-control" placeholder="Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    {{-- <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Weight</label>
                                                            <input type="text" name="weight"  class="form-control" placeholder="Weight" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div> --}}
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Brand Name</label>
                                                        <select class="form-control selectpicker" data-live-search="true" name="b_name">
                                                            <option value=""  selected>Select...</option>
                                                            @foreach ($brands as $u)
                                                            <option  value="{{$u->b_name}}">{{$u->b_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Category Name</label>
                                                            <select class="form-control selectpicker" data-live-search="true" name="c_name">
                                                                <option value=""  selected>Select...</option>
                                                                @foreach ($cat as $u)
                                                                <option  value="{{$u->cat_name}}">{{$u->cat_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Active Status</label>
                                                        <select name="status" class="form-control selectpicker ">
                                                        <option selected="" value="">No Filter</option>
                                                        <option value="1">Active</option>
                                                        <option value="0">InActive</option>
                                                        </select>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Date</label>
                                                            <input type="date" name="date"  class="form-control" placeholder="Product Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8"></div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        {{-- <label for="">Unit Name</label> --}}
                                                        {{-- <label for="" style="visibility: hidden">.</label> --}}
                                                        <button style="background: #32c5d2" id="search" style="color: " class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="5%">S.No</th>
                                                            <th>Name</th>
                                                            <th width="5%">Code</th>
                                                            <th>Brand</th>
                                                            <th>Category</th>
                                                            <th>Date</th>
                                                            <th>Weight</th>
                                                            <th>Quantity</th>
                                                            <th width="18%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('modal')
    <div id="Sale" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Sales History</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-12">
                            <table id="example2" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Sale No</th>
                                        <th>Sale Date</th>
                                        <th>Sale Quantity</th>
                                        <th>Price</th>
                                        <th>Warehouse</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>


                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <div id="Product" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Product Detail</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-12">
                            <table id="example3" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Product Name</th>
                                        <th>Quantity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>


                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Product</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Product Name</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Name" id="pro_name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Product Code</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Code" id="pro_code" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Product Weight</label>
                                    <input class="form-control" type="number" placeholder="Enter Product Weight" id="weight" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Unit</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="unit" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Cost</label>
                                    <input class="form-control" type="number" placeholder="Enter Product Cost" id="cost" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Price</label>
                                    <input class="form-control" type="number" placeholder="Enter Product Code" id="price" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            {{-- <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Quantity</label>
                                    <input class="form-control" type="number" placeholder="Enter Product Quantity" id="quantity" readonly>
                                </div>
                            </div> --}}
                            {{-- <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Alert Quantity</label>
                                    <input class="form-control" type="number" placeholder="Enter Product Alert Quantity" id="alert_quantity" readonly>
                                </div>
                            </div>
                        </div> --}}

                        {{-- <div class="row"> --}}
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Brand</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Brand" id="brand" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Category</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Category" id="cat" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Sub Category</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Sub Category" id="sub" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >MRP</label>
                                    <input class="form-control" type="text" placeholder="Enter Product MRP" id="mrp" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row variant">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!--End Modal-->

    @endsection
    @section('custom-script')
    @toastr_js
@toastr_render
        {{-- view one product --}}
        <script>
            $(document).on('click','.view',function(){
                var id=$(this).attr("id");
                $.ajax({
                    url:"{{url('')}}/product/"+id,
                    method:"GET",
                    error: function (request, error) {
                                alert(" Can't do because: " + error +request);
                            },
                    success:function(data){
                        console.log(data);
                        $('#pro_name').val(data.pro_name);
                        $('#pro_code').val(data.pro_code);
                        $('#weight').val(data.weight);
                        $('#unit').val(data.unit.u_name);
                        $('#brand').val(data.brands.b_name);
                        $('#cat').val(data.category.cat_name);
                        $('#sub').val(data.subcategory.s_cat_name);
                        // $('#ware').val(data.warehouse.w_name);
                        $('#cost').val(data.cost);
                        $('#price').val(data.price);
                        $('#mrp').val(data.mrp);
                        $('#quantity').val(data.quantity);
                        $('#alert_quantity').val(data.alert_quantity);
                        // if(data.variants != null)
                        // {
                        //     $('.variant').append('<div class="col-sm-6"><div class="form-outline"><label><b>Variants</b></label><div class="child"></div></div></div>');
                        //     for(var i = 0 ; i < data.variants.length ; i ++)
                        //     {
                        //         $('.child').append(data.variants[i].variant[0].name+'<br>');
                        //     }
                        // }
                    }
                });
            });
        </script>
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#example').DataTable({
            processing: true,
            serverSide: true,
            order: [[ 0, "desc" ]],
            scrollX: true,
            ajax: '{{route("product.finishProducts.datatable")}}',
            "columns": [{
                    "data": "id",
                    "defaultContent": ""
                },
                {
                    "data": "pro_name",
                    "defaultContent": ""
                },
                {
                    "data": "pro_code",
                    "defaultContent": ""
                },
                {
                    "data": "brands.b_name",
                    "defaultContent": ""
                },
                {
                    "data": "category.cat_name",
                    "defaultContent": ""
                },
                {
                    "data": "created_at",
                    "defaultContent": "",
                    "render": function (value) {
                        if (value === null) return "";
                        return moment(value).format('MM/DD/YYYY');
                    }
                },
                {
                    "data": "",
                    "defaultContent": "",
                    "render": function (data, type, row, meta) {
                        if(row.weight == null)
                        {
                            return row.unit.u_name;
                        }
                        else
                        {
                            return row.weight+row.unit.u_name;
                        }
                    }
                },
                {
                    "data": "total",
                    "defaultContent": "",
                    "render": function (data, type, row, meta) {
                        if(row.total == null)
                        {
                            return `0`;
                        }
                        else
                        {
                           return row.total;
                        }
                    }
                },
                {
                    "data": "status",
                    "defaultContent": ""
                },
            ],
            "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                },
                {
                    "targets": 0,
                    "render": function (data, type, row, meta) {
                        return meta.row + 1;
                    },
                },
                {
                    "targets": -1,
                    "render": function (data, type, row, meta) {
                        var checked = row.status == 1 ? 'checked' : null;
                        var edit = '{{route("product.finishProducts.edit",[":id"])}}';
                        edit = edit.replace(':id', row.id);
                        return `
                        @if(in_array('edit',$permissions))
                            <a id="GFG" href="` + edit + `" class="text-info p-1">
                                <button type="button" class="btn blue edit" >
                                    <i class="fa fa-edit"></i>
                                </button>
                            </a>
                        @endif
                        @if(in_array('show',$permissions))
                            <button type="button" data-target="#myModal" data-toggle="modal"  class="btn green view" id="`+row.id +`">
                                <i class="icon-eye"></i>
                            </button>
                        @endif
                        @if(in_array('status',$permissions))
                            <input class="status" type="checkbox" data-plugin="switchery" data-color="#005CA3" data-size="small" ` +
                                            checked + ` value="` + row.id + `">
                        @endif
                            <select style="margin-top:5px" class="form-control action" id="`+row.id +`" >
                                <option >Actions</option>
                                @if(in_array("product sale details",$permissions))
                                <option >Sale Details</option>
                                @endif
                                @if(in_array("product details",$permissions))
                                <option >Product Details</option>
                                @endif
                            </select>
                        `;
                    },
                },
            ],
            "drawCallback": function (settings) {
                var elems = Array.prototype.slice.call(document.querySelectorAll('.status'));
                if (elems) {
                elems.forEach(function (html) {
                    var switchery = new Switchery(html, {
                    color: '#007bff'
                    , secondaryColor: '#dfdfdf'
                    , jackColor: '#fff'
                    , jackSecondaryColor: null
                    , className: 'switchery'
                    , disabled: false
                    , disabledOpacity: 0.5
                    , speed: '0.1s'
                    , size: 'small'
                    });

                });
            }
            $('.status').change(function () {
                var $this = $(this);
                var id = $this.val();
                var status = this.checked;

                if (status) {
                    status = 1;
                } else {
                    status = 0;
                }
                // console.log(status);
                axios
                    .post('{{route("product.status")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    id: id,
                    status: status,
                    })
                    .then(function (responsive) {
                    console.log('responsive');
                    // location.reload();
                    })
                    .catch(function (error) {
                    console.log(error);
                    });
                });

            },
        });
        $('#advanceSearch').submit(function(e){
            e.preventDefault();
            // console.log($('input[name="p_name"]').val());
            table.columns(1).search($('input[name="p_name"]').val());
            table.columns(2).search($('input[name="code"]').val());
            // table.columns(3).search($('input[name="weight"]').val());
            table.columns(3).search($('select[name="b_name"]').val());
            table.columns(4).search($('select[name="c_name"]').val());
            table.columns(5).search($('input[name="date"]').val());
            table.columns(8).search($('select[name="status"]').val());
            table.draw();
        });
    });

    $(document).on('change','.action',function(){
        var val=$(this).val();
        if(val == 'Sale Details')
        {
            var id=$(this).attr("id");
            $("#example2 tbody").empty();
            $.ajax({
                url:"{{url('')}}/sales/product/"+id,
                method:"GET",
                error: function (request, error) {
                    lert(" Can't do because: " + error +request);
                },
                success:function(data){
                    console.log(data);
                    for (let i = 0; i < data.length; i++) {
                        $("#example2").append("<tr><td>"+data[i].s_id+"</td><td>"+data[i].sale.sale_date+"</td><td>"+data[i].quantity+"</td><td>"+data[i].price+"</td><td>"+data[i].sale.warehouse.w_name+"</td></tr>");
                    }
                    $('#Sale').modal("show");
                    $('.action').val('Actions');
                    $('#example2').DataTable();
                }
            });
        }
        if(val == 'Product Details')
        {
            var id=$(this).attr("id");
            $("#example3 tbody").empty();
            $.ajax({
                url:"{{url('')}}/product/detail/"+id,
                method:"GET",
                error: function (request, error) {
                    lert(" Can't do because: " + error +request);
                },
                success:function(data){
                    console.log(data);
                    var a = 1;
                    for (let i = 0; i < data.length; i++) {
                        if(data[i].type == 0)
                        {
                            $("#example3").append("<tr><td>"+a+"</td><td>"+data[i].rawproducts.pro_name+"</td><td>"+data[i].quantity+"</td></tr>");
                        }

                        else
                        {
                            $("#example3").append("<tr><td>"+a+"</td><td>"+data[i].rawproducts2.name+"</td><td>"+data[i].quantity+"</td></tr>");
                        }
                        a +=1;
                    }
                    $('#Product').modal("show");
                    $('.action').val('Actions');
                    $('#example3').DataTable();
                }
            });
        }
    });

</script>
    @endsection
@endsection
