@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/company">Company</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Company</span>
</li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-industry font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Company</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('company.update',$company->id) :  route('company.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Company Name*</label>
                                        <input value="{{$company->name ?? old('name')}}" class="form-control" type="text" placeholder="Enter Company Name" name="name" required>
                                        <span class="text-danger">{{$errors->first('name') ? 'company already exist' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Company Address*</label>
                                        <input value="{{$company->address ?? old('address')}}" class="form-control" type="text" placeholder="Enter Company Address" name="address" required>
                                        <span class="text-danger">{{$errors->first('address') ? 'company address already exist' : null}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Contact Person Name*</label>
                                        <input value="{{$company->c_p_name ?? old('c_p_name')}}" class="form-control" type="text" placeholder="Enter Contact Person Name" name="c_p_name" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Contact Person Number*</label>
                                        <input autocomplete="off" value="{{$company->c_p_contactNo ?? old('c_p_contactNo')}}" class="form-control" type="number" placeholder="Enter Contact Person Number" name="c_p_contactNo" required>
                                    </div>
                                </div>
                            </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>


@endsection
@section('custom-script')
@toastr_js
@toastr_render
@endsection
