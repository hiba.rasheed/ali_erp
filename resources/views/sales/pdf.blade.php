<html>
  <head>
    {{-- <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> --}}
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
      .attendance-table table{
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #000;
        font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

        .blank-cell{

        min-width: 50px;


        }

        .attendance-cell{

        padding: 8px;
        /* border: 1px solid #000; */

        }

        .attendance-table table th.attendance-cell, .attendance-table table td.attendance-cell {
            border: 1px solid #000;
        }
        h2,h4 {
            color: #00CCFF;
        }
        .table-inv
        {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            border-collapse: collapse;
            width: 50%;
        }
        .table-inv thead
        {
            background-color: #ADD8E6;
        }
        .table-inv td,th
        {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        .row
        {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            padding-left: 30px;
        }
        .col
        {
            font-size: 18px;
            background: #F0FFFF;
            height: 28px;
        }
        header
        {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            font-size: 30px;
            text-align: center;
            padding-top: 15px;
            background: #F0F8FF;
            height: 50px;
            font-weight: bold;
        }
        .label
        {
            background: #F0F8FF;
            font-size: 15px;
            text-align: left;
            color: #000;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            height: 150px;
            padding-left: 30px;
            /* font-weight: bold; */
        }
        @page
        {
            margin: 0;
            size: A3;
        }
        #overlay
        {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background-image: url('{{url('')}}/uploads/CN7R2pF2hxI5A9KsFYNhsFSJ2HYFi0bsNDjt016D.jpg');
            background-position: center top;
            background-repeat: no-repeat;
            z-index: -1;
        }
  </style>
  <body>
    <img src="{{url('')}}/uploads/posch.jpg" style="margin-top: 10px;  float: left;" width="110px" height="40px" alt="logo" class="logo-default"  />
    <header>
        SALE RECIEPT
    </header>
    <div class="label col-sm-12">
        <label><b>Reference#: </b>{{$sales->ref_no}}</label>
        <br>
        @php
            $date=Carbon\Carbon::parse($sales->sale_date)->format('d-m-y');
        @endphp
        <label><b>Sale Date: </b>{{$date}}</label>
        <br>
        <label><b>Sale#: </b>{{$sales->id}}</label>
        <br>
        <label><b>Customer ID: </b>{{$sales->customer->id}}</label>
        <br>
        <label><b>Terms: </b>{{$sales->note}}</label>
        <br>
        <label><b>Sale Person Name: </b>{{$sales->saleperson->name}}</label>
        <br>
        <label><b>Remaining Balance: </b>{{$balance}}</label>
    </div>
    <div class="form-body">
        <div class="row col">
            <div>
                <div class="form-outline">
                    <label for=""><b>Bill From:</b></label>
                </div>
            </div>
            <div style="margin-top: -18px; margin-left: 700px">
                <div class="form-outline">
                    <label for=""><b>Bill To:</b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div>
                <div class="form-outline">
                    <p><b>Biller Name: {{ucwords($sales->biller->name)}}</b></p>
                    <p><b>Address: {{ucwords($sales->biller->address)}}</b></p>
                    <p><b>Contact No: {{ucwords($sales->biller->c_no)}}</b></p>
                    {{-- <p><b>Email: {{$sales->biller->email}}</b></p> --}}
                    <p><b>Warehouse Name: {{ucwords($sales->warehouse->w_name)}}</b></p>
                </div>
            </div>
            <div style="margin-top: -170px; margin-left: 700px">
                <div class="form-outline">
                    <p><b>Customer Name: {{ucwords($sales->customer->name)}}</b></p>
                    <p><b>Company Name: {{ucwords($sales->customer->company)}}</b></p>
                    {{-- <p><b>Customer Address: {{ucwords($sales->customer->address)}}</b></p> --}}
                    <p><b>Contact No: {{ucwords($sales->customer->c_no)}}</b></p>
                    {{-- <p><b>Email: {{ucwords($sales->customer->email)}}</b></p> --}}
                    <p><b>Shipment Address: {{ucwords($sales->s_address)}}</b></p>
                </div>
            </div>
        </div>
    </div>
        <div class="attendance-table" >
        <table class="table table-striped table-bordered">

            <thead>
                <tr>
                    <th class="attendance-cell"  width="5%">S.No</th>
                    <th class="attendance-cell"  width="30%">Code - Name</th>
                    {{-- <th class="attendance-cell"  width="10%">Weight</th> --}}
                    <th class="attendance-cell"   width="15%">Brand</th>
                    <th class="attendance-cell"   width="7%">Price</th>
                    <th class="attendance-cell"  width="7%">Quantity</th>
                    <th class="attendance-cell"  width="7%">Sub total</th>
                    <th class="attendance-cell"  width="7%">Discount</th>
                    <th class="attendance-cell"  width="10%">Discounted Amount</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $amount=0;
                    $total=0;
                    $tax=0;
                @endphp
                @foreach($saledetail as $d)
                    <tr>
                        <td class="attendance-cell" >{{$d->id}}</td>
                        @if ($d->type == 1)
                            <td class="attendance-cell" >{{$d->variant->name}}</td>
                            <td class="attendance-cell" >{{$d->variant->product->brands->b_name}}</td>
                        @else
                         <td class="attendance-cell" >{{$d->products->pro_code}} - {{$d->products->pro_name}}</td>
                         <td class="attendance-cell" >{{$d->products->brands->b_name}}</td>
                        @endif
                        {{-- <td class="attendance-cell" >{{$d->products->weight}} {{$d->products->unit->u_name}}</td> --}}
                        <td class="attendance-cell" >{{$d->price}}</td>
                        <td class="attendance-cell" >{{$d->quantity}}</td>
                        <td class="attendance-cell" >{{$d->quantity * $d->price}}</td>
                        <td class="attendance-cell" >{{$d->discount_percent}}</td>
                        <td class="attendance-cell" >{{$d->sub_total}}</td>
                    </tr>
                    @php
                        $amount+=$d->sub_total;
                        $total+=$d->sub_total;
                    @endphp
                @endforeach
                <tr >
                    <td class="attendance-cell"  colspan="6"></td>
                    <td class="attendance-cell" ><b>SUB TOTAL</b></td>
                    <td class="attendance-cell" >{{$amount}}</td>
                </tr>
                @if ($sales->tax!=null)
                    <tr>
                        <td class="attendance-cell"  colspan="6"></td>
                        <td class="attendance-cell" ><b>TAX</b></td>
                        <td class="attendance-cell" >{{$sales->tax}}</td>
                    </tr>
                    @php
                        $tax=(($amount * $sales->tax)/100);
                        $total += $tax;
                    @endphp
                @endif
                @if ($sales->tax==null && $sales->discount==null)
                    @php
                        $total = $amount;
                    @endphp
                @endif
                <tr>
                    <td class="attendance-cell"  colspan="6"></td>
                    <td class="attendance-cell" ><b>TOTAL</b></td>
                    <td class="attendance-cell" >{{$total}}</td>
                </tr>
                <tr>
                    <td class="attendance-cell"  colspan="6"></td>
                    <td class="attendance-cell" ><b>PAID AMOUNT</b></td>
                    <td class="attendance-cell" >{{$th}}</td>
                </tr>
                <tr>
                    <td class="attendance-cell"  colspan="6"></td>
                    <td class="attendance-cell" ><b>BALANCE</b></td>
                    <td class="attendance-cell" >{{$total - $th}}</td>
                </tr>
            </tbody>

        </table>

    </div>
  </body>
</html>
