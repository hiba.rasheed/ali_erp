

<div id="paymentPurchase" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Add Payment</h4>
            </div>
            <form action="{{route('transaction.store')}}" class="form-horizontal" method="POST">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="p_s_id" id="p_s_id">
                    <input type="hidden" name="p_type" id="p_type">
                    <input type="hidden" name="t_type" id="t_type">
                        <div class="row">
                            {{-- <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Reference Number</label>
                                    <input class="form-control" type="number" min="0" placeholder="Enter Reference Number" name="p_ref_no" id="p_ref_no">
                                </div>
                            </div> --}}
                            <div class="col-md-6">
                                <div class="form-outline">
                                    <label >Total Amount</label>
                                    <input class="form-control" type="number" min="0" name="actual" readonly id="actual">
                                </div>
                            </div>
                            {{-- <div class="col-md-4">
                                <div class="form-outline">
                                    <label >Payment Status</label>
                                    <select class="form-control selectpicker" name="p_status" id="p_status" required>
                                        <option value="" disabled selected>Select...</option>
                                        <option value="Partial">Partial</option>
                                        <option value="Paid">Paid</option>
                                    </select>
                                </div>
                            </div> --}}
                        {{-- </div>
                        <div class="row"> --}}
                            <div class="col-md-6">
                                <div class="form-outline">
                                    <label >Paid Amount*</label>
                                    <input required class="form-control" type="number" min="1" id="p_total" name="p_total">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-outline">
                                    <label >Paying By*</label>
                                    <select required class="form-control selectpicker" name="paid_by" id="paid_by" >
                                        <option value="" disabled selected>Select...</option>
                                        <option value="Cash">Cash</option>
                                        <option value="Gift">Gift Card</option>
                                        <option value="Credit">Credit Card</option>
                                        <option value="Cheque">Cheque</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="cheque" hidden>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Cheque Number*</label>
                                        <input class="form-control" type="text" placeholder="Enter Cheque Number" id="cheque_no" name="cheque_no">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Bank*</label>
                                        <select class="form-control selectpicker" id="bank_id" data-live-search="true" name="b_id" >
                                            <option value="" disabled selected>Select...</option>
                                                @foreach ($bank as $s)
                                                <option value="{{$s->id}}">{{$s->name}} - {{$s->branch}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="gift" hidden>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-outline">
                                        <label >Gift Card Number*</label>
                                        <input class="form-control" type="text" placeholder="Enter Gift Card Number" id="gift_no" name="gift_no">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="credit" hidden>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Credit Card Number*</label>
                                        <input class="form-control" type="text" placeholder="Enter Credit Card Number" id="cc_no" name="cc_no">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Holder Name*</label>
                                        <input class="form-control" type="text" placeholder="Enter Holder Name" id="cc_holder" name="cc_holder">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Bank*</label>
                                        <select class="form-control selectpicker" id="bank_id1" data-live-search="true" name="b_id" >
                                            <option value="" disabled selected>Select...</option>
                                                @foreach ($bank as $s)
                                                <option value="{{$s->id}}">{{$s->name}} - {{$s->branch}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-outline">
                                    <label >Payment Note <small>(optional)</small></label>
                                    <textarea name="editor2" id="editor3" rows="5" cols="80">
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-offset-0 col-md-12">
                                <button type="submit" class="btn green">Submit</button>
                            </div>
                        </div>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
