

<div id="productModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Add Product</h4>
            </div>
            <form action="{{url('')}}/product" class="form-horizontal" method="POST" >
                @csrf
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Product Name*</label>
                                <input  class="form-control" type="text" placeholder="Enter Product Name" name="pro_name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Product Code*</label>
                                <input  class="form-control" type="text" placeholder="Enter Product Code" name="pro_code" required>
                                <span class="text-danger">{{$errors->first('pro_code') ? 'Product Code already exist' : null}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Product Weight <small>(optional)</small></label>
                                <input min="1" class="form-control" type="number" placeholder="Enter Product Weight" name="weight">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Product Unit*</label>
                                <select class="form-control selectpicker" data-live-search="true" name="unit_id" required id="unit_id">
                                    <option value="" disabled selected>Select...</option>
                                    @foreach ($unit as $u)
                                    <option  value="{{$u->id}}">{{$u->u_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Cost*</label>
                                <input min="1" class="form-control" type="number" placeholder="Enter Product Cost" name="cost" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Price*</label>
                                <input min="1" class="form-control" type="number" placeholder="Enter Product Price" name="price" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Description <small>(optional)</small></label>
                                <input class="form-control" type="text" placeholder="Enter Product Description" name="description">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Alert Quantity*</label>
                                <input min="1" class="form-control" type="number" placeholder="Enter Product Alert Quantity" name="alert_quantity" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Brand*</label>
                                <select class="form-control selectpicker" data-live-search="true" name="brand_id" id="brand_id" required>
                                    <option value="" disabled selected>Select...</option>
                                    @foreach ($brands as $u)
                                    <option  value="{{$u->id}}">{{$u->b_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Category*</label>
                                <select class="form-control selectpicker" data-live-search="true" name="cat_id" id="cat_id" required>
                                    <option value="" disabled selected>Select...</option>
                                    @foreach ($cat as $u)
                                    <option  value="{{$u->id}}">{{$u->cat_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Sub Category*</label>
                                <select class="form-control selectpicker" data-live-search="true" name="s_cat_id" id="s_cat_id" required>
                                    <option value="" disabled selected>Select...</option>
                                    @foreach ($sub as $u)
                                    <option  value="{{$u->id}}">{{$u->s_cat_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button type="submit" class="btn green">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
