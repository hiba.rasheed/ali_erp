

<div id="subModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Sub Category</h4>
            </div>
            <form action="{{url('')}}/subcategory" class="form-horizontal" method="POST" >
                @csrf
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Sub Category Name*</label>
                                <input class="form-control" type="text" placeholder="Enter Sub Category Name" name="s_cat_name">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Category Name*</label>
                                <select class="form-control" id="cattt" name="cat_id" required>
                                    <option value="" disabled selected>Select...</option>
                                    @foreach ($cat as $s)
                                        <option value="{{$s->id}}">{{$s->cat_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button type="submit" class="btn green">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
