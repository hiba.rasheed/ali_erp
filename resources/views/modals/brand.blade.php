

<div id="brandModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Brand</h4>
            </div>
            <form action="{{url('')}}/brands" class="form-horizontal" method="POST" >
                @csrf
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Brand Name*</label>
                                <input class="form-control" type="text" placeholder="Enter Product Name" name="b_name" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact Person Name*</label>
                                <input value="{{$brand->c_p_name ?? old('c_p_name')}}" class="form-control" type="text" placeholder="Enter Contact Person Name" name="c_p_name" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact Person Number*</label>
                                <input min="0" title="11 characters minimum" pattern="[0-9]{11}" value="{{$brand->c_p_contactNo ?? old('c_p_contactNo')}}" class="form-control" type="number" placeholder="Enter Contact Person Number" name="c_p_contactNo" required>
                            </div>
                            <span class="text-danger">{{$errors->first('c_p_contactNo') ? 'Contact number not be greater than 11 digits' : null}}</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button type="submit" class="btn green">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
