<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', function () {
    return view('auth.login');
});
//trialBalance
////////////////// dashboard and report//////////////////////
Route::get('/pos-opening-closing','HomeController@POS_OC')->name('pos.oc');
Route::get('/dashboard','HomeController@index')->name('dashboard');
Route::get('/pos-detail','HomeController@pos')->name('pos');
Route::get('/pos-detail/product/{id}','HomeController@posProduct')->name('pos.product');
Route::get('/pos-detail/product/code/{code}','HomeController@posProductCode')->name('pos.product.code');
Route::get('/price-checker','HomeController@priceChecker')->name('price-checker');
Route::get('/trialBalance','HomeController@trialBalance')->name('trialBalance');
Route::get('/trialBalance/excel','HomeController@trialBalanceExcel')->name('trialBalance.excel');
Route::get('/trialBalance/search/{lm}/{trial}/{period}','HomeController@trialBalanceSearchExcel')->name('trialBalance.search.excel');
Route::post('/trialBalance/search','HomeController@trialBalanceSearch')->name('trialBalance.search');

///////////////// Home controller incomeStatement Reports Route //////////////////////////////

Route::post('/reports/incomeStatementsearch','HomeController@incomeStatementSearch')->name('reports.incomeStatementsearch');
Route::get('/reports/incomeStatement','HomeController@incomeStatement')->name('reports.incomeStatement');
Route::get('/reports/incomeStatementsearch/{M}/excel/month','HomeController@incomeStatementSearchMExcel')->name('reports.incomeStatementsearchMExcel');//excel for month
Route::get('/reports/incomeStatementsearch/{M}/pdf/month','HomeController@incomeStatementSearchMPDF')->name('reports.incomeStatementsearchMpdf');//pdf for month
Route::get('/reports/incomeStatementsearch/{Y}/excel/year','HomeController@incomeStatementSearchYExcel')->name('reports.incomeStatementsearchYExcel');//excel for year
Route::get('/reports/incomeStatementsearch/{Y}/pdf/year','HomeController@incomeStatementSearchYPDF')->name('reports.incomeStatementsearchYpdf');//pdf for year
Route::get('/reports/incomeStatement/excel','HomeController@incomeStatementExcel')->name('reports.incomeStatement.excel');
Route::get('/reports/incomeStatement/pdf','HomeController@incomeStatementPDF')->name('reports.incomeStatement.pdf');

///////////////// Home controller sale costing Reports Route //////////////////////////////

Route::post('/reports/saleCostingsearch','HomeController@saleCostingSearch')->name('reports.saleCostingsearch');
Route::get('/reports/salesCosting','HomeController@salesCosting')->name('reports.salesCosting');
Route::get('/reports/saleCosting/excel','HomeController@saleCostingExcel')->name('reports.saleCosting.excel');
Route::get('/reports/saleCosting/pdf','HomeController@saleCostingPDF')->name('reports.saleCosting.pdf');
Route::get('/reports/saleCostingsearch/{M}/excel/month','HomeController@saleCostingSearchMExcel')->name('reports.saleCostingsearchMExcel');//excel for month
Route::get('/reports/saleCostingsearch/{M}/pdf/month','HomeController@saleCostingSearchMPDF')->name('reports.saleCostingsearchMpdf');//pdf for month
Route::get('/reports/saleCostingsearch/{Y}/excel/year','HomeController@saleCostingSearchYExcel')->name('reports.saleCostingsearchYExcel');//excel for year
Route::get('/reports/saleCostingsearch/{Y}/pdf/year','HomeController@saleCostingSearchYPDF')->name('reports.saleCostingsearchYpdf');//pdf for year
Route::get('/reports/saleCostingsearch/{D}/excel/date','HomeController@saleCostingSearchDExcel')->name('reports.saleCostingsearchDExcel');//excel for one date
Route::get('/reports/saleCostingsearch/{D}/pdf/date','HomeController@saleCostingSearchDPDF')->name('reports.saleCostingsearchDpdf');//pdf for one date
Route::get('/reports/saleCostingsearch/{from}/{to}/excel/date','HomeController@saleCostingSearchDatesExcel')->name('reports.saleCostingsearchDatesExcel');//excel for date diff


//////////////// product controller /////////////////////////

Route::get('/product/datatable','ProductController@datatable')->name('product.datatable');
Route::get('/product/modal/{id}','ProductController@modal')->name('product.modal');
Route::get('/product/variants/{id}','ProductController@variants')->name('product.variants');
Route::get('/product/Pvariants/{id}','ProductController@Pvariants')->name('product.Pvariants');
Route::get('/product/Pvariantsforfinishproduct/{id}','ProductController@Pvariantsforfinishproduct')->name('product.Pvariantsforfinishproduct');
Route::get('/product/sales/{id}','ProductController@sales')->name('product.sales');
Route::get('/product/salesProduct/{id}','ProductController@salesProduct')->name('product.salesProduct');
Route::get('/product/detail/{id}','ProductController@detail')->name('product.detail');
Route::get('/product/excel','ProductController@excel')->name('product.excel');
Route::get('/product/finishProducts','ProductController@finishProducts')->name('product.finishProducts');
Route::get('/product/finishProducts/datatable','ProductController@finishProductsDatatable')->name('product.finishProducts.datatable');
Route::get('/product/finishProducts/edit/{id}','ProductController@finishProductEdit')->name('product.finishProducts.edit');
Route::put('/product/finishProducts/update/{id}','ProductController@finishProductUpdate')->name('product.finishProducts.update');
Route::get('/product/finish/excel','ProductController@Finishexcel')->name('product.finish.excel');
Route::get('/product/add','ProductController@add')->name('product.add');
Route::get('/product/stockShow/{id}','ProductController@stockShow')->name('product.stockShow');
Route::get('/product/add/show/{id}','ProductController@addSingleProduct')->name('product.add.show');
Route::get('/product/add/datatable','ProductController@addDT')->name('product.add.datatable');

Route::get('/product/productsearch/{id}','ProductController@productsearch')->name('product.productsearch');
Route::post('/product/addFinishProduct','ProductController@addFinishProduct')->name('product.addFinishProduct');
Route::resource('/product','ProductController');
Route::post('/product/status','ProductController@status')->name('product.status');
Route::post('/product/variants/status','ProductController@Vstatus')->name('product.variants.status');

/////////////////////// brands controller //////////////////////////

Route::get('/brands/datatable','BrandsController@datatable')->name('brands.datatable');
Route::get('/brands/excel','BrandsController@excel')->name('brands.excel');
Route::resource('/brands','BrandsController');
Route::post('/brands/status','BrandsController@status')->name('brands.status');

//////////////////////// unit controller ///////////////////////////

Route::get('/unit/datatable','UnitController@unitDatatable')->name('unit.datatable');
Route::resource('/unit','UnitController');
Route::post('/unit/status','UnitController@status')->name('unit.status');

///////////////////// category controller //////////////////////

Route::get('/category/datatable','CategoryController@datatable')->name('category.datatable');
Route::get('/category/excel','CategoryController@excel')->name('category.excel');
Route::resource('/category','CategoryController');
Route::post('/category/status','CategoryController@status')->name('category.status');

//////////////////// sub category controller ///////////////////////

Route::get('/subcategory/datatable','SubcategoryController@datatable')->name('subcategory.datatable');
Route::resource('/subcategory','SubcategoryController');
Route::post('/subcategory/status','SubcategoryController@status')->name('subcategory.status');

////////////// city controller ///////////////////////

Route::post('/city/status','CityController@status')->name('city.status');
Route::get('/city/datatable','CityController@datatable')->name('city.datatable');
Route::resource('/city','CityController');

//////////////////// warehouse controller /////////////////////

Route::get('/warehouse/datatable','WarehouseController@datatable')->name('warehouse.datatable');
Route::resource('/warehouse','WarehouseController');

/////////////////// product transfer controller /////////////////////////

Route::post('/productTransfer/status','ProductTransaferHistoryController@status')->name('productTransfer.status');
Route::get('/productTransfer/product','ProductTransaferHistoryController@product')->name('productTransfer.product');
Route::get('/productTransfer/excel','ProductTransaferHistoryController@excel')->name('productTransfer.excel');
Route::get('/productTransfer/pdf','ProductTransaferHistoryController@pdf')->name('productTransfer.pdf');
Route::post('/productTransfer/search','ProductTransaferHistoryController@search')->name('productTransfer.search');
Route::get('/productTransfer/search/{M}/{pname}/type/{fw}/{tw}/excel/month','ProductTransaferHistoryController@searchMExcel')->name('productTransfer.searchMExcel');//excel for month
Route::get('/productTransfer/search/{Y}/{pname}/type/{fw}/{tw}/excel/year','ProductTransaferHistoryController@searchYExcel')->name('productTransfer.searchYExcel');//excel for year
Route::get('/productTransfer/search/{D}/{pname}/type/{fw}/{tw}/excel/date','ProductTransaferHistoryController@searchDExcel')->name('productTransfer.searchDExcel');//excel for one date
Route::get('/productTransfer/search/{from}/{to}/{pname}/type/{fw}/{tw}/excel/date','ProductTransaferHistoryController@searchDatesExcel')->name('productTransfer.searchDatesExcel');//excel for date diff
Route::resource('/productTransfer','ProductTransaferHistoryController');

///////////////// company controller ////////////////////////

Route::get('/company/datatable','CompanyController@datatable')->name('company.datatable');
Route::get('/company/excel','CompanyController@excel')->name('company.excel');
Route::get('/company/pdf','CompanyController@pdf')->name('company.pdf');
Route::resource('/company','CompanyController');

/////////////////// group controller //////////////////////////

Route::get('/group/datatable','GroupController@datatable')->name('group.datatable');
Route::resource('/group','GroupController');

///////////////counter controller///////////////////////////
Route::get('/counter/datatable','CounterController@datatable')->name('counter.datatable');
Route::resource('/counter','CounterController');


///////////////CounterAssignedController ///////////////////////////
Route::get('/assigned/datatable','CounterAssignedController@datatable')->name('assigned.datatable');
Route::resource('/assigned','CounterAssignedController');

/////////////// customer controller ///////////////////////

Route::post('/customer/status','CustomerController@status')->name('customer.status');
Route::get('/customer/datatable','CustomerController@datatable')->name('customer.datatable');
Route::get('/customer/excel','CustomerController@excel')->name('customer.excel');
Route::get('/customer/pdf','CustomerController@pdf')->name('customer.pdf');
Route::get('/customer/balance/{id}','CustomerController@balance')->name('customer.balance');
Route::resource('/customer','CustomerController');

///////////////// supplier controller /////////////////

Route::post('/supplier/status','SupplierController@status')->name('supplier.status');
Route::get('/supplier/datatable','SupplierController@datatable')->name('supplier.datatable');
Route::get('/supplier/excel','SupplierController@excel')->name('supplier.excel');
Route::get('/supplier/pdf','SupplierController@pdf')->name('supplier.pdf');
Route::resource('/supplier','SupplierController');

////////////////// sale person controller ////////////////////////////////

Route::post('/saleperson/status','SalePersonController@status')->name('saleperson.status');
Route::get('/saleperson/datatable','SalePersonController@datatable')->name('saleperson.datatable');
Route::get('/saleperson/excel','SalePersonController@excel')->name('saleperson.excel');
Route::get('/saleperson/pdf','SalePersonController@pdf')->name('saleperson.pdf');
Route::resource('/saleperson','SalePersonController');

//////////////////// purchase controller  ///////////////////////////

Route::post('/purchase/search','PurchaseOrderController@search')->name('purchase.search');
Route::get('/purchase/pdf/{id}','PurchaseOrderController@pdf')->name('purchase.pdf');
Route::get('/purchaseDetail/{id}','PurchaseOrderController@purchaseDetail')->name('purchaseDetail');
Route::get('/purchase/pdf','PurchaseOrderController@pdf1');
Route::get('/purchase/grr/{no,id}','PurchaseOrderController@GRR')->name('purchase.GRR');
Route::get('/purchase/grr1/{no}','PurchaseOrderController@GRR1');
Route::get('/purchase/document/{id}','PurchaseOrderController@document')->name('purchase.document');
Route::get('/purchase/invoice/{id}','PurchaseOrderController@invoice')->name('purchase.invoice');
Route::get('/purchase/product/{id}','PurchaseOrderController@product')->name('purchase.product');
Route::get('/purchase/grn/{id}','PurchaseOrderController@GRN')->name('purchase.grn');
Route::get('/purchase/report','PurchaseOrderController@report')->name('purchase.report');
// Route::get('/purchase/excel','PurchaseOrderController@excel')->name('purchase.excel');
Route::resource('/purchase','PurchaseOrderController');
Route::post('/purchase/status','PurchaseOrderController@status')->name('purchase.status');
// Route::post('/purchase/Receiving','PurchaseOrderController@Receiving')->name('purchase.Receiving');
Route::post('/purchase/updateQuantity','PurchaseOrderController@updateQuantity')->name('purchase.updateQuantity');

//////////////////// biller controller ////////////////////////////////

Route::post('/biller/status','BillerController@status')->name('biller.status');
Route::get('/biller/datatable','BillerController@datatable')->name('biller.datatable');
Route::get('/biller/excel','BillerController@excel')->name('biller.excel');
Route::get('/biller/pdf','BillerController@pdf')->name('biller.pdf');
Route::resource('/biller','BillerController');

//////////////////// sales  controller ///////////////////////////

Route::post('/sales/search','SalesController@search')->name('sales.search');
Route::get('/sales/sync','SalesController@sync')->name('sales.sync');
Route::get('/sales/excel','SalesController@excel')->name('sales.excel');
Route::get('/sales/search/{Y}/{c}/{s}/{ss}/{ps}/{check}/excel/year','SalesController@saleYExcel')->name('sales.saleYExcel');//excel for year
Route::get('/sales/search/{M}/{c}/{s}/{ss}/{ps}/{check}/excel/month','SalesController@saleMExcel')->name('sales.saleMExcel');//excel for month
Route::get('/sales/search/{D}/{c}/{s}/{ss}/{ps}/{check}/excel/date','SalesController@saleDExcel')->name('sales.saleDExcel');//excel for one date
Route::get('/sales/search/{from}/{to}/{c}/{s}/{ss}/{ps}/{check}/excel/date','SalesController@saleDatesExcel')->name('sales.saleDatesExcel');//excel for date diff
Route::get('/sales/search/{c}/{s}/{ss}/{check}/excel/ss','SalesController@saleSSExcel')->name('sales.saleSSExcel');//excel for year
Route::get('/sales/search/{c}/{s}/{ps}/{check}/excel/ps','SalesController@salePSExcel')->name('sales.salePSExcel');//excel for year
Route::get('/sales/search/{c}/excel/c','SalesController@saleCExcel')->name('sales.saleCExcel');//excel for year
Route::get('/sales/search/{sp}/excel/sp','SalesController@saleSPExcel')->name('sales.saleSPExcel');//excel for year

Route::get('/sales/report','SalesController@report')->name('sales.report');
Route::get('/sales/report/excel','SalesController@reportExcel')->name('sales.report.excel');
Route::post('/sales/report/search','SalesController@reportSearch')->name('sales.report.search');
Route::get('/sales/report/search/{Y}/{c}/{s}/{p}/{type}/{w}/{check}/excel/year','SalesController@reportExcelYExcel')->name('sales.report.saleYExcel');//excel for year
Route::get('/sales/report/search/{M}/{c}/{s}/{p/{type}}/{w}/{check}/excel/month','SalesController@reportExcelMExcel')->name('sales.report.saleMExcel');//excel for month
Route::get('/sales/report/search/{D}/{c}/{s}/{p}/{type}/{w}/{check}/excel/date','SalesController@reportExcelDExcel')->name('sales.report.saleDExcel');//excel for one date
Route::get('/sales/report/search/{from}/{to}/{c}/{s}/{p}/{type}/{w}/{check}/excel/date','SalesController@reportExcelDatesExcel')->name('sales.report.saleDatesExcel');//excel for date diff
Route::get('/sales/report/search/{c}/excel/c','SalesController@reportCExcel')->name('sales.report.reportExcelCExcel');//excel for year
Route::get('/sales/report/search/{p}/{type}/excel/p','SalesController@reportPExcel')->name('sales.report.reportExcelPExcel');//excel for year
Route::get('/sales/report/search/{w}/excel/w','SalesController@reportWExcel')->name('sales.report.reportExcelWExcel');//excel for year
Route::get('/sales/report/search/{sp}/excel/sp','SalesController@reportSPExcel')->name('sales.report.reportExcelSPExcel');

Route::get('/sales/pdf/{id}','SalesController@pdf')->name('sales.pdf');
Route::get('/sales/pdf','SalesController@pdf1');
Route::get('/sales/gdr/{no,id}','SalesController@GDR')->name('sales.GDR');
Route::get('/sales/gdr1/{no}','SalesController@GDR1');
Route::get('/sales/invoice/{id}','SalesController@invoice')->name('sales.invoice');
Route::get('/sales/product/{id}','SalesController@product')->name('sales.product');
Route::get('/sales/document/{id}','SalesController@document')->name('sales.document');
Route::get('/sales/gdn/{id}','SalesController@GDN')->name('sales.gdn');
Route::get('/sales/return/view','SalesController@returnView')->name('sales.return.view');
Route::get('/sales/return/show/{id}','SalesController@returnShow')->name('sales.return.show');
Route::resource('/sales','SalesController');
Route::post('/sales/status','SalesController@status')->name('sales.status');
Route::post('/sales/returnstatus','SalesController@returnStatus')->name('sales.returnstatus');
Route::post('/sales/delivery','SalesController@delivery')->name('sales.delivery');
Route::post('/sales/return','SalesController@return')->name('sales.return');

///////////////// transaction controller ////////////////////////////

Route::get('/transaction/view','TransactionHistoryController@view')->name('transaction.view');
Route::get('/transaction/excel','TransactionHistoryController@excel')->name('transaction.excel');
Route::post('/transaction/search','TransactionHistoryController@search')->name('transaction.search');
Route::get('/transaction/search/{M}/{tf}/{pb}/excel/month','TransactionHistoryController@searchMExcel')->name('transaction.searchMExcel');//excel for month
Route::get('/transaction/search/{Y}/{tf}/{pb}/excel/year','TransactionHistoryController@searchYExcel')->name('transaction.searchYExcel');//excel for year
Route::get('/transaction/search/{D}/{tf}/{pb}/excel/date','TransactionHistoryController@searchDExcel')->name('transaction.searchDExcel');//excel for one date
Route::get('/transaction/search/{from}/{to}/{tf}/{pb}/excel/date','TransactionHistoryController@searchDatesExcel')->name('transaction.searchDatesExcel');//excel for date diff
// Route::get('/transaction/datatable','TransactionHistoryController@datatable')->name('transaction.datatable');
Route::resource('/transaction','TransactionHistoryController');

////////////////// stock controller ///////////////////////////

Route::post('/stocks/stockInSearch','StockController@stockInSearch')->name('stocks.stockInSearch');
Route::get('/stocks/search/{id}','StockController@search')->name('stocks.search');
Route::get('/stocks/current','StockController@current')->name('stocks.current');
Route::get('/stocks/OC_inventory','StockController@OC_inventory')->name('stocks.OC_inventory');
Route::post('/stocks/OC_inventory','StockController@OC_inventory')->name('stocks.OC_inventory');
Route::get('/stocks/pdf','StockController@pdf')->name('stocks.pdf');
Route::get('/stocks/excel','StockController@excel')->name('stocks.excel');
Route::resource('/stocks','StockController');

///////////////////// bank controller /////////////////////////////

Route::post('/bank/status','BankController@status')->name('bank.status');
Route::get('/bank/datatable','BankController@datatable')->name('bank.datatable');
Route::resource('/bank','BankController');

/////////////////////// accounts controller ///////////////////////

Route::get('/accountdetails/view/{id}','AccountDetailsController@view')->name('accountdetails.view');
Route::get('/accountdetails/prodName/{id}','AccountDetailsController@prodName')->name('accountdetails.prodName');
Route::get('/accountdetails/chkHistory/{id}','AccountDetailsController@chkHistory')->name('accountdetails.chkHistory');
Route::get('/accountdetails/history/{id}','AccountDetailsController@history')->name('accountdetails.history');
Route::resource('/accountdetails','AccountDetailsController');

///////////////////// general ledger controller ////////////////////////////

Route::post('/generalLedger/updateEntry','GeneralLedgerController@updateEntry')->name('generalLedger.updateEntry');
Route::get('/generalLedger/find/{id}','GeneralLedgerController@find')->name('generalLedger.find');
Route::resource('/generalLedger','GeneralLedgerController');

////////////////// head category controller ////////////////////

Route::get('/headcategory/history/{id}','HeadCategoryController@history')->name('headcategory.history');
Route::get('/headcategory/account/{id}','HeadCategoryController@accountsName')->name('headcategory.account');
Route::resource('/headcategory','HeadCategoryController');

/////////////////////// head of accounts controller///////////////////////////////

Route::resource('/headofaccounts','HeadofAccountsController');

//////////////////// stock out controller ///////////////////////////

Route::get('/stockout/PosInvoice/{id}','StockOutController@PosInvoice')->name('stockout.PosInvoice');
Route::get('/stockout/pdf/{id}','StockOutController@pdf')->name('stockout.pdf');
Route::post('/stockout/stockOutSearch','StockOutController@stockOutSearch')->name('stockout.stockOutSearch');
Route::resource('/stockout','StockOutController');

/////////////////////// Stock Out Request Controller ////////////////////////////
Route::post('/stockoutRequest/status','StockOutRequestController@status')->name('stockoutRequest.status');
Route::post('/stockoutRequest/delivery','StockOutRequestController@delivery')->name('stockoutRequest.delivery');
Route::resource('/stockoutRequest','StockOutRequestController');

////////////////////// Purchase Request Controller /////////////////////////
Route::post('/request/status','PurchaseRequestController@status')->name('request.status');
// Route::get('/request/datatable','PurchaseRequestController@datatable')->name('request.datatable');
Route::resource('/request','PurchaseRequestController');

//////////////////////// Quotation Controller/////////////////////////////////
Route::get('/quotation/newQuotation/{id}','QuotationController@newQuotation')->name('quotation.newQuotation');
Route::post('/quotation/status','QuotationController@status')->name('quotation.status');
Route::resource('/quotation','QuotationController');


////////////////////// Roles Controller ==////////////////////////
Route::get('/roles/datatable','RolesController@datatable')->name('roles.datatable');
Route::get('/roles/menu/{id}','RolesController@menu')->name('roles.menu');
Route::get('/roles/permission/{id}','RolesController@permission')->name('roles.permission');
Route::post('/roles/status','RolesController@status')->name('roles.status');
Route::resource('/roles','RolesController');


////////////////////// User Menu Controller ==////////////////////////
Route::get('/menu/datatable','UserMenuController@datatable')->name('menu.datatable');
Route::resource('/menu','UserMenuController');

///////////////// permission controller////////////////////////
Route::get('/permission/datatable','PermissionsController@datatable')->name('permission.datatable');
Route::resource('/permission','PermissionsController');


///////////////// user controller////////////////////////
Route::get('/users/datatable','UserController@datatable')->name('users.datatable');
Route::get('/users/editProfile/{id}','UserController@editProfile')->name('users.editProfile');
Route::resource('/users','UserController');

Auth::routes();
////////////////////// login customize ============================

Route::post('/login/user','Auth\LoginController@loginUser')->name('login.user');

//////////////// mark all notifications as read /////////////////////////////
Route::get('markAsRead',function(){
    auth()->user()->unreadNotifications->markAsRead();
    return redirect()->back();
})->name('markAllRead');

////////////////////// mark signle notification as read /////////////////////
Route::get('markRead/{id}',function($id){
    $userUnreadNotification = auth()->user()->unreadNotifications->where('id', $id)->first();
    if($userUnreadNotification) {
        $userUnreadNotification->markAsRead();
    }
    return redirect()->back();
})->name('markReadSingle');

//////////////////////// VIEW ALL NOTIFICATIONS /////////////////////////////////
Route::get('/notifications', function () {
    return view('notifications');
})->name('viewAllNotifications');


/////////////////////// biller request update ////////////////////////
Route::get('/billerUpdateRequest/status/{id}','BillerUpdateRequestController@status')->name('billerUpdateRequest.status');
Route::get('/billerUpdateRequest/datatable','BillerUpdateRequestController@datatable')->name('billerUpdateRequest.datatable');
Route::resource('/billerUpdateRequest','BillerUpdateRequestController');


/////////////////////// customer request update ////////////////////////
Route::get('/customerUpdateRequest/status/{id}','CustomerUpdateRequestController@status')->name('customerUpdateRequest.status');
Route::get('/customerUpdateRequest/datatable','CustomerUpdateRequestController@datatable')->name('customerUpdateRequest.datatable');
Route::resource('/customerUpdateRequest','CustomerUpdateRequestController');

/////////////////////// saleperson request update ////////////////////////
Route::get('/salepersonUpdateRequest/status/{id}','SalePersonUpdateRequestController@status')->name('salepersonUpdateRequest.status');
Route::get('/salepersonUpdateRequest/datatable','SalePersonUpdateRequestController@datatable')->name('salepersonUpdateRequest.datatable');
Route::resource('/salepersonUpdateRequest','SalePersonUpdateRequestController');

/////////////////////// supplier request update ////////////////////////
Route::get('/supplierUpdateRequest/status/{id}','SupplierUpdateRequestController@status')->name('supplierUpdateRequest.status');
Route::get('/supplierUpdateRequest/datatable','SupplierUpdateRequestController@datatable')->name('supplierUpdateRequest.datatable');
Route::resource('/supplierUpdateRequest','SupplierUpdateRequestController');


/////////////////////// product request update ////////////////////////
Route::get('/productUpdateRequest/status/{id}','ProductUpdateRequestController@status')->name('productUpdateRequest.status');
Route::get('/productUpdateRequest/datatable','ProductUpdateRequestController@datatable')->name('productUpdateRequest.datatable');
Route::resource('/productUpdateRequest','ProductUpdateRequestController');

//////////////////////// variants controller ///////////////////////////
Route::resource('/variants','VariantsController');

// use App\AccountDetails;
// use App\Products;
// use App\Brands;
// use App\Category;
// use App\Unit;
// use App\Subcategory;
// use App\HeadCategory;
// use App\ProductVariants;
//import data query//////////////
// Route::get('/add/data',function(){
//     // dd(storage_path());
//     $filename         = storage_path().'/app/variants.csv';
//     $delimiter        = ',';
//     if (!file_exists($filename) || !is_readable($filename))
//         return false;
//     $header = null;
//     $data = array();
//     if (($handle = fopen($filename, 'r')) !== false)
//     {
//         while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
//         {
//             if (!$header)
//                 $header = $row;
//             else
//                 {
//                     $data[] = array_combine($header, $row);
//                 }

//         }
//         fclose($handle);
//     }
//     // $account = AccountDetails::latest('created_at')->orderBy('Code','desc')->where('c_id',6)->first();
//     // dd($account);
//     // if($account == null)
//     // {
//         $id = 133;
//         // dd($id);
//     // }
//     // else
//     // {
//     //     $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1;
//     // }
//     for ($i = 0; $i < count($data); $i ++)
//     {
//         // if($data[$i]['vstatus'] == 0)
//         // {
//             // $s = Subcategory::where('s_cat_name',$data[$i]['sub-category'])->first();
//             // $c = Category::where('cat_name',$data[$i]['category'])->first();
//             // $b = Brands::where('b_name',$data[$i]['brand'])->first();
//             // $u = Unit::where('u_name',$data[$i]['unit'])->first();
//             // Products::create([
//             //     'pro_code' => $data[$i]['code'],
//             //     'pro_name' => $data[$i]['product name'],
//             //     'p_type' => $data[$i]['product type'],
//             //     'unit_id' => $u->id,
//             //     'brand_id' => $b->id,
//             //     'cat_id' => $c->id,
//             //     's_cat_id' => $s->id,
//             //     'vstatus' => $data[$i]['vstatus'],
//             // ]);
//         //     $hcat = HeadCategory::where('name','Inventory')->first();

//         //     $str_length = strlen((string)$id)+2;
//         //     $id = substr("0000{$id}", -$str_length);
//         //     $code = $hcat->code.'-'.$id;
//         //     // dd($code);
//         //     $data1 = [
//         //         'Code' => $code,
//         //         'name_of_account' => $data[$i]['code'].' - '.$data[$i]['product name'],
//         //         'c_id' => $hcat->id,
//         //         'type' => 0
//         //     ];
//         //     AccountDetails::create($data1);
//         //     $id++;

//         // }
//         // else {
//         //     $s = Subcategory::where('s_cat_name',$data[$i]['sub-category'])->first();
//         //     $c = Category::where('cat_name',$data[$i]['category'])->first();
//         //     $b = Brands::where('b_name',$data[$i]['brand'])->first();
//         //     $u = Unit::where('u_name',$data[$i]['unit'])->first();
//         //     Products::create([
//         //         'pro_code' => $data[$i]['code'],
//         //         'pro_name' => $data[$i]['product name'],
//         //         'p_type' => $data[$i]['product type'],
//         //         'unit_id' => $u->id,
//         //         'brand_id' => $b->id,
//         //         'cat_id' => $c->id,
//         //         's_cat_id' => $s->id,
//         //         'vstatus' => $data[$i]['vstatus'],
//         //     ]);
//         // }
//         $p = Products::where('pro_code',$data[$i]['code'])->first();
//         $name = $data[$i]['code'].' - '.$data[$i]['product name'].'-';
//         if($data[$i]['variant']!="" && $data[$i]['color']!="" && $data[$i]['size']!="" )
//         {
//             $name.=$data[$i]['variant'].'-'.$data[$i]['color'].'-'.$data[$i]['size'];
//         }
//         if($data[$i]['variant']=="" && $data[$i]['color']!="" && $data[$i]['size']!="")
//         {
//             $name.=$data[$i]['color'].'-'.$data[$i]['size'];
//         }
//         if($data[$i]['variant']!="" && $data[$i]['color']=="" && $data[$i]['size']!="")
//         {
//             $name.=$data[$i]['variant'].'-'.$data[$i]['size'];
//         }
//         if($data[$i]['variant']!="" && $data[$i]['color']!="" && $data[$i]['size']=="")
//         {
//             $name.=$data[$i]['variant'].'-'.$data[$i]['color'];
//         }
//         if($data[$i]['variant']!="" && $data[$i]['color']=="" && $data[$i]['size']=="")
//         {
//             $name.=$data[$i]['variant'];
//         }
//         if($data[$i]['variant']=="" && $data[$i]['color']=="" && $data[$i]['size']!="")
//         {
//             $name.=$data[$i]['size'];
//         }
//         if($data[$i]['variant']=="" && $data[$i]['color']!="" && $data[$i]['size']=="")
//         {
//             $name.=$data[$i]['color'];
//         }

//         ProductVariants::create([
//             'p_id' => $p->id,
//             'name' => $name,
//         ]);
//             // $hcat = HeadCategory::where('name','Inventory')->first();
//             // $str_length = strlen((string)$id)+2;
//             // $id = substr("0000{$id}", -$str_length);
//             // // dd($id);
//             // $code = $hcat->code.'-'.$id;
//             // // dd($code);
//             // $data1 = [
//             //     'Code' => $code,
//             //     'name_of_account' => $name,
//             //     'c_id' => $hcat->id,
//             //     'type' => 0
//             // ];
//             // AccountDetails::create($data1);
//             // $id++;

//     }
//     // dd($name);
//     return 'ok';
// });

