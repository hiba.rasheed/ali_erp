<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('order_date');
            $table->string('ref_no')->nullable();
            // $table->unique('ref_no');
            $table->unsignedBigInteger('w_id');
            $table->foreign('w_id')->references('id')->on('warehouse');
            $table->string('status')->default('Pending');
            $table->string('doc')->nullable();
            $table->unsignedBigInteger('s_id');
            $table->foreign('s_id')->references('id')->on('vendors');
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order');
    }
}
