<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarehouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('w_name');
            $table->string('w_address');
            $table->string('w_type');
            $table->bigInteger('w_contactNo')->nullable();
            $table->string('c_p_name');
            $table->bigInteger('c_p_contactNo');
            $table->unsignedBigInteger('c_id');
            $table->foreign('c_id')->references('id')->on('city');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse');
    }
}
