<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTransferHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_transfer_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('transfer_date');
            $table->unsignedBigInteger('from_w')->nullable();
            $table->foreign('from_w')->references('id')->on('warehouse');
            $table->unsignedBigInteger('to_w')->nullable();
            $table->foreign('to_w')->references('id')->on('warehouse');
            $table->unsignedBigInteger('p_id')->nullable();
            $table->foreign('p_id')->references('id')->on('products');
            $table->bigInteger('quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_transfer_history');
    }
}
